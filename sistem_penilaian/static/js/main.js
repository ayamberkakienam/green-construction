$(function () {
    $('td').each(function (index, value) {
        // ASPEK
        if (this.innerHTML == 'Aspek Konservasi Energi') {
            this.innerHTML += ` <i class="zmdi zmdi-pin-help"
            data-toggle="tooltip" data-placement="right"
            title="Penilaian terhadap upaya pemantauan dan pencatatan terhadap pemakaian, penghematan konsumsi energi, dan pengendalian penggunaan sumber energi yang berdampak terhadap lingkungan selama proses konstruksi."></i>`;
        }
        else if (this.innerHTML == 'Aspek Konservasi Air') {
            this.innerHTML += ` <i class="zmdi zmdi-pin-help"
            data-toggle="tooltip" data-placement="right"
            title="Penilaian terhadap upaya pemantauan dan pencatatan pemakaian, penghematan konsumsi air, dan melakukan daur ulang pemakaian air (menggunakan limpasan air hujan) selama proses konstruksi."></i>`;
        }
        else if (this.innerHTML == 'Aspek Tepat Guna Lahan') {
            this.innerHTML += ` <i class="zmdi zmdi-pin-help"
            data-toggle="tooltip" data-placement="right"
            title="Penilaian terhadap upaya pemeliharaan kualitas kehijauan lingkungan, pengurangan/menyerapan CO2 dan polutan, serta upaya mengurangi beban drainase yang disebabkan oleh limpasan air hujan selama proses konstruksi."></i>`;
        }
        else if (this.innerHTML == 'Aspek Sumber dan Siklus Material') {
            this.innerHTML += ` <i class="zmdi zmdi-pin-help"
            data-toggle="tooltip" data-placement="right"
            title="Penilaian terhadap upaya menahan laju eksploitasi sumber daya alam tidak terbarukan, diperlukan upaya untuk memperpanjang daur hidup material, yang mencakup:
            a. Penggunaan material lokal bekas bangunan (hasil dekonstruksi) untuk mengurangi pemakaian material baru dan mengurangi limbah di tempat pembuangan akhir serta memperpanjang usia pakai material
            b. Penggunaan bahan bangunan yang menggunakan proses daur ulang dan yang ramah lingkungan
            c. Penggunaan bahan baku kayu yang dapat dipertanggungjawabkan asal usulnya
            d. Penggunaan material lokal untuk mengurangi energi untuk proses transportasi
            "></i>`
        }
        else if (this.innerHTML == 'Aspek Manajemen Lingkungan Bangunan') {
            this.innerHTML += ` <i class="zmdi zmdi-pin-help"
            data-toggle="tooltip" data-placement="right"
            title="Penilaian terhadap upaya mengurangi limbah dan mendorong gerakan pemilahan sampah secara sederhana sehingga mempermudah proses daur ulang."></i>`
        }
        else if (this.innerHTML == 'Aspek Kualitas Udara') {
            this.innerHTML += ` <i class="zmdi zmdi-pin-help"
            data-toggle="tooltip" data-placement="right"
            title="Penilaian terhadap upaya mengurangi pencemaran udara yang ditimbulkan akibat bahan bangunan dan peralatan yang digunakan selama proses konstruksi."></i>`
        }
        else if (this.innerHTML == 'Aspek Kesehatan dan Kenyamanan dalam Proyek') {
            this.innerHTML += ` <i class="zmdi zmdi-pin-help"
            data-toggle="tooltip" data-placement="right"
            title="Penilaian terhadap upaya: (a) Pengurangan dampak asap rokok; (b) Pengurangan polusi zat kimia yang
            berbahaya bagi kesehatan manusia, dan (c) Menjaga kebersihan dan kenyamanan lingkungan proyek"></i>`
        }

        // Faktor
        if (this.innerHTML == 'Rencana Perlindungan Lokasi Pekerjaan') {
            this.innerHTML += ` <i class="zmdi zmdi-pin-help" data-toggle="tooltip" data-placement="right"
            title="Maksud dan tujuan faktor ini adalah mengurangi kerusakan ekologi dan kerusakan lainnya dan menjalin relasi yang baik dengan berbagai pihak selama proses konstruksi."></i>`
        }
        else if (this.innerHTML == 'Pengurangan Jejak Ekologis Tahap Konstruksi') {
            this.innerHTML += ` <i
            class="zmdi zmdi-pin-help" data-toggle="tooltip" data-placement="right"
            title="Maksud dan tujuan faktor ini adalah efisiensi penggunaan sumber daya alam dan mengurangi emisi akibat pembakaran minyak bumi."></i>`
        }
        else if (this.innerHTML == 'Pengelolaan Lahan') {
            this.innerHTML += ` <i
            class="zmdi zmdi-pin-help" data-toggle="tooltip" data-placement="right"
            title="Maksud dan tujuan faktor ini adalah pertimbangan dalam pemilihan lokasi gedung yang akan berdampak pada
            kinerja dari gedung yang akan dibangun dalam hal pemilihan dan pengolahan lahan, alternatif
            transportasi, gangguan pada lingkungan, pengelolaan air dan polusi. Dalam tahap konstruksi lebih
            difokuskan penggunaan lahan selama proses konstruksi."></i>`
        }
        else if (this.innerHTML == 'Perencanaan dan Penjadwalan Proyek') {
            this.innerHTML += ` <i
            class="zmdi zmdi-pin-help" data-toggle="tooltip" data-placement="right"
            title="Maksud dan tujuan faktor ini adalah penggunaan bahan bangunan ramah lingkungan, salah satunya adalah menggunakan bahan bangunan yang bersumber dari sekitar lokasi proyek (aspek lokalitas) dengan pertimbangan jarak pengambilan material ke lokasi pekerjaan menjadi relatif lebih dekat sehingga berpotensi untuk mereduksi emisi yang bersumber dari penggunaan bahan bakar yang berdampak pada emisi CO2."></i>`
        }
        else if (this.innerHTML == 'Pengelolaan Material') {
            this.innerHTML += ` <i
            class="zmdi zmdi-pin-help" data-toggle="tooltip" data-placement="right"
            title="Maksud dan tujuan faktor ini adalah menjaga keberlanjutan sumber daya alam yang terbarukan
            (misalnya hutan) dengan cara menerapkan tatanan dan pengelolaan yang baik. Sedangkan untuk
            menjaga keberlanjutan sumber daya alam tidak terbarukan dapat dilakukan dengan cara
            memperpanjang daur hidupnya."></i>`
        }
        else if (this.innerHTML == 'Manajemen Limbah Konstruksi') {
            this.innerHTML += ` <i class="zmdi zmdi-pin-help"
            data-toggle="tooltip" data-placement="right"
            title="Tujuan dari faktor ini adalah untuk mengurangi penggunaan berbagai sumber material bangunan dengan cara memakai kembali, dan mendaur ulang."></i>`
        }
        else if (this.innerHTML == 'Pelatihan Bagi Subkontraktor') {
            this.innerHTML += ` <i class="zmdi zmdi-pin-help"
            data-toggle="tooltip" data-placement="right"
            title="Tujuan faktor ini adalah untuk meyakinkan proses konstruksi yang dilakukan oleh subkontraktor ramah lingkungan."></i>`
        }
        else if (this.innerHTML == 'Manajemen Lingkungan Proyek') {
            this.innerHTML += ` <i class="zmdi zmdi-pin-help"
            data-toggle="tooltip" data-placement="right"
            title="Tujuan faktor ini adalah mengantisipasi dampak akibat proses konstruksi terhadap lingkungan di sekitar lokasi proyek."></i>`
        }
        else if (this.innerHTML == 'Dokumentasi') {
            this.innerHTML += ` <i
            class="zmdi zmdi-pin-help" data-toggle="tooltip" data-placement="right"
            title="Tujuan faktor ini adalah untuk menyiapkan dan memastikan proses konstruksi yang akan dilaksanakan ramah lingkungan."></i>`
        }
        else if (this.innerHTML == 'Penyimpanan dan Perlindungan Material') {
            this.innerHTML += ` <i
            class="zmdi zmdi-pin-help" data-toggle="tooltip" data-placement="right"
            title="Tujuan faktor ini adalah untuk menghindari terjadinya kerusakan material yang akan digunakan dalam proses konstruksi."></i>`
        }
        else if (this.innerHTML == 'Pemilihan dan Operasional Peralatan Konstruksi') {
            this.innerHTML += ` <i
            class="zmdi zmdi-pin-help" data-toggle="tooltip" data-placement="right"
            title="Tujuan faktor ini adalah mengurangi pemakaian bahan bakar dan mengurangi terjadinya polusi yang ditimbulkan oleh peralatan yang digunakan. Cara ini tidak hanya memberikan aspek positif terhadap lingkungan tetapi juga mengurangi biaya bahkan mungkin dapat meningkatkan produktivitas."></i>`
        }
        else if (this.innerHTML == 'Kualitas Udara dalam Proyek') {
            this.innerHTML += ` <i
            class="zmdi zmdi-pin-help" data-toggle="tooltip" data-placement="right"
            title="Tujuan faktor ini adalah terciptanya udara segar tanpa ada kandungan polutan berbahaya selama proses konstruksi berlangsung yang dibutuhkan oleh seluruh pekerja konstruksi."></i>`
        }
        else if (this.innerHTML == 'Kesehatan Lingkungan Kerja Tahap Konstruksi') {
            this.innerHTML += ` <i
            class="zmdi zmdi-pin-help" data-toggle="tooltip" data-placement="right" title="Tujuan faktor ini adalah terciptanya lingkungan kerja dengan memperhatikan proses konstruksi yang efisien, efisiensi konsumsi energi dan didukung moral pekerja.
            "></i>`
        }
        else if (this.innerHTML == 'Program Kesehatan dan Keselamatan Kerja') {
            this.innerHTML += ` <i
            class="zmdi zmdi-pin-help" data-toggle="tooltip" data-placement="right"
            title="Tujuan faktor ini adalah terjaminnya kesehatan dan keselamatan pekerja konstruksi selama pelaksanaan proyek konstruksi."></i>`
        }
    });
});

$(function () {
    $('[data-toggle="tooltip"]').tooltip()
});
