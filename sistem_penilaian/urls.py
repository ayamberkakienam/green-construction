from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('bobot_user', views.bobot_user, name='bobot_user'),
    path('pembobotan_aspek', views.pembobotan_aspek, name='pembobotan_aspek'),
    path('pembobotan_faktor', views.pembobotan_faktor, name='pembobotan_faktor'),
    path('pembobotan_indikator', views.pembobotan_indikator,
         name='pembobotan_indikator'),
    path('sistem_penilaian', views.penilaian, name='sistem_penilaian'),
    path('penilaian_user', views.penilaian_user, name='penilaian_user'),
    path('hasil', views.hasil_user, name='hasil'),
    path('hasil_default', views.hasil_default, name='hasil_default'),
    path('hasil_bobot', views.hasil_bobot, name='hasil_bobot'),
    path('tentang_kami', views.tentang_kami, name='tentang_kami'),
    path('petunjuk_pembobotan', views.petunjuk_pembobotan,
         name='petunjuk_pembobotan'),
    path('form_data', views.form_data, name='form_data'),
    path('kategori', views.kategori, name='kategori'),
    path('kategori_user', views.kategori_user, name='kategori_user'),
    path('setting_bobot', views.setting_bobot, name='setting_bobot'),
    path('database', views.database, name='database')

]
