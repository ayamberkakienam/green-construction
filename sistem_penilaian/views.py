from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.template import loader
from random import seed, random
from .functions import calculate_cr, create_fuzzy_matrix, calculate_weight, fuzzy_logic, nilai_gc_category, calculate_weight_aspek
import numpy as np
from .models import User, Proyek, Bobot_Aspek, Bobot_Faktor, Bobot_Indikator, Penilaian, Himpunan, Aspek_user, Faktor_user, Indikator_user, Cr
# Home page
response_kategori = []


def index(request):
    return render(request, 'sistem_penilaian/index.html')


def database(request):
    responden = User.objects.all()
    proyek = Proyek.objects.all()
    bobot_aspek = Bobot_Aspek.objects.all()
    bobot_faktor = Bobot_Faktor.objects.all()
    bobot_indikator = Bobot_Indikator.objects.all()
    himpunan = Himpunan.objects.all()

    return render(request, 'sistem_penilaian/database.html', {
        'responden': responden,
        'proyek': proyek,
        'bobot_aspek': bobot_aspek,
        'bobot_faktor': bobot_faktor,
        'bobot_indikator': bobot_indikator,
        'himpunan': himpunan
    })

# Pembobotan page


def pembobotan_aspek(request):
    res = []
    if request.method == 'POST':
        cr_value = Cr.objects.last()
        cr_value = cr_value.cr_value
        cr_value = (100 - cr_value)/100
        matrix_pair = np.ones((7, 7, 1), dtype=float)
        idx_row = 0
        idx_col = 1
        for key in request.POST:
            matrix_pair[idx_row][idx_col][0] = eval(request.POST[key])
            matrix_pair[idx_col][idx_row][0] = 1 / \
                matrix_pair[idx_row][idx_col][0]
            if (key in ['energi_kesehatan', 'air_kesehatan', 'tepat_guna_kesehatan', 'material_kesehatan', 'bangunan_kesehatan', 'udara_kesehatan']):
                idx_row += 1
                idx_col = idx_row + 1
            else:
                idx_col += 1

        # Checking CR
        cr = calculate_cr(matrix_pair)
        print(cr)
        if (cr <= cr_value):  # Konsistensi masih bisa diterima
            fuzzy_matrix = create_fuzzy_matrix(matrix_pair)
            weight = calculate_weight_aspek(fuzzy_matrix)

            for i in weight:
                j = '{},{},{}'.format(i[0], i[1], i[2])
                res.append(j)
            # weight = ','.join([str(s) for s in weight.tolist()])
            aspek = Aspek_user(
                A=res[0], B=res[1], C=res[2], D=res[3], E=res[4], F=res[5], G=res[6])
            aspek.save()

            del fuzzy_matrix
            del weight
            del res
            del aspek
            # request.session['aspek_id'] = weight.id

            return redirect('pembobotan_faktor')
        elif (cr > 1):
            return render(request, 'sistem_penilaian/pembobotan_aspek.html')
        else:
            return render(request, 'sistem_penilaian/pembobotan_aspek.html', {"error": "CR tidak mencapai {}".format(cr_value)})
    else:
        return render(request, 'sistem_penilaian/pembobotan_aspek.html', {"error": ""})


def pembobotan_faktor(request):
    res = []
    cr_value = Cr.objects.last()
    cr_value = cr_value.cr_value
    cr_value = (100 - cr_value)/100
    if request.method == 'POST':
        matrix_pair = np.ones((3, 3, 1), dtype=float)
        for key in request.POST:
            idx_row = 0
            idx_col = 1
            matrix_pair[idx_row][idx_col][0] = eval(request.POST[key])
            matrix_pair[idx_col][idx_row][0] = 1 / \
                matrix_pair[idx_row][idx_col][0]
            if (key in ['c1_2', 'e1_3', 'e2_2', 'f1_2']):
                idx_row += 1
                idx_col = idx_row + 1
            elif (key in ['c2_1', 'd1_1', 'e3_1', 'f2_1', 'g1_1']):
                idx_row = 0
                idx_col = 1
                if (key == 'c2_1'):
                    # Checking CR
                    cr = calculate_cr(matrix_pair)
                    if (cr <= cr_value):  # Konsistensi masih bisa diterima
                        fuzzy_matrix = create_fuzzy_matrix(matrix_pair)
                        weight = calculate_weight_aspek(fuzzy_matrix)
                        res.append(weight)

                    else:
                        return render(request, 'sistem_penilaian/pembobotan_faktor.html', {"error_c": "CR tidak mencapai {}".format(cr_value)})
                    # Next Matrix Pair
                    matrix_pair = np.ones((2, 2, 1), dtype=float)  # For D

                elif (key == 'd1_1'):
                    # Checking CR
                    cr = calculate_cr(matrix_pair)
                    print(cr)
                    if (cr <= cr_value):  # Konsistensi masih bisa diterima
                        fuzzy_matrix = create_fuzzy_matrix(matrix_pair)
                        weight = calculate_weight_aspek(fuzzy_matrix)
                        res.append(weight)

                    else:
                        return render(request, 'sistem_penilaian/pembobotan_faktor.html', {"error_d": "CR tidak mencapai {}".format(cr_value)})
                    # Next Matrix Pair
                    matrix_pair = np.ones((4, 4, 1), dtype=float)  # For E

                elif (key == 'e3_1'):
                    # Checking CR
                    cr = calculate_cr(matrix_pair)
                    if (cr <= cr_value):  # Konsistensi masih bisa diterima
                        fuzzy_matrix = create_fuzzy_matrix(matrix_pair)
                        weight = calculate_weight_aspek(fuzzy_matrix)
                        res.append(weight)

                    else:
                        return render(request, 'sistem_penilaian/pembobotan_faktor.html', {"error_e": "CR tidak mencapai {}".format(cr_value)})
                    matrix_pair = np.ones((3, 3, 1), dtype=float)  # For F

                elif (key == 'f2_1'):
                    # Checking CR
                    cr = calculate_cr(matrix_pair)
                    if (cr <= cr_value):  # Konsistensi masih bisa diterima
                        fuzzy_matrix = create_fuzzy_matrix(matrix_pair)
                        weight = calculate_weight_aspek(fuzzy_matrix)
                        res.append(weight)

                    else:
                        return render(request, 'sistem_penilaian/pembobotan_faktor.html', {"error_f": "CR tidak mencapai {}".format(cr_value)})
                    matrix_pair = np.ones((2, 2, 1), dtype=float)  # For G
                elif (key == 'g1_1'):
                    # Checking CR
                    cr = calculate_cr(matrix_pair)
                    if (cr <= cr_value):  # Konsistensi masih bisa diterima
                        fuzzy_matrix = create_fuzzy_matrix(matrix_pair)
                        weight = calculate_weight_aspek(fuzzy_matrix)
                        res.append(weight)

                    else:
                        return render(request, 'sistem_penilaian/pembobotan_faktor.html', {"error_g": "CR tidak mencapai {}".format(cr_value)})
            else:
                idx_col += 1

        faktor = Faktor_user(
            f1='1,1,1',
            f2='1,1,1',
            f3='{},{},{}'.format(res[0][0][0], res[0][0][1], res[0][0][2]),
            f4='{},{},{}'.format(res[0][1][0], res[0][1][1], res[0][1][2]),
            f5='{},{},{}'.format(res[0][2][0], res[0][2][1], res[0][2][2]),
            f6='{},{},{}'.format(res[1][0][0], res[1][0][1], res[1][0][2]),
            f7='{},{},{}'.format(res[1][1][0], res[1][1][1], res[1][1][2]),
            f8='{},{},{}'.format(res[2][0][0], res[2][0][1], res[2][0][2]),
            f9='{},{},{}'.format(res[2][1][0], res[2][1][1], res[2][1][2]),
            f10='{},{},{}'.format(res[2][2][0], res[2][2][1], res[2][2][2]),
            f11='{},{},{}'.format(res[2][3][0], res[2][3][1], res[2][3][2]),
            f12='{},{},{}'.format(res[3][0][0], res[3][0][1], res[3][0][2]),
            f13='{},{},{}'.format(res[3][1][0], res[3][1][1], res[3][1][2]),
            f14='{},{},{}'.format(res[3][2][0], res[3][2][1], res[3][2][2]),
            f15='{},{},{}'.format(res[4][0][0], res[4][0][1], res[4][0][2]),
            f16='{},{},{}'.format(res[4][1][0], res[4][1][1], res[4][1][2])
        )
        faktor.save()

        del fuzzy_matrix
        del weight
        del res
        del faktor
        return redirect('pembobotan_indikator')

    else:
        return render(request, 'sistem_penilaian/pembobotan_faktor.html')


def pembobotan_indikator(request):
    cr_value = Cr.objects.last()
    cr_value = cr_value.cr_value
    cr_value = (100 - cr_value)/100
    res = []
    if request.method == 'POST':
        matrix_pair = np.ones((4, 4, 1), dtype=float)  # For A1
        for key in request.POST:
            idx_row = 0
            idx_col = 1
            matrix_pair[idx_row][idx_col][0] = eval(request.POST[key])
            matrix_pair[idx_col][idx_row][0] = 1 / \
                matrix_pair[idx_row][idx_col][0]
            if (key in ['a11_3', 'a12_2', 'b11_2', 'c11_2', 'g11_2']):
                idx_row += 1
                idx_col = idx_row + 1
            elif (key in ['a13_1', 'b12_1', 'c12_1', 'c31_1', 'd21_1', 'e11_1', 'e31_1', 'f31_1', 'g12_1']):
                idx_row = 0
                idx_col = 1
                if (key == 'a13_1'):
                    # Checking CR
                    cr = calculate_cr(matrix_pair)
                    if (cr <= cr_value):  # Konsistensi masih bisa diterima
                        fuzzy_matrix = create_fuzzy_matrix(matrix_pair)
                        weight = calculate_weight_aspek(fuzzy_matrix)
                        res.append(weight)

                    else:
                        return render(request, 'sistem_penilaian/pembobotan_indikator.html', {"error_a1": "CR tidak mencapai {}".format(cr_value)})
                    # Next Matrix Pair
                    matrix_pair = np.ones((3, 3, 1), dtype=float)  # For B1

                elif (key == 'b12_1'):
                    # Checking CR
                    cr = calculate_cr(matrix_pair)
                    if (cr <= cr_value):  # Konsistensi masih bisa diterima
                        fuzzy_matrix = create_fuzzy_matrix(matrix_pair)
                        weight = calculate_weight_aspek(fuzzy_matrix)
                        res.append(weight)

                    else:
                        return render(request, 'sistem_penilaian/pembobotan_indikator.html', {"error_b1": "CR tidak mencapai {}".format(cr_value)})
                    # Next Matrix Pair
                    matrix_pair = np.ones((3, 3, 1), dtype=float)  # For C1

                elif (key == 'c12_1'):
                    # Checking CR
                    cr = calculate_cr(matrix_pair)
                    if (cr <= cr_value):  # Konsistensi masih bisa diterima
                        fuzzy_matrix = create_fuzzy_matrix(matrix_pair)
                        weight = calculate_weight_aspek(fuzzy_matrix)
                        res.append(weight)

                    else:
                        return render(request, 'sistem_penilaian/pembobotan_indikator.html', {"error_c1": "CR tidak mencapai {}".format(cr_value)})
                    matrix_pair = np.ones((2, 2, 1), dtype=float)  # For C3

                elif (key == 'c31_1'):
                    # Checking CR
                    cr = calculate_cr(matrix_pair)
                    if (cr <= cr_value):  # Konsistensi masih bisa diterima
                        fuzzy_matrix = create_fuzzy_matrix(matrix_pair)
                        weight = calculate_weight_aspek(fuzzy_matrix)
                        res.append(weight)

                    else:
                        return render(request, 'sistem_penilaian/pembobotan_indikator.html', {"error_c3": "CR tidak mencapai {}".format(cr_value)})
                    matrix_pair = np.ones((2, 2, 1), dtype=float)  # For D2

                elif (key == 'd21_1'):
                    # Checking CR
                    cr = calculate_cr(matrix_pair)
                    if (cr <= cr_value):  # Konsistensi masih bisa diterima
                        fuzzy_matrix = create_fuzzy_matrix(matrix_pair)
                        weight = calculate_weight_aspek(fuzzy_matrix)
                        res.append(weight)

                    else:
                        return render(request, 'sistem_penilaian/pembobotan_indikator.html', {"error_d2": "CR tidak mencapai {}".format(cr_value)})
                    matrix_pair = np.ones((2, 2, 1), dtype=float)  # For E1

                elif (key == 'e11_1'):
                    # Checking CR
                    cr = calculate_cr(matrix_pair)
                    if (cr <= cr_value):  # Konsistensi masih bisa diterima
                        fuzzy_matrix = create_fuzzy_matrix(matrix_pair)
                        weight = calculate_weight_aspek(fuzzy_matrix)
                        res.append(weight)

                    else:
                        return render(request, 'sistem_penilaian/pembobotan_indikator.html', {"error_e1": "CR tidak mencapai {}".format(cr_value)})
                    matrix_pair = np.ones((2, 2, 1), dtype=float)  # For E3

                elif (key == 'e31_1'):
                    # Checking CR
                    cr = calculate_cr(matrix_pair)
                    if (cr <= cr_value):  # Konsistensi masih bisa diterima
                        fuzzy_matrix = create_fuzzy_matrix(matrix_pair)
                        weight = calculate_weight_aspek(fuzzy_matrix)
                        res.append(weight)

                    else:
                        return render(request, 'sistem_penilaian/pembobotan_indikator.html', {"error_e3": "CR tidak mencapai {}".format(cr_value)})
                    matrix_pair = np.ones((2, 2, 1), dtype=float)  # For F3

                elif (key == 'f31_1'):
                    # Checking CR
                    cr = calculate_cr(matrix_pair)
                    if (cr <= cr_value):  # Konsistensi masih bisa diterima
                        fuzzy_matrix = create_fuzzy_matrix(matrix_pair)
                        weight = calculate_weight_aspek(fuzzy_matrix)
                        res.append(weight)

                    else:
                        return render(request, 'sistem_penilaian/pembobotan_indikator.html', {"error_f3": "CR tidak mencapai {}".format(cr_value)})
                    matrix_pair = np.ones((3, 3, 1), dtype=float)  # For G1

                elif (key == 'g12_1'):
                    # Checking CR
                    cr = calculate_cr(matrix_pair)
                    if (cr <= cr_value):  # Konsistensi masih bisa diterima
                        fuzzy_matrix = create_fuzzy_matrix(matrix_pair)
                        weight = calculate_weight_aspek(fuzzy_matrix)
                        res.append(weight)

                    else:
                        return render(request, 'sistem_penilaian/pembobotan_indikator.html', {"error_g1": "CR tidak mencapai {}".format(cr_value)})
            else:
                idx_col += 1

        indikator = Indikator_user(
            i1='{},{},{}'.format(res[0][0][0], res[0][0][1], res[0][0][2]),
            i2='{},{},{}'.format(res[0][1][0], res[0][1][1], res[0][1][2]),
            i3='{},{},{}'.format(res[0][2][0], res[0][2][1], res[0][2][2]),
            i4='{},{},{}'.format(res[0][3][0], res[0][3][1], res[0][3][2]),
            i5='{},{},{}'.format(res[1][0][0], res[1][0][1], res[1][0][2]),
            i6='{},{},{}'.format(res[1][1][0], res[1][1][1], res[1][1][2]),
            i7='{},{},{}'.format(res[1][2][0], res[1][2][1], res[1][2][2]),
            i8='{},{},{}'.format(res[2][0][0], res[2][0][1], res[2][0][2]),
            i9='{},{},{}'.format(res[2][1][0], res[2][1][1], res[2][1][2]),
            i10='{},{},{}'.format(res[2][2][0], res[2][2][1], res[2][2][2]),
            i11='1,1,1',
            i12='{},{},{}'.format(res[3][0][0], res[3][0][1], res[3][0][2]),
            i13='{},{},{}'.format(res[3][1][0], res[3][1][1], res[3][1][2]),
            i14='1,1,1',
            i15='{},{},{}'.format(res[4][0][0], res[4][0][1], res[4][0][2]),
            i16='{},{},{}'.format(res[4][1][0], res[4][1][1], res[4][1][2]),
            i17='{},{},{}'.format(res[5][0][0], res[5][0][1], res[5][0][2]),
            i18='{},{},{}'.format(res[5][1][0], res[5][1][1], res[5][1][2]),
            i19='1,1,1',
            i20='{},{},{}'.format(res[6][0][0], res[6][0][1], res[6][0][2]),
            i21='{},{},{}'.format(res[6][1][0], res[6][1][1], res[6][1][2]),
            i22='1,1,1',
            i23='1,1,1',
            i24='1,1,1',
            i25='{},{},{}'.format(res[7][0][0], res[7][0][1], res[7][0][2]),
            i26='{},{},{}'.format(res[7][1][0], res[7][1][1], res[7][1][2]),
            i27='{},{},{}'.format(res[8][0][0], res[8][0][1], res[8][0][2]),
            i28='{},{},{}'.format(res[8][1][0], res[8][1][1], res[8][1][2]),
            i29='{},{},{}'.format(res[8][2][0], res[8][2][1], res[8][2][2]),
            i30='1,1,1'
        )
        indikator.save()

        del fuzzy_matrix
        del weight
        del res
        del indikator
        return redirect('kategori_user')
    else:
        return render(request, 'sistem_penilaian/pembobotan_indikator.html')
# Bobot user page


def bobot_user(request):
    return render(request, 'sistem_penilaian/bobot_user.html')

# Sistem Penilaian page


def penilaian(request):
    if len(Himpunan.objects.all()) != 0:
        sbr_lower = request.POST['sbr_lower']
        sbr_middle = request.POST['sbr_middle']
        sbr_upper = request.POST['sbr_upper']
        br_lower = request.POST['br_lower']
        br_middle = request.POST['br_middle']
        br_upper = request.POST['br_upper']
        c_lower = request.POST['c_lower']
        c_middle = request.POST['c_middle']
        c_upper = request.POST['c_upper']
        b_lower = request.POST['b_lower']
        b_middle = request.POST['b_middle']
        b_upper = request.POST['b_upper']
        sb_lower = request.POST['sb_lower']
        sb_middle = request.POST['sb_middle']
        sb_upper = request.POST['sb_upper']
        himpunan = Himpunan(
            sbr_lower=sbr_lower, sbr_middle=sbr_middle, sbr_upper=sbr_upper,
            br_lower=br_lower, br_middle=br_middle, br_upper=br_upper,
            c_lower=c_lower, c_middle=c_middle, c_upper=c_upper,
            b_lower=b_lower, b_middle=b_middle, b_upper=b_upper,
            sb_lower=sb_lower, sb_middle=sb_middle, sb_upper=sb_upper
        )
        himpunan.save()
        return render(request, 'sistem_penilaian/penilaian.html')
    else:
        sbr_lower = request.POST['sbr_lower']
        sbr_middle = request.POST['sbr_middle']
        sbr_upper = request.POST['sbr_upper']
        br_lower = request.POST['br_lower']
        br_middle = request.POST['br_middle']
        br_upper = request.POST['br_upper']
        c_lower = request.POST['c_lower']
        c_middle = request.POST['c_middle']
        c_upper = request.POST['c_upper']
        b_lower = request.POST['b_lower']
        b_middle = request.POST['b_middle']
        b_upper = request.POST['b_upper']
        sb_lower = request.POST['sb_lower']
        sb_middle = request.POST['sb_middle']
        sb_upper = request.POST['sb_upper']
        himpunan = Himpunan(
            sbr_lower=sbr_lower, sbr_middle=sbr_middle, sbr_upper=sbr_upper,
            br_lower=br_lower, br_middle=br_middle, br_upper=br_upper,
            c_lower=c_lower, c_middle=c_middle, c_upper=c_upper,
            b_lower=b_lower, b_middle=b_middle, b_upper=b_upper,
            sb_lower=sb_lower, sb_middle=sb_middle, sb_upper=sb_upper
        )
        himpunan.save()
        return render(request, 'sistem_penilaian/penilaian.html')


def penilaian_user(request):
    if len(Himpunan.objects.all()) != 0:
        sbr_lower = request.POST['sbr_lower']
        sbr_middle = request.POST['sbr_middle']
        sbr_upper = request.POST['sbr_upper']
        br_lower = request.POST['br_lower']
        br_middle = request.POST['br_middle']
        br_upper = request.POST['br_upper']
        c_lower = request.POST['c_lower']
        c_middle = request.POST['c_middle']
        c_upper = request.POST['c_upper']
        b_lower = request.POST['b_lower']
        b_middle = request.POST['b_middle']
        b_upper = request.POST['b_upper']
        sb_lower = request.POST['sb_lower']
        sb_middle = request.POST['sb_middle']
        sb_upper = request.POST['sb_upper']
        himpunan = Himpunan(
            sbr_lower=sbr_lower, sbr_middle=sbr_middle, sbr_upper=sbr_upper,
            br_lower=br_lower, br_middle=br_middle, br_upper=br_upper,
            c_lower=c_lower, c_middle=c_middle, c_upper=c_upper,
            b_lower=b_lower, b_middle=b_middle, b_upper=b_upper,
            sb_lower=sb_lower, sb_middle=sb_middle, sb_upper=sb_upper
        )
        himpunan.save()
        return render(request, 'sistem_penilaian/penilaian_user.html')
    else:
        sbr_lower = request.POST['sbr_lower']
        sbr_middle = request.POST['sbr_middle']
        sbr_upper = request.POST['sbr_upper']
        br_lower = request.POST['br_lower']
        br_middle = request.POST['br_middle']
        br_upper = request.POST['br_upper']
        c_lower = request.POST['c_lower']
        c_middle = request.POST['c_middle']
        c_upper = request.POST['c_upper']
        b_lower = request.POST['b_lower']
        b_middle = request.POST['b_middle']
        b_upper = request.POST['b_upper']
        sb_lower = request.POST['sb_lower']
        sb_middle = request.POST['sb_middle']
        sb_upper = request.POST['sb_upper']
        himpunan = Himpunan(
            sbr_lower=sbr_lower, sbr_middle=sbr_middle, sbr_upper=sbr_upper,
            br_lower=br_lower, br_middle=br_middle, br_upper=br_upper,
            c_lower=c_lower, c_middle=c_middle, c_upper=c_upper,
            b_lower=b_lower, b_middle=b_middle, b_upper=b_upper,
            sb_lower=sb_lower, sb_middle=sb_middle, sb_upper=sb_upper
        )
        himpunan.save()
        return render(request, 'sistem_penilaian/penilaian_user.html')

# Hasil Bobot


def kategori(request):
    res = [0, 20, 29, 24, 44.50, 53.70, 49.30, 67.80,
           74, 66, 78.80, 87.50, 78.80, 92.20, 100]
    return render(request, 'sistem_penilaian/kategori.html', {'res': res})


def kategori_user(request):
    res = [0, 20, 29, 24, 44.50, 53.70, 49.30, 67.80,
           74, 66, 78.80, 87.50, 78.80, 92.20, 100]
    return render(request, 'sistem_penilaian/kategori_user.html', {'res': res})


def hasil_bobot(request):
    aspek = Bobot_Aspek.objects.filter(pk=request.session['aspek_id'])
    faktor = Bobot_Faktor.objects.filter(pk=request.session['faktor_id'])
    indikator = Bobot_Indikator.objects.filter(
        pk=request.session['indikator_id'])

    weight_aspek = [float(s) for s in aspek[0].aspek.split(',')]
    weight_faktor = [float(s) for s in faktor[0].faktor.split(',')]
    weight_indikator = [float(s) for s in indikator[0].indikator.split(',')]

    return render(request, 'sistem_penilaian/hasil_bobot.html',
                  {"weight_aspek": weight_aspek, "weight_faktor": weight_faktor, "weight_indikator": weight_indikator})

# Setting Bobot


def setting_bobot(request):
    if request.method == 'GET':
        aspek = Bobot_Aspek.objects.filter(pk=1)
        faktor = Bobot_Faktor.objects.filter(pk=1)
        indikator = Bobot_Indikator.objects.filter(pk=1)

        weight_aspek = [float(s) for s in aspek[0].aspek.split(',')]
        weight_faktor = [float(s) for s in faktor[0].faktor.split(',')]
        weight_indikator = [float(s)
                            for s in indikator[0].indikator.split(',')]

        return render(request, 'sistem_penilaian/setting_bobot.html',
                      {"weight_aspek": weight_aspek, "weight_faktor": weight_faktor, "weight_indikator": weight_indikator})

    elif request.method == 'POST':
        bobot_aspek = request.POST.getlist('bobot_aspek')
        bobot_faktor = request.POST.getlist('bobot_faktor')
        bobot_indikator = request.POST.getlist('bobot_indikator')

        aspek = Bobot_Aspek.objects.filter(
            pk=1).update(aspek=','.join(bobot_aspek))
        faktor = Bobot_Faktor.objects.filter(
            pk=1).update(faktor=','.join(bobot_faktor))
        indikator = Bobot_Indikator.objects.filter(
            pk=1).update(indikator=','.join(bobot_indikator))

        aspek = Bobot_Aspek.objects.filter(pk=1)
        faktor = Bobot_Faktor.objects.filter(pk=1)
        indikator = Bobot_Indikator.objects.filter(pk=1)

        weight_aspek = [float(s) for s in aspek[0].aspek.split(',')]
        weight_faktor = [float(s) for s in faktor[0].faktor.split(',')]
        weight_indikator = [float(s)
                            for s in indikator[0].indikator.split(',')]

        return render(request, 'sistem_penilaian/setting_bobot.html',
                      {"weight_aspek": weight_aspek, "weight_faktor": weight_faktor, "weight_indikator": weight_indikator})

# Hasil Penilaian


def tentang_kami(request):
    return render(request, 'sistem_penilaian/tentang_kami.html')


def petunjuk_pembobotan(request):
    return render(request, 'sistem_penilaian/petunjuk_pembobotan.html')


def form_data(request):
    if request.method == 'POST':
        # USER
        nama = request.POST['nama_responden']
        instansi = request.POST['instansi']
        pendidikan = request.POST['pendidikan']
        jabatan = request.POST.getlist('jabatan')
        lama_bekerja = request.POST['lama_bekerja']
        kontak = request.POST['kontak']

        jabatan_string = ','.join(jabatan)
        if 'praktisi' in jabatan:
            jabatan_string += ',' + request.POST['jabatan_lainnya']

        user = User(nama=nama, instansi=instansi, pendidikan=pendidikan,
                    jabatan=jabatan_string, lama_bekerja=lama_bekerja, kontak=kontak)
        user.save()
        request.session['user_id'] = user.id

        # PROYEK
        nama_proyek = request.POST['nama_proyek']
        jenis_bangunan = request.POST['jenis_bangunan']
        luas = request.POST['luas']
        lokasi_bangunan = request.POST['lokasi_bangunan']
        nilai_kontrak = request.POST['nilai_kontrak']
        green_building = request.POST['green_building']
        kontraktor = request.POST['kontraktor']
        proyek = Proyek(nama=nama_proyek, jenis_bangunan=jenis_bangunan, luas_bangunan=luas, lokasi=lokasi_bangunan, nilai_kontrak=nilai_kontrak,
                        green_building=green_building, kontraktor_pelaksana=kontraktor, user_id=user.id)
        proyek.save()
        request.session['proyek_id'] = proyek.id

        if request.GET['redirect'] == 'kategori':
            request.session['aspek_id'] = 1
            request.session['faktor_id'] = 1
            request.session['indikator_id'] = 1
            return redirect('/' + request.GET['redirect'] + '?metode=1')
        elif request.GET['redirect'] == 'pembobotan':
            return redirect('petunjuk_pembobotan')

    elif request.method == 'GET':
        return render(request, 'sistem_penilaian/form_data.html')


def hasil_default(request):
    himpunan = Himpunan.objects.last()
    response_kategori = [
        {
            'sbr_lower': himpunan.sbr_lower,
            'sbr_middle': himpunan.sbr_middle,
            'sbr_upper': himpunan.sbr_upper,
            'br_lower': himpunan.br_lower,
            'br_middle': himpunan.br_middle,
            'br_upper': himpunan.br_upper,
            'c_lower': himpunan.c_lower,
            'c_middle': himpunan.c_middle,
            'c_upper': himpunan.c_upper,
            'b_lower': himpunan.b_lower,
            'b_middle': himpunan.b_middle,
            'b_upper': himpunan.b_upper,
            'sb_lower': himpunan.sb_lower,
            'sb_middle': himpunan.sb_middle,
            'sb_upper': himpunan.sb_upper,
        }
    ]

    pengali_max = [float(response_kategori[0]['sb_lower']), float(response_kategori[0]
                                                                  ['sb_middle']), float(response_kategori[0]['sb_upper'])]
    print(request.POST)
    bobot_l = {
        'A.1': 1, 'A.1.1': 0.116598308, 'A.1.2': 0.193171144, 'A.1.3': 0.241536864, 'A.1.4': 0.15816992,
        'B.1': 1, 'B.1.1': 0.222339392, 'B.1.2': 0.286683203,
        'B.1.3': 0.270536892,
        'C.1': 0.178077315, 'C.1.1': 0.264927625, 'C.1.2': 0.265733883, 'C.1.3': 0.233234173, 'C.2': 0.246558164, 'C.2.1': 1, 'C.3': 0.253705486, 'C.3.1': 0.459972884, 'C.3.2': 0.337263584,
        'D.1': 0.453003739, 'D.1.1': 1, 'D.2': 0.367246101, 'D.2.1': 0.32978047, 'D.2.2': 0.539612064,
        'E.1': 0.264654157, 'E.1.1': 0.3497384515, 'E.1.2': 0.436112566, 'E.2': 0.096462177, 'E.2.1': 1, 'E.3': 0.205848629, 'E.3.1': 0.323795685, 'E.3.2': 0.482953469, 'E.4': 0.123992719, 'E.4.1': 1,
        'F.1': 0.198042086, 'F.1.1': 1, 'F.2': 0.221677598, 'F.2.1': 1, 'F.3': 0.313315395, 'F.3.1': 0.477837861, 'F.3.2': 0.328821459,
        'G.1': 0.491173601, 'G.1.1': 0.178479533, 'G.1.2': 0.38639296, 'G.1.3': 0.164745378, 'G.2': 0.431507233, 'G.2.1': 1
    }

    bobot_m = {
        'A.1': 1, 'A.1.1': 0.158056289, 'A.1.2': 0.276165874, 'A.1.3': 0.344398826, 'A.1.4': 0.22137901,
        'B.1': 1, 'B.1.1': 0.280780976, 'B.1.2': 0.36928003, 'B.1.3': 0.349938994,
        'C.1': 0.253031374, 'C.1.1': 0.347889175, 'C.1.2': 0.349893369, 'C.1.3': 0.302217456, 'C.2': 0.365990903, 'C.2.1': 1, 'C.3': 0.380977723, 'C.3.1': 0.58106435, 'C.3.2': 0.41893565,
        'D.1': 0.551506226, 'D.1.1': 1, 'D.2': 0.448493774, 'D.2.1': 0.376387125, 'D.2.2': 0.623612875,
        'E.1': 0.376286873, 'E.1.1': 0.438195033, 'E.1.2': 0.561804967, 'E.2': 0.142026464, 'E.2.1': 1, 'E.3': 0.305341719, 'E.3.1': 0.398898794, 'E.3.2': 0.601101206, 'E.4': 0.176344946, 'E.4.1': 1,
        'F.1': 0.262758745, 'F.1.1': 1, 'F.2': 0.29822834, 'F.2.1': 1, 'F.3': 0.439012915, 'F.3.1': 0.596080759, 'F.3.2': 0.403919241,
        'G.1': 0.534601961, 'G.1.1': 0.234616307, 'G.1.2': 0.541918102, 'G.1.3': 0.223465591, 'G.2': 0.465398039, 'G.2.1': 1
    }

    bobot_u = {
        'A.1': 1, 'A.1.1': 0.221140842, 'A.1.2': 0.391156504, 'A.1.3': 0.482524455, 'A.1.4': 0.314668679,
        'B.1': 1, 'B.1.1': 0.361248895, 'B.1.2': 0.471907096, 'B.1.3': 0.449619752,
        'C.1': 0.369093093, 'C.1.1': 0.453595029, 'C.1.2': 0.453688952, 'C.1.3': 0.401795287, 'C.2': 0.553516171, 'C.2.1': 1, 'C.3': 0.551575623, 'C.3.1': 0.723698908, 'C.3.2': 0.530634078,
        'D.1': 0.673301338, 'D.1.1': 1, 'D.2': 0.545839405, 'D.2.1': 0.436307944, 'D.2.2': 0.713920476,
        'E.1': 0.522021096, 'E.1.1': 0.566321362, 'E.1.2': 0.706184467, 'E.2': 0.218762751, 'E.2.1': 1, 'E.3': 0.440029337, 'E.3.1': 0.497501071, 'E.3.2': 0.742041599, 'E.4': 0.266453454, 'E.4.1': 1,
        'F.1': 0.362153059, 'F.1.1': 1, 'F.2': 0.408401008, 'F.2.1': 1, 'F.3': 0.593637127, 'F.3.1': 0.734345175, 'F.3.2': 0.50533553599,
        'G.1': 0.576941816, 'G.1.1': 0.316671954, 'G.1.2': 0.734035103, 'G.1.3': 0.319873406, 'G.2': 0.50685657, 'G.2.1': 1
    }

    aspek = {
        'A_l': 0.09681058, 'A_m': 0.148958738, 'A_u': 0.223698495,
        'B_l': 0.118217416, 'B_m': 0.177561136, 'B_u': 0.26375763,
        'C_l': 0.087574644, 'C_m': 0.133894544, 'C_u': 0.207897613,
        'D_l': 0.068706821, 'D_m': 0.104658087, 'D_u': 0.165096369,
        'E_l': 0.078467709, 'E_m': 0.119261623, 'E_u': 0.183908829,
        'F_l': 0.112701761, 'F_m': 0.174625375, 'F_u': 0.270797162,
        'G_l': 0.091666786, 'G_m': 0.141040497, 'G_u': 0.213555279,
    }
    pengali = dict()

    for a in request.POST:
        if request.POST[a] == 'Sangat Buruk':
            pengali.update({a: [float(response_kategori[0]['sbr_lower']),
                                float(response_kategori[0]['sbr_middle']), float(response_kategori[0]['sbr_upper'])]})
        elif request.POST[a] == 'Buruk':
            pengali.update({a: [float(response_kategori[0]['br_lower']),
                                float(response_kategori[0]['br_middle']), float(response_kategori[0]['br_upper'])]})
        elif request.POST[a] == 'Cukup':
            pengali.update({a: [float(response_kategori[0]['c_lower']),
                                float(response_kategori[0]['c_middle']), float(response_kategori[0]['c_upper'])]})
        elif request.POST[a] == 'Baik':
            pengali.update({a: [float(response_kategori[0]['b_lower']),
                                float(response_kategori[0]['b_middle']), float(response_kategori[0]['b_upper'])]})
        elif request.POST[a] == 'Sangat Baik':
            pengali.update({a: [float(response_kategori[0]['sb_lower']),
                                float(response_kategori[0]['sb_middle']), float(response_kategori[0]['sb_upper'])]})

    # perhitugan maksimum
    jumlah_l = ((((bobot_l['A.1.1']*pengali_max[0]) + (bobot_l['A.1.2']*pengali_max[0]) + (bobot_l['A.1.3']*pengali_max[0]) + (bobot_l['A.1.4']*pengali_max[0]))*bobot_l['A.1'])*aspek['A_l']) + \
        ((((bobot_l['B.1.1']*pengali_max[0]) + (bobot_l['B.1.2']*pengali_max[0]) + (bobot_l['B.1.3']*pengali_max[0]))*bobot_l['B.1'])*aspek['B_l']) + \
        (((((bobot_l['C.1.1']*pengali_max[0]) + (bobot_l['C.1.2']*pengali_max[0]) + (bobot_l['C.1.3']*pengali_max[0]))*bobot_l['C.1']) + ((bobot_l['C.2.1']*pengali_max[0])*bobot_l['C.2']) + ((bobot_l['C.3.1']*pengali_max[0]) + (bobot_l['C.3.2']*pengali_max[0]))*bobot_l['C.3'])*aspek['C_l']) + \
        (((((bobot_l['D.1.1']*pengali_max[0]))*bobot_l['D.1']) + (((bobot_l['D.2.1']*pengali_max[0]) + (bobot_l['D.2.2']*pengali_max[0]))*bobot_l['D.2']))*aspek['D_l']) + \
        (((((bobot_l['E.1.1']*pengali_max[0]) + (bobot_l['E.1.2']*pengali_max[0]))*bobot_l['E.1']) + ((bobot_l['E.2.1']*pengali_max[0])*bobot_l['E.2']) + ((bobot_l['E.3.1']*pengali_max[0]) + (bobot_l['E.3.2']*pengali_max[0]))*bobot_l['E.3'] + ((bobot_l['E.4.1']*pengali_max[0]) * bobot_l['E.4']))*aspek['E_l']) + \
        (((((bobot_l['F.1.1']*pengali_max[0]))*bobot_l['F.1']) + ((bobot_l['F.2.1']*pengali_max[0])*bobot_l['F.2']) + ((bobot_l['F.3.1']*pengali_max[0]) + (bobot_l['F.3.2']*pengali_max[0]))*bobot_l['F.3'])*aspek['F_l']) + \
        (((((bobot_l['G.1.1']*pengali_max[0]) + (bobot_l['G.1.2']*pengali_max[0]) + (bobot_l['G.1.3'] *
                                                                                     pengali_max[0])) * bobot_l['G.1']) + ((bobot_l['G.2.1']*pengali_max[0])*bobot_l['G.2']))*aspek['G_l'])

    jumlah_m = ((((bobot_m['A.1.1']*pengali_max[1]) + (bobot_m['A.1.2']*pengali_max[1]) + (bobot_m['A.1.3']*pengali_max[1]) + (bobot_m['A.1.4']*pengali_max[1]))*bobot_m['A.1'])*aspek['A_m']) + \
        ((((bobot_m['B.1.1']*pengali_max[1]) + (bobot_m['B.1.2']*pengali_max[1]) + (bobot_m['B.1.3']*pengali_max[1]))*bobot_m['B.1'])*aspek['B_m']) + \
        (((((bobot_m['C.1.1']*pengali_max[1]) + (bobot_m['C.1.2']*pengali_max[1]) + (bobot_m['C.1.3']*pengali_max[1]))*bobot_m['C.1']) + ((bobot_m['C.2.1']*pengali_max[1])*bobot_m['C.2']) + ((bobot_m['C.3.1']*pengali_max[1]) + (bobot_m['C.3.2']*pengali_max[1]))*bobot_m['C.3'])*aspek['C_m']) + \
        (((((bobot_m['D.1.1']*pengali_max[1]))*bobot_m['D.1']) + (((bobot_m['D.2.1']*pengali_max[1]) + (bobot_m['D.2.2']*pengali_max[1]))*bobot_m['D.2']))*aspek['D_m']) + \
        (((((bobot_m['E.1.1']*pengali_max[1]) + (bobot_m['E.1.2']*pengali_max[1]))*bobot_m['E.1']) + ((bobot_m['E.2.1']*pengali_max[1])*bobot_m['E.2']) + ((bobot_m['E.3.1']*pengali_max[1]) + (bobot_m['E.3.2']*pengali_max[1]))*bobot_m['E.3'] + ((bobot_m['E.4.1']*pengali_max[1]) * bobot_m['E.4']))*aspek['E_m']) + \
        (((((bobot_m['F.1.1']*pengali_max[1]))*bobot_m['F.1']) + ((bobot_m['F.2.1']*pengali_max[1])*bobot_m['F.2']) + ((bobot_m['F.3.1']*pengali_max[1]) + (bobot_m['F.3.2']*pengali_max[1]))*bobot_m['F.3'])*aspek['F_m']) + \
        (((((bobot_m['G.1.1']*pengali_max[1]) + (bobot_m['G.1.2']*pengali_max[1]) + (bobot_m['G.1.3'] *
                                                                                     pengali_max[1])) * bobot_m['G.1']) + ((bobot_m['G.2.1']*pengali_max[1])*bobot_m['G.2']))*aspek['G_m'])

    jumlah_u = ((((bobot_u['A.1.1']*pengali_max[2]) + (bobot_u['A.1.2']*pengali_max[2]) + (bobot_u['A.1.3']*pengali_max[2]) + (bobot_u['A.1.4']*pengali_max[2]))*bobot_u['A.1'])*aspek['A_u']) + \
        ((((bobot_u['B.1.1']*pengali_max[2]) + (bobot_u['B.1.2']*pengali_max[2]) + (bobot_u['B.1.3']*pengali_max[2]))*bobot_u['B.1'])*aspek['B_u']) + \
        (((((bobot_u['C.1.1']*pengali_max[2]) + (bobot_u['C.1.2']*pengali_max[2]) + (bobot_u['C.1.3']*pengali_max[2]))*bobot_u['C.1']) + ((bobot_u['C.2.1']*pengali_max[2])*bobot_u['C.2']) + ((bobot_u['C.3.1']*pengali_max[2]) + (bobot_u['C.3.2']*pengali_max[2]))*bobot_u['C.3'])*aspek['C_u']) + \
        (((((bobot_u['D.1.1']*pengali_max[2]))*bobot_u['D.1']) + (((bobot_u['D.2.1']*pengali_max[2]) + (bobot_u['D.2.2']*pengali_max[2]))*bobot_u['D.2']))*aspek['D_u']) + \
        (((((bobot_u['E.1.1']*pengali_max[2]) + (bobot_u['E.1.2']*pengali_max[2]))*bobot_u['E.1']) + ((bobot_u['E.2.1']*pengali_max[2])*bobot_u['E.2']) + ((bobot_u['E.3.1']*pengali_max[2]) + (bobot_u['E.3.2']*pengali_max[2]))*bobot_u['E.3'] + ((bobot_u['E.4.1']*pengali_max[2]) * bobot_u['E.4']))*aspek['E_u']) + \
        (((((bobot_u['F.1.1']*pengali_max[2]))*bobot_u['F.1']) + ((bobot_u['F.2.1']*pengali_max[2])*bobot_u['F.2']) + ((bobot_u['F.3.1']*pengali_max[2]) + (bobot_u['F.3.2']*pengali_max[2]))*bobot_u['F.3'])*aspek['F_u']) + \
        (((((bobot_u['G.1.1']*pengali_max[2]) + (bobot_u['G.1.2']*pengali_max[2]) + (bobot_u['G.1.3'] *
                                                                                     pengali_max[2])) * bobot_u['G.1']) + ((bobot_u['G.2.1']*pengali_max[2])*bobot_u['G.2']))*aspek['G_u'])

    ngc_max = (jumlah_l + jumlah_m + jumlah_u) / 3

    indikator_l = [
        bobot_l['A.1.1']*pengali_max[0], bobot_l['A.1.2']*pengali_max[0], bobot_l['A.1.3'] *
        pengali_max[0], bobot_l['A.1.4']*pengali_max[0],
        bobot_l['B.1.1']*pengali_max[0], bobot_l['B.1.2'] *
        pengali_max[0], bobot_l['B.1.3']*pengali_max[0],
        bobot_l['C.1.1']*pengali_max[0], bobot_l['C.1.2']*pengali_max[0], bobot_l['C.1.3']*pengali_max[0], bobot_l['C.2.1'] *
        pengali_max[0], bobot_l['C.3.1'] *
        pengali_max[0], bobot_l['C.3.2']*pengali_max[0],
        bobot_l['D.1.1']*pengali_max[0], bobot_l['D.2.1'] *
        pengali_max[0], bobot_l['D.2.2']*pengali_max[0],
        bobot_l['E.1.1']*pengali_max[0], bobot_l['E.1.2']*pengali_max[0], bobot_l['E.2.1']*pengali_max[0], bobot_l['E.3.1'] *
        pengali_max[0], bobot_l['E.3.2'] *
        pengali_max[0], bobot_l['E.4.1']*pengali_max[0],
        bobot_l['F.1.1']*pengali_max[0], bobot_l['F.2.1']*pengali_max[0], bobot_l['F.3.1'] *
        pengali_max[0], bobot_l['F.3.2']*pengali_max[0],
        bobot_l['G.1.1']*pengali_max[0], bobot_l['G.1.2']*pengali_max[0], bobot_l['G.1.3'] *
        pengali_max[0], bobot_l['G.2.1']*pengali_max[0]
    ]

    indikator_m = [
        bobot_m['A.1.1']*pengali_max[1], bobot_m['A.1.2']*pengali_max[1], bobot_m['A.1.3'] *
        pengali_max[1], bobot_m['A.1.4']*pengali_max[1],
        bobot_m['B.1.1']*pengali_max[1], bobot_m['B.1.2'] *
        pengali_max[1], bobot_m['B.1.3']*pengali_max[1],
        bobot_m['C.1.1']*pengali_max[1], bobot_m['C.1.2']*pengali_max[1], bobot_m['C.1.3']*pengali_max[1], bobot_m['C.2.1'] *
        pengali_max[1], bobot_m['C.3.1'] *
        pengali_max[1], bobot_m['C.3.2']*pengali_max[1],
        bobot_m['D.1.1']*pengali_max[1], bobot_m['D.2.1'] *
        pengali_max[1], bobot_m['D.2.2']*pengali_max[1],
        bobot_m['E.1.1']*pengali_max[1], bobot_m['E.1.2']*pengali_max[1], bobot_m['E.2.1']*pengali_max[1], bobot_m['E.3.1'] *
        pengali_max[1], bobot_m['E.3.2'] *
        pengali_max[1], bobot_m['E.4.1']*pengali_max[1],
        bobot_m['F.1.1']*pengali_max[1], bobot_m['F.2.1']*pengali_max[1], bobot_m['F.3.1'] *
        pengali_max[1], bobot_m['F.3.2']*pengali_max[1],
        bobot_m['G.1.1']*pengali_max[1], bobot_m['G.1.2']*pengali_max[1], bobot_m['G.1.3'] *
        pengali_max[1], bobot_m['G.2.1']*pengali_max[1]
    ]

    indikator_u = [
        bobot_u['A.1.1']*pengali_max[2], bobot_u['A.1.2']*pengali_max[2], bobot_u['A.1.3'] *
        pengali_max[2], bobot_u['A.1.4']*pengali_max[2],
        bobot_u['B.1.1']*pengali_max[2], bobot_u['B.1.2'] *
        pengali_max[2], bobot_u['B.1.3']*pengali_max[2],
        bobot_u['C.1.1']*pengali_max[2], bobot_u['C.1.2']*pengali_max[2], bobot_u['C.1.3']*pengali_max[2], bobot_u['C.2.1'] *
        pengali_max[2], bobot_u['C.3.1'] *
        pengali_max[2], bobot_u['C.3.2']*pengali_max[2],
        bobot_u['D.1.1']*pengali_max[2], bobot_u['D.2.1'] *
        pengali_max[2], bobot_u['D.2.2']*pengali_max[2],
        bobot_u['E.1.1']*pengali_max[2], bobot_u['E.1.2']*pengali_max[2], bobot_u['E.2.1']*pengali_max[2], bobot_u['E.3.1'] *
        pengali_max[2], bobot_u['E.3.2'] *
        pengali_max[2], bobot_u['E.4.1']*pengali_max[2],
        bobot_u['F.1.1']*pengali_max[2], bobot_u['F.2.1']*pengali_max[2], bobot_u['F.3.1'] *
        pengali_max[2], bobot_u['F.3.2']*pengali_max[2],
        bobot_u['G.1.1']*pengali_max[2], bobot_u['G.1.2']*pengali_max[2], bobot_u['G.1.3'] *
        pengali_max[2], bobot_u['G.2.1']*pengali_max[2]
    ]

    faktor_l = [
        ((bobot_l['A.1.1']*pengali_max[0]) + (bobot_l['A.1.2']*pengali_max[0]) +
         (bobot_l['A.1.3']*pengali_max[0]) + (bobot_l['A.1.4']*pengali_max[0]))*bobot_l['A.1'],
        ((bobot_l['B.1.1']*pengali_max[0]) + (bobot_l['B.1.2']*pengali_max
                                              [0]) + (bobot_l['B.1.3']*pengali_max[0]))*bobot_l['B.1'],
        ((bobot_l['C.1.1']*pengali_max[0]) + (bobot_l['C.1.2']*pengali_max
                                              [0]) + (bobot_l['C.1.3']*pengali_max[0]))*bobot_l['C.1'],
        (bobot_l['C.2.1']*pengali_max[0])*bobot_l['C.2'],
        ((bobot_l['C.3.1']*pengali_max[0]) +
         (bobot_l['C.3.2']*pengali_max[0]))*bobot_l['C.3'],
        ((bobot_l['D.1.1']*pengali_max[0]))*bobot_l['D.1'],
        ((bobot_l['D.2.1']*pengali_max[0]) +
         (bobot_l['D.2.2']*pengali_max[0]))*bobot_l['D.2'],
        ((bobot_l['E.1.1']*pengali_max[0]) +
         (bobot_l['E.1.2']*pengali_max[0]))*bobot_l['E.1'],
        (bobot_l['E.2.1']*pengali_max[0])*bobot_l['E.2'],
        ((bobot_l['E.3.1']*pengali_max[0]) +
         (bobot_l['E.3.2']*pengali_max[0]))*bobot_l['E.3'],
        (bobot_l['E.4.1']*pengali_max[0]) * bobot_l['E.4'],
        ((bobot_l['F.1.1']*pengali_max[0]))*bobot_l['F.1'],
        (bobot_l['F.2.1']*pengali_max[0])*bobot_l['F.2'],
        ((bobot_l['F.3.1']*pengali_max[0]) +
         (bobot_l['F.3.2']*pengali_max[0]))*bobot_l['F.3'],
        ((bobot_l['G.1.1']*pengali_max[0]) + (bobot_l['G.1.2']*pengali_max
                                              [0]) + (bobot_l['G.1.3'] * pengali_max[0])) * bobot_l['G.1'],
        (bobot_l['G.2.1']*pengali_max[0])*bobot_l['G.2']
    ]

    faktor_m = [
        ((bobot_m['A.1.1']*pengali_max[1]) + (bobot_m['A.1.2']*pengali_max[1]) +
         (bobot_m['A.1.3']*pengali_max[1]) + (bobot_m['A.1.4']*pengali_max[1]))*bobot_m['A.1'],
        ((bobot_m['B.1.1']*pengali_max[1]) + (bobot_m['B.1.2']*pengali_max
                                              [1]) + (bobot_m['B.1.3']*pengali_max[1]))*bobot_m['B.1'],
        ((bobot_m['C.1.1']*pengali_max[1]) + (bobot_m['C.1.2']*pengali_max
                                              [1]) + (bobot_m['C.1.3']*pengali_max[1]))*bobot_m['C.1'],
        (bobot_m['C.2.1']*pengali_max[1])*bobot_m['C.2'],
        ((bobot_m['C.3.1']*pengali_max[1]) +
         (bobot_m['C.3.2']*pengali_max[1]))*bobot_m['C.3'],
        ((bobot_m['D.1.1']*pengali_max[1]))*bobot_m['D.1'],
        ((bobot_m['D.2.1']*pengali_max[1]) +
         (bobot_m['D.2.2']*pengali_max[1]))*bobot_m['D.2'],
        ((bobot_m['E.1.1']*pengali_max[1]) +
         (bobot_m['E.1.2']*pengali_max[1]))*bobot_m['E.1'],
        (bobot_m['E.2.1']*pengali_max[1])*bobot_m['E.2'],
        ((bobot_m['E.3.1']*pengali_max[1]) +
         (bobot_m['E.3.2']*pengali_max[1]))*bobot_m['E.3'],
        (bobot_m['E.4.1']*pengali_max[1]) * bobot_m['E.4'],
        ((bobot_m['F.1.1']*pengali_max[1]))*bobot_m['F.1'],
        (bobot_m['F.2.1']*pengali_max[1])*bobot_m['F.2'],
        ((bobot_m['F.3.1']*pengali_max[1]) +
         (bobot_m['F.3.2']*pengali_max[1]))*bobot_m['F.3'],
        ((bobot_m['G.1.1']*pengali_max[1]) + (bobot_m['G.1.2']*pengali_max
                                              [1]) + (bobot_m['G.1.3'] * pengali_max[1])) * bobot_m['G.1'],
        (bobot_m['G.2.1']*pengali_max[1])*bobot_m['G.2']
    ]

    faktor_u = [
        ((bobot_u['A.1.1']*pengali_max[2]) + (bobot_u['A.1.2']*pengali_max[2]) +
         (bobot_u['A.1.3']*pengali_max[2]) + (bobot_u['A.1.4']*pengali_max[2]))*bobot_u['A.1'],
        ((bobot_u['B.1.1']*pengali_max[2]) + (bobot_u['B.1.2']*pengali_max
                                              [2]) + (bobot_u['B.1.3']*pengali_max[2]))*bobot_u['B.1'],
        ((bobot_u['C.1.1']*pengali_max[2]) + (bobot_u['C.1.2']*pengali_max
                                              [2]) + (bobot_u['C.1.3']*pengali_max[2]))*bobot_u['C.1'],
        (bobot_u['C.2.1']*pengali_max[2])*bobot_u['C.2'],
        ((bobot_u['C.3.1']*pengali_max[2]) +
         (bobot_u['C.3.2']*pengali_max[2]))*bobot_u['C.3'],
        ((bobot_u['D.1.1']*pengali_max[2]))*bobot_u['D.1'],
        ((bobot_u['D.2.1']*pengali_max[2]) +
         (bobot_u['D.2.2']*pengali_max[2]))*bobot_u['D.2'],
        ((bobot_u['E.1.1']*pengali_max[2]) +
         (bobot_u['E.1.2']*pengali_max[2]))*bobot_u['E.1'],
        (bobot_u['E.2.1']*pengali_max[2])*bobot_u['E.2'],
        ((bobot_u['E.3.1']*pengali_max[2]) +
         (bobot_u['E.3.2']*pengali_max[2]))*bobot_u['E.3'],
        (bobot_u['E.4.1']*pengali_max[2]) * bobot_u['E.4'],
        ((bobot_u['F.1.1']*pengali_max[2]))*bobot_u['F.1'],
        (bobot_u['F.2.1']*pengali_max[2])*bobot_u['F.2'],
        ((bobot_u['F.3.1']*pengali_max[2]) +
         (bobot_u['F.3.2']*pengali_max[2]))*bobot_u['F.3'],
        ((bobot_u['G.1.1']*pengali_max[2]) + (bobot_u['G.1.2']*pengali_max
                                              [2]) + (bobot_u['G.1.3'] * pengali_max[2])) * bobot_u['G.1'],
        (bobot_u['G.2.1']*pengali_max[2])*bobot_u['G.2']
    ]

    aspek_l = [
        ((((bobot_l['A.1.1']*pengali_max[0]) + (bobot_l['A.1.2']*pengali_max[0]) + (bobot_l['A.1.3']
                                                                                    * pengali_max[0]) + (bobot_l['A.1.4']*pengali_max[0]))*bobot_l['A.1'])*aspek['A_l']),
        ((((bobot_l['B.1.1']*pengali_max[0]) + (bobot_l['B.1.2']*pengali_max
                                                [0]) + (bobot_l['B.1.3']*pengali_max[0]))*bobot_l['B.1'])*aspek['B_l']),
        (((((bobot_l['C.1.1']*pengali_max[0]) + (bobot_l['C.1.2']*pengali_max[0]) + (bobot_l['C.1.3']*pengali_max[0]))*bobot_l['C.1']) + ((bobot_l['C.2.1']
                                                                                                                                           * pengali_max[0])*bobot_l['C.2']) + ((bobot_l['C.3.1']*pengali_max[0]) + (bobot_l['C.3.2']*pengali_max[0]))*bobot_l['C.3'])*aspek['C_l']),
        (((((bobot_l['D.1.1']*pengali_max[0]))*bobot_l['D.1']) + (((bobot_l['D.2.1'] *
                                                                    pengali_max[0]) + (bobot_l['D.2.2']*pengali_max[0]))*bobot_l['D.2']))*aspek['D_l']),
        (((((bobot_l['E.1.1']*pengali_max[0]) + (bobot_l['E.1.2']*pengali_max[0]))*bobot_l['E.1']) + ((bobot_l['E.2.1']*pengali_max[0])*bobot_l['E.2']) + (
            (bobot_l['E.3.1']*pengali_max[0]) + (bobot_l['E.3.2']*pengali_max[0]))*bobot_l['E.3'] + ((bobot_l['E.4.1']*pengali_max[0]) * bobot_l['E.4']))*aspek['E_l']),
        (((((bobot_l['F.1.1']*pengali_max[0]))*bobot_l['F.1']) + ((bobot_l['F.2.1']*pengali_max[0])*bobot_l['F.2']
                                                                  ) + ((bobot_l['F.3.1']*pengali_max[0]) + (bobot_l['F.3.2']*pengali_max[0]))*bobot_l['F.3'])*aspek['F_l']),
        (((((bobot_l['G.1.1']*pengali_max[0]) + (bobot_l['G.1.2']*pengali_max[0]) + (bobot_l['G.1.3'] *
                                                                                     pengali_max[0])) * bobot_l['G.1']) + ((bobot_l['G.2.1']*pengali_max[0])*bobot_l['G.2']))*aspek['G_l'])]

    aspek_m = [
        ((((bobot_m['A.1.1']*pengali_max[1]) + (bobot_m['A.1.2']*pengali_max[1]) + (bobot_m['A.1.3']
                                                                                    * pengali_max[1]) + (bobot_m['A.1.4']*pengali_max[1]))*bobot_m['A.1'])*aspek['A_m']),
        ((((bobot_m['B.1.1']*pengali_max[1]) + (bobot_m['B.1.2']*pengali_max
                                                [1]) + (bobot_m['B.1.3']*pengali_max[1]))*bobot_m['B.1'])*aspek['B_m']),
        (((((bobot_m['C.1.1']*pengali_max[1]) + (bobot_m['C.1.2']*pengali_max[1]) + (bobot_m['C.1.3']*pengali_max[1]))*bobot_m['C.1']) + ((bobot_m['C.2.1']
                                                                                                                                           * pengali_max[1])*bobot_m['C.2']) + ((bobot_m['C.3.1']*pengali_max[1]) + (bobot_m['C.3.2']*pengali_max[1]))*bobot_m['C.3'])*aspek['C_m']),
        (((((bobot_m['D.1.1']*pengali_max[1]))*bobot_m['D.1']) + (((bobot_m['D.2.1'] *
                                                                    pengali_max[1]) + (bobot_m['D.2.2']*pengali_max[1]))*bobot_m['D.2']))*aspek['D_m']),
        (((((bobot_m['E.1.1']*pengali_max[1]) + (bobot_m['E.1.2']*pengali_max[1]))*bobot_m['E.1']) + ((bobot_m['E.2.1']*pengali_max[1])*bobot_m['E.2']) +
          ((bobot_m['E.3.1']*pengali_max[1]) + (bobot_m['E.3.2']*pengali_max[1]))*bobot_m['E.3'] + ((bobot_m['E.4.1']*pengali_max[1]) * bobot_m['E.4']))*aspek['E_m']),
        (((((bobot_m['F.1.1']*pengali_max[1]))*bobot_m['F.1']) + ((bobot_m['F.2.1']*pengali_max[1])*bobot_m['F.2']
                                                                  ) + ((bobot_m['F.3.1']*pengali_max[1]) + (bobot_m['F.3.2']*pengali_max[1]))*bobot_m['F.3'])*aspek['F_m']),
        (((((bobot_m['G.1.1']*pengali_max[1]) + (bobot_m['G.1.2']*pengali_max[1]) + (bobot_m['G.1.3'] *
                                                                                     pengali_max[1])) * bobot_m['G.1']) + ((bobot_m['G.2.1']*pengali_max[1])*bobot_m['G.2']))*aspek['G_m'])
    ]

    aspek_u = [
        ((((bobot_u['A.1.1']*pengali_max[2]) + (bobot_u['A.1.2']*pengali_max[2]) + (bobot_u['A.1.3']
                                                                                    * pengali_max[2]) + (bobot_u['A.1.4']*pengali_max[2]))*bobot_u['A.1'])*aspek['A_u']),
        ((((bobot_u['B.1.1']*pengali_max[2]) + (bobot_u['B.1.2']*pengali_max
                                                [2]) + (bobot_u['B.1.3']*pengali_max[2]))*bobot_u['B.1'])*aspek['B_u']),
        (((((bobot_u['C.1.1']*pengali_max[2]) + (bobot_u['C.1.2']*pengali_max[2]) + (bobot_u['C.1.3']*pengali_max[2]))*bobot_u['C.1']) + ((bobot_u['C.2.1']
                                                                                                                                           * pengali_max[2])*bobot_u['C.2']) + ((bobot_u['C.3.1']*pengali_max[2]) + (bobot_u['C.3.2']*pengali_max[2]))*bobot_u['C.3'])*aspek['C_u']),
        (((((bobot_u['D.1.1']*pengali_max[2]))*bobot_u['D.1']) + (((bobot_u['D.2.1'] *
                                                                    pengali_max[2]) + (bobot_u['D.2.2']*pengali_max[2]))*bobot_u['D.2']))*aspek['D_u']),
        (((((bobot_u['E.1.1']*pengali_max[2]) + (bobot_u['E.1.2']*pengali_max[2]))*bobot_u['E.1']) + ((bobot_u['E.2.1']*pengali_max[2])*bobot_u['E.2']) + (
            (bobot_u['E.3.1']*pengali_max[2]) + (bobot_u['E.3.2']*pengali_max[2]))*bobot_u['E.3'] + ((bobot_u['E.4.1']*pengali_max[2]) * bobot_u['E.4']))*aspek['E_u']),
        (((((bobot_u['F.1.1']*pengali_max[2]))*bobot_u['F.1']) + ((bobot_u['F.2.1']*pengali_max[2])*bobot_u['F.2']
                                                                  ) + ((bobot_u['F.3.1']*pengali_max[2]) + (bobot_u['F.3.2']*pengali_max[2]))*bobot_u['F.3'])*aspek['F_u']),
        (((((bobot_u['G.1.1']*pengali_max[2]) + (bobot_u['G.1.2']*pengali_max[2]) + (bobot_u['G.1.3'] *
                                                                                     pengali_max[2])) * bobot_u['G.1']) + ((bobot_u['G.2.1']*pengali_max[2])*bobot_u['G.2']))*aspek['G_u'])]
    max_indikator = [
        (indikator_l[0] + indikator_m[0] +
         indikator_u[0])/3,
        (indikator_l[1] + indikator_m[1] +
         indikator_u[1])/3,
        (indikator_l[2] + indikator_m[2] +
         indikator_u[2])/3,
        (indikator_l[3] + indikator_m[3] +
         indikator_u[3])/3,
        (indikator_l[4] + indikator_m[4] +
         indikator_u[4])/3,
        (indikator_l[5] + indikator_m[5] +
         indikator_u[5])/3,
        (indikator_l[6] + indikator_m[6] +
         indikator_u[6])/3,
        (indikator_l[7] + indikator_m[7] +
         indikator_u[7])/3,
        (indikator_l[8] + indikator_m[8] +
         indikator_u[8])/3,
        (indikator_l[9] + indikator_m[9] +
         indikator_u[9])/3,
        (indikator_l[10] + indikator_m[10] +
         indikator_u[10])/3,
        (indikator_l[11] + indikator_m[11] +
         indikator_u[11])/3,
        (indikator_l[12] + indikator_m[12] +
         indikator_u[12])/3,
        (indikator_l[13] + indikator_m[13] +
         indikator_u[13])/3,
        (indikator_l[14] + indikator_m[14] +
         indikator_u[14])/3,
        (indikator_l[15] + indikator_m[15] +
         indikator_u[15])/3,
        (indikator_l[16] + indikator_m[16] +
         indikator_u[16])/3,
        (indikator_l[17] + indikator_m[17] +
         indikator_u[17])/3,
        (indikator_l[18] + indikator_m[18] +
         indikator_u[18])/3,
        (indikator_l[19] + indikator_m[19] +
         indikator_u[19])/3,
        (indikator_l[20] + indikator_m[20] +
         indikator_u[20])/3,
        (indikator_l[21] + indikator_m[21] +
         indikator_u[21])/3,
        (indikator_l[22] + indikator_m[22] +
         indikator_u[22])/3,
        (indikator_l[23] + indikator_m[23] +
         indikator_u[23])/3,
        (indikator_l[24] + indikator_m[24] +
         indikator_u[24])/3,
        (indikator_l[25] + indikator_m[25] +
         indikator_u[25])/3,
        (indikator_l[26] + indikator_m[26] +
         indikator_u[26])/3,
        (indikator_l[27] + indikator_m[27] +
         indikator_u[27])/3,
        (indikator_l[28] + indikator_m[28] +
         indikator_u[28])/3,
        (indikator_l[29] + indikator_m[29] +
         indikator_u[29])/3,
    ]

    max_faktor = [
        (faktor_l[0] + faktor_m[0] + faktor_u[0])/3,
        (faktor_l[1] + faktor_m[1] + faktor_u[1])/3,
        (faktor_l[2] + faktor_m[2] + faktor_u[2])/3,
        (faktor_l[3] + faktor_m[3] + faktor_u[3])/3,
        (faktor_l[4] + faktor_m[4] + faktor_u[4])/3,
        (faktor_l[5] + faktor_m[5] + faktor_u[5])/3,
        (faktor_l[6] + faktor_m[6] + faktor_u[6])/3,
        (faktor_l[7] + faktor_m[7] + faktor_u[7])/3,
        (faktor_l[8] + faktor_m[8] + faktor_u[8])/3,
        (faktor_l[9] + faktor_m[9] + faktor_u[9])/3,
        (faktor_l[10] + faktor_m[10] + faktor_u[10])/3,
        (faktor_l[11] + faktor_m[11] + faktor_u[11])/3,
        (faktor_l[12] + faktor_m[12] + faktor_u[12])/3,
        (faktor_l[13] + faktor_m[13] + faktor_u[13])/3,
        (faktor_l[14] + faktor_m[14] + faktor_u[14])/3,
        (faktor_l[15] + faktor_m[15] + faktor_u[15])/3
    ]
    max_aspek = [
        (aspek_l[0] + aspek_m[0] + aspek_u[0])/3,
        (aspek_l[1] + aspek_m[1] + aspek_u[1])/3,
        (aspek_l[2] + aspek_m[2] + aspek_u[2])/3,
        (aspek_l[3] + aspek_m[3] + aspek_u[3])/3,
        (aspek_l[4] + aspek_m[4] + aspek_u[4])/3,
        (aspek_l[5] + aspek_m[5] + aspek_u[5])/3,
        (aspek_l[6] + aspek_m[6] + aspek_u[6])/3
    ]

    # perhitungan input
    jumlah_l = ((((bobot_l['A.1.1']*pengali['A.1.1'][0]) + (bobot_l['A.1.2']*pengali['A.1.2'][0]) + (bobot_l['A.1.3']*pengali['A.1.3'][0]) + (bobot_l['A.1.4']*pengali['A.1.4'][0]))*bobot_l['A.1'])*aspek['A_l']) + \
        ((((bobot_l['B.1.1']*pengali['B.1.1'][0]) + (bobot_l['B.1.2']*pengali['B.1.2'][0]) + (bobot_l['B.1.3']*pengali['B.1.3'][0]))*bobot_l['B.1'])*aspek['B_l']) + \
        (((((bobot_l['C.1.1']*pengali['C.1.1'][0]) + (bobot_l['C.1.2']*pengali['C.1.2'][0]) + (bobot_l['C.1.3']*pengali['C.1.3'][0]))*bobot_l['C.1']) + ((bobot_l['C.2.1']*pengali['C.2.1'][0])*bobot_l['C.2']) + ((bobot_l['C.3.1']*pengali['C.3.1'][0]) + (bobot_l['C.3.2']*pengali['C.3.2'][0]))*bobot_l['C.3'])*aspek['C_l']) + \
        (((((bobot_l['D.1.1']*pengali['D.1.1'][0]))*bobot_l['D.1']) + (((bobot_l['D.2.1']*pengali['D.2.1'][0]) + (bobot_l['D.2.2']*pengali['D.2.2'][0]))*bobot_l['D.2']))*aspek['D_l']) + \
        (((((bobot_l['E.1.1']*pengali['E.1.1'][0]) + (bobot_l['E.1.2']*pengali['E.1.2'][0]))*bobot_l['E.1']) + ((bobot_l['E.2.1']*pengali['E.2.1'][0])*bobot_l['E.2']) + ((bobot_l['E.3.1']*pengali['E.3.1'][0]) + (bobot_l['E.3.2']*pengali['E.3.2'][0]))*bobot_l['E.3'] + ((bobot_l['E.4.1']*pengali['E.4.1'][0]) * bobot_l['E.4']))*aspek['E_l']) + \
        (((((bobot_l['F.1.1']*pengali['F.1.1'][0]))*bobot_l['F.1']) + ((bobot_l['F.2.1']*pengali['F.2.1'][0])*bobot_l['F.2']) + ((bobot_l['F.3.1']*pengali['F.3.1'][0]) + (bobot_l['F.3.2']*pengali['F.3.2'][0]))*bobot_l['F.3'])*aspek['F_l']) + \
        (((((bobot_l['G.1.1']*pengali['G.1.1'][0]) + (bobot_l['G.1.2']*pengali['G.1.2'][0]) + (bobot_l['G.1.3'] *
                                                                                               pengali['G.1.3'][0])) * bobot_l['G.1']) + ((bobot_l['G.2.1']*pengali['G.2.1'][0])*bobot_l['G.2']))*aspek['G_l'])

    jumlah_m = ((((bobot_m['A.1.1']*pengali['A.1.1'][1]) + (bobot_m['A.1.2']*pengali['A.1.2'][1]) + (bobot_m['A.1.3']*pengali['A.1.3'][1]) + (bobot_m['A.1.4']*pengali['A.1.4'][1]))*bobot_m['A.1'])*aspek['A_m']) + \
        ((((bobot_m['B.1.1']*pengali['B.1.1'][1]) + (bobot_m['B.1.2']*pengali['B.1.2'][1]) + (bobot_m['B.1.3']*pengali['B.1.3'][1]))*bobot_m['B.1'])*aspek['B_m']) + \
        (((((bobot_m['C.1.1']*pengali['C.1.1'][1]) + (bobot_m['C.1.2']*pengali['C.1.2'][1]) + (bobot_m['C.1.3']*pengali['C.1.3'][1]))*bobot_m['C.1']) + ((bobot_m['C.2.1']*pengali['C.2.1'][1])*bobot_m['C.2']) + ((bobot_m['C.3.1']*pengali['C.3.1'][1]) + (bobot_m['C.3.2']*pengali['C.3.2'][1]))*bobot_m['C.3'])*aspek['C_m']) + \
        (((((bobot_m['D.1.1']*pengali['D.1.1'][1]))*bobot_m['D.1']) + (((bobot_m['D.2.1']*pengali['D.2.1'][1]) + (bobot_m['D.2.2']*pengali['D.2.2'][1]))*bobot_m['D.2']))*aspek['D_m']) + \
        (((((bobot_m['E.1.1']*pengali['E.1.1'][1]) + (bobot_m['E.1.2']*pengali['E.1.2'][1]))*bobot_m['E.1']) + ((bobot_m['E.2.1']*pengali['E.2.1'][1])*bobot_m['E.2']) + ((bobot_m['E.3.1']*pengali['E.3.1'][1]) + (bobot_m['E.3.2']*pengali['E.3.2'][1]))*bobot_m['E.3'] + ((bobot_m['E.4.1']*pengali['E.4.1'][1]) * bobot_m['E.4']))*aspek['E_m']) + \
        (((((bobot_m['F.1.1']*pengali['F.1.1'][1]))*bobot_m['F.1']) + ((bobot_m['F.2.1']*pengali['F.2.1'][1])*bobot_m['F.2']) + ((bobot_m['F.3.1']*pengali['F.3.1'][1]) + (bobot_m['F.3.2']*pengali['F.3.2'][1]))*bobot_m['F.3'])*aspek['F_m']) + \
        (((((bobot_m['G.1.1']*pengali['G.1.1'][1]) + (bobot_m['G.1.2']*pengali['G.1.2'][1]) + (bobot_m['G.1.3'] *
                                                                                               pengali['G.1.3'][1])) * bobot_m['G.1']) + ((bobot_m['G.2.1']*pengali['G.2.1'][1])*bobot_m['G.2']))*aspek['G_m'])

    jumlah_u = ((((bobot_u['A.1.1']*pengali['A.1.1'][2]) + (bobot_u['A.1.2']*pengali['A.1.2'][2]) + (bobot_u['A.1.3']*pengali['A.1.3'][2]) + (bobot_u['A.1.4']*pengali['A.1.4'][2]))*bobot_u['A.1'])*aspek['A_u']) + \
        ((((bobot_u['B.1.1']*pengali['B.1.1'][2]) + (bobot_u['B.1.2']*pengali['B.1.2'][2]) + (bobot_u['B.1.3']*pengali['B.1.3'][2]))*bobot_u['B.1'])*aspek['B_u']) + \
        (((((bobot_u['C.1.1']*pengali['C.1.1'][2]) + (bobot_u['C.1.2']*pengali['C.1.2'][2]) + (bobot_u['C.1.3']*pengali['C.1.3'][2]))*bobot_u['C.1']) + ((bobot_u['C.2.1']*pengali['C.2.1'][2])*bobot_u['C.2']) + ((bobot_u['C.3.1']*pengali['C.3.1'][2]) + (bobot_u['C.3.2']*pengali['C.3.2'][2]))*bobot_u['C.3'])*aspek['C_u']) + \
        (((((bobot_u['D.1.1']*pengali['D.1.1'][2]))*bobot_u['D.1']) + (((bobot_u['D.2.1']*pengali['D.2.1'][2]) + (bobot_u['D.2.2']*pengali['D.2.2'][2]))*bobot_u['D.2']))*aspek['D_u']) + \
        (((((bobot_u['E.1.1']*pengali['E.1.1'][2]) + (bobot_u['E.1.2']*pengali['E.1.2'][2]))*bobot_u['E.1']) + ((bobot_u['E.2.1']*pengali['E.2.1'][2])*bobot_u['E.2']) + ((bobot_u['E.3.1']*pengali['E.3.1'][2]) + (bobot_u['E.3.2']*pengali['E.3.2'][2]))*bobot_u['E.3'] + ((bobot_u['E.4.1']*pengali['E.4.1'][2]) * bobot_u['E.4']))*aspek['E_u']) + \
        (((((bobot_u['F.1.1']*pengali['F.1.1'][2]))*bobot_u['F.1']) + ((bobot_u['F.2.1']*pengali['F.2.1'][2])*bobot_u['F.2']) + ((bobot_u['F.3.1']*pengali['F.3.1'][2]) + (bobot_u['F.3.2']*pengali['F.3.2'][2]))*bobot_u['F.3'])*aspek['F_u']) + \
        (((((bobot_u['G.1.1']*pengali['G.1.1'][2]) + (bobot_u['G.1.2']*pengali['G.1.2'][2]) + (bobot_u['G.1.3'] *
                                                                                               pengali['G.1.3'][2])) * bobot_u['G.1']) + ((bobot_u['G.2.1']*pengali['G.2.1'][2])*bobot_u['G.2']))*aspek['G_u'])

    indikator_l = [
        bobot_l['A.1.1']*pengali['A.1.1'][0], bobot_l['A.1.2']*pengali['A.1.2'][0], bobot_l['A.1.3'] *
        pengali['A.1.3'][0], bobot_l['A.1.4']*pengali['A.1.4'][0],
        bobot_l['B.1.1']*pengali['B.1.1'][0], bobot_l['B.1.2'] *
        pengali['B.1.2'][0], bobot_l['B.1.3']*pengali['B.1.3'][0],
        bobot_l['C.1.1']*pengali['C.1.1'][0], bobot_l['C.1.2']*pengali['C.1.2'][0], bobot_l['C.1.3']*pengali['C.1.3'][0], bobot_l['C.2.1'] *
        pengali['C.2.1'][0], bobot_l['C.3.1'] *
        pengali['C.3.1'][0], bobot_l['C.3.2']*pengali['C.3.2'][0],
        bobot_l['D.1.1']*pengali['D.1.1'][0], bobot_l['D.2.1'] *
        pengali['D.2.1'][0], bobot_l['D.2.2']*pengali['D.2.2'][0],
        bobot_l['E.1.1']*pengali['E.1.1'][0], bobot_l['E.1.2']*pengali['E.1.2'][0], bobot_l['E.2.1']*pengali['E.2.1'][0], bobot_l['E.3.1'] *
        pengali['E.3.1'][0], bobot_l['E.3.2'] *
        pengali['E.3.2'][0], bobot_l['E.4.1']*pengali['E.4.1'][0],
        bobot_l['F.1.1']*pengali['F.1.1'][0], bobot_l['F.2.1']*pengali['F.2.1'][0], bobot_l['F.3.1'] *
        pengali['F.3.1'][0], bobot_l['F.3.2']*pengali['F.3.2'][0],
        bobot_l['G.1.1']*pengali['G.1.1'][0], bobot_l['G.1.2']*pengali['G.1.2'][0], bobot_l['G.1.3'] *
        pengali['G.1.3'][0], bobot_l['G.2.1']*pengali['G.2.1'][0]
    ]

    indikator_m = [
        bobot_m['A.1.1']*pengali['A.1.1'][1], bobot_m['A.1.2']*pengali['A.1.2'][1], bobot_m['A.1.3'] *
        pengali['A.1.3'][1], bobot_m['A.1.4']*pengali['A.1.4'][1],
        bobot_m['B.1.1']*pengali['B.1.1'][1], bobot_m['B.1.2'] *
        pengali['B.1.2'][1], bobot_m['B.1.3']*pengali['B.1.3'][1],
        bobot_m['C.1.1']*pengali['C.1.1'][1], bobot_m['C.1.2']*pengali['C.1.2'][1], bobot_m['C.1.3']*pengali['C.1.3'][1], bobot_m['C.2.1'] *
        pengali['C.2.1'][1], bobot_m['C.3.1'] *
        pengali['C.3.1'][1], bobot_m['C.3.2']*pengali['C.3.2'][1],
        bobot_m['D.1.1']*pengali['D.1.1'][1], bobot_m['D.2.1'] *
        pengali['D.2.1'][1], bobot_m['D.2.2']*pengali['D.2.2'][1],
        bobot_m['E.1.1']*pengali['E.1.1'][1], bobot_m['E.1.2']*pengali['E.1.2'][1], bobot_m['E.2.1']*pengali['E.2.1'][1], bobot_m['E.3.1'] *
        pengali['E.3.1'][1], bobot_m['E.3.2'] *
        pengali['E.3.2'][1], bobot_m['E.4.1']*pengali['E.4.1'][1],
        bobot_m['F.1.1']*pengali['F.1.1'][1], bobot_m['F.2.1']*pengali['F.2.1'][1], bobot_m['F.3.1'] *
        pengali['F.3.1'][1], bobot_m['F.3.2']*pengali['F.3.2'][1],
        bobot_m['G.1.1']*pengali['G.1.1'][1], bobot_m['G.1.2']*pengali['G.1.2'][1], bobot_m['G.1.3'] *
        pengali['G.1.3'][1], bobot_m['G.2.1']*pengali['G.2.1'][1]
    ]

    indikator_u = [
        bobot_u['A.1.1']*pengali['A.1.1'][2], bobot_u['A.1.2']*pengali['A.1.2'][2], bobot_u['A.1.3'] *
        pengali['A.1.3'][2], bobot_u['A.1.4']*pengali['A.1.4'][2],
        bobot_u['B.1.1']*pengali['B.1.1'][2], bobot_u['B.1.2'] *
        pengali['B.1.2'][2], bobot_u['B.1.3']*pengali['B.1.3'][2],
        bobot_u['C.1.1']*pengali['C.1.1'][2], bobot_u['C.1.2']*pengali['C.1.2'][2], bobot_u['C.1.3']*pengali['C.1.3'][2], bobot_u['C.2.1'] *
        pengali['C.2.1'][2], bobot_u['C.3.1'] *
        pengali['C.3.1'][2], bobot_u['C.3.2']*pengali['C.3.2'][2],
        bobot_u['D.1.1']*pengali['D.1.1'][2], bobot_u['D.2.1'] *
        pengali['D.2.1'][2], bobot_u['D.2.2']*pengali['D.2.2'][2],
        bobot_u['E.1.1']*pengali['E.1.1'][2], bobot_u['E.1.2']*pengali['E.1.2'][2], bobot_u['E.2.1']*pengali['E.2.1'][2], bobot_u['E.3.1'] *
        pengali['E.3.1'][2], bobot_u['E.3.2'] *
        pengali['E.3.2'][2], bobot_u['E.4.1']*pengali['E.4.1'][2],
        bobot_u['F.1.1']*pengali['F.1.1'][2], bobot_u['F.2.1']*pengali['F.2.1'][2], bobot_u['F.3.1'] *
        pengali['F.3.1'][2], bobot_u['F.3.2']*pengali['F.3.2'][2],
        bobot_u['G.1.1']*pengali['G.1.1'][2], bobot_u['G.1.2']*pengali['G.1.2'][2], bobot_u['G.1.3'] *
        pengali['G.1.3'][2], bobot_u['G.2.1']*pengali['G.2.1'][2]
    ]

    faktor_l = [
        ((bobot_l['A.1.1']*pengali['A.1.1'][0]) + (bobot_l['A.1.2']*pengali['A.1.2'][0]) +
         (bobot_l['A.1.3']*pengali['A.1.3'][0]) + (bobot_l['A.1.4']*pengali['A.1.4'][0]))*bobot_l['A.1'],
        ((bobot_l['B.1.1']*pengali['B.1.1'][0]) + (bobot_l['B.1.2']*pengali['B.1.2']
                                                   [0]) + (bobot_l['B.1.3']*pengali['B.1.3'][0]))*bobot_l['B.1'],
        ((bobot_l['C.1.1']*pengali['C.1.1'][0]) + (bobot_l['C.1.2']*pengali['C.1.2']
                                                   [0]) + (bobot_l['C.1.3']*pengali['C.1.3'][0]))*bobot_l['C.1'],
        (bobot_l['C.2.1']*pengali['C.2.1'][0])*bobot_l['C.2'],
        ((bobot_l['C.3.1']*pengali['C.3.1'][0]) +
         (bobot_l['C.3.2']*pengali['C.3.2'][0]))*bobot_l['C.3'],
        ((bobot_l['D.1.1']*pengali['D.1.1'][0]))*bobot_l['D.1'],
        ((bobot_l['D.2.1']*pengali['D.2.1'][0]) +
         (bobot_l['D.2.2']*pengali['D.2.2'][0]))*bobot_l['D.2'],
        ((bobot_l['E.1.1']*pengali['E.1.1'][0]) +
         (bobot_l['E.1.2']*pengali['E.1.2'][0]))*bobot_l['E.1'],
        (bobot_l['E.2.1']*pengali['E.2.1'][0])*bobot_l['E.2'],
        ((bobot_l['E.3.1']*pengali['E.3.1'][0]) +
         (bobot_l['E.3.2']*pengali['E.3.2'][0]))*bobot_l['E.3'],
        (bobot_l['E.4.1']*pengali['E.4.1'][0]) * bobot_l['E.4'],
        ((bobot_l['F.1.1']*pengali['F.1.1'][0]))*bobot_l['F.1'],
        (bobot_l['F.2.1']*pengali['F.2.1'][0])*bobot_l['F.2'],
        ((bobot_l['F.3.1']*pengali['F.3.1'][0]) +
         (bobot_l['F.3.2']*pengali['F.3.2'][0]))*bobot_l['F.3'],
        ((bobot_l['G.1.1']*pengali['G.1.1'][0]) + (bobot_l['G.1.2']*pengali['G.1.2']
                                                   [0]) + (bobot_l['G.1.3'] * pengali['G.1.3'][0])) * bobot_l['G.1'],
        (bobot_l['G.2.1']*pengali['G.2.1'][0])*bobot_l['G.2']
    ]

    faktor_m = [
        ((bobot_m['A.1.1']*pengali['A.1.1'][1]) + (bobot_m['A.1.2']*pengali['A.1.2'][1]) +
         (bobot_m['A.1.3']*pengali['A.1.3'][1]) + (bobot_m['A.1.4']*pengali['A.1.4'][1]))*bobot_m['A.1'],
        ((bobot_m['B.1.1']*pengali['B.1.1'][1]) + (bobot_m['B.1.2']*pengali['B.1.2']
                                                   [1]) + (bobot_m['B.1.3']*pengali['B.1.3'][1]))*bobot_m['B.1'],
        ((bobot_m['C.1.1']*pengali['C.1.1'][1]) + (bobot_m['C.1.2']*pengali['C.1.2']
                                                   [1]) + (bobot_m['C.1.3']*pengali['C.1.3'][1]))*bobot_m['C.1'],
        (bobot_m['C.2.1']*pengali['C.2.1'][1])*bobot_m['C.2'],
        ((bobot_m['C.3.1']*pengali['C.3.1'][1]) +
         (bobot_m['C.3.2']*pengali['C.3.2'][1]))*bobot_m['C.3'],
        ((bobot_m['D.1.1']*pengali['D.1.1'][1]))*bobot_m['D.1'],
        ((bobot_m['D.2.1']*pengali['D.2.1'][1]) +
         (bobot_m['D.2.2']*pengali['D.2.2'][1]))*bobot_m['D.2'],
        ((bobot_m['E.1.1']*pengali['E.1.1'][1]) +
         (bobot_m['E.1.2']*pengali['E.1.2'][1]))*bobot_m['E.1'],
        (bobot_m['E.2.1']*pengali['E.2.1'][1])*bobot_m['E.2'],
        ((bobot_m['E.3.1']*pengali['E.3.1'][1]) +
         (bobot_m['E.3.2']*pengali['E.3.2'][1]))*bobot_m['E.3'],
        (bobot_m['E.4.1']*pengali['E.4.1'][1]) * bobot_m['E.4'],
        ((bobot_m['F.1.1']*pengali['F.1.1'][1]))*bobot_m['F.1'],
        (bobot_m['F.2.1']*pengali['F.2.1'][1])*bobot_m['F.2'],
        ((bobot_m['F.3.1']*pengali['F.3.1'][1]) +
         (bobot_m['F.3.2']*pengali['F.3.2'][1]))*bobot_m['F.3'],
        ((bobot_m['G.1.1']*pengali['G.1.1'][1]) + (bobot_m['G.1.2']*pengali['G.1.2']
                                                   [1]) + (bobot_m['G.1.3'] * pengali['G.1.3'][1])) * bobot_m['G.1'],
        (bobot_m['G.2.1']*pengali['G.2.1'][1])*bobot_m['G.2']
    ]

    faktor_u = [
        ((bobot_u['A.1.1']*pengali['A.1.1'][2]) + (bobot_u['A.1.2']*pengali['A.1.2'][2]) +
         (bobot_u['A.1.3']*pengali['A.1.3'][2]) + (bobot_u['A.1.4']*pengali['A.1.4'][2]))*bobot_u['A.1'],
        ((bobot_u['B.1.1']*pengali['B.1.1'][2]) + (bobot_u['B.1.2']*pengali['B.1.2']
                                                   [2]) + (bobot_u['B.1.3']*pengali['B.1.3'][2]))*bobot_u['B.1'],
        ((bobot_u['C.1.1']*pengali['C.1.1'][2]) + (bobot_u['C.1.2']*pengali['C.1.2']
                                                   [2]) + (bobot_u['C.1.3']*pengali['C.1.3'][2]))*bobot_u['C.1'],
        (bobot_u['C.2.1']*pengali['C.2.1'][2])*bobot_u['C.2'],
        ((bobot_u['C.3.1']*pengali['C.3.1'][2]) +
         (bobot_u['C.3.2']*pengali['C.3.2'][2]))*bobot_u['C.3'],
        ((bobot_u['D.1.1']*pengali['D.1.1'][2]))*bobot_u['D.1'],
        ((bobot_u['D.2.1']*pengali['D.2.1'][2]) +
         (bobot_u['D.2.2']*pengali['D.2.2'][2]))*bobot_u['D.2'],
        ((bobot_u['E.1.1']*pengali['E.1.1'][2]) +
         (bobot_u['E.1.2']*pengali['E.1.2'][2]))*bobot_u['E.1'],
        (bobot_u['E.2.1']*pengali['E.2.1'][2])*bobot_u['E.2'],
        ((bobot_u['E.3.1']*pengali['E.3.1'][2]) +
         (bobot_u['E.3.2']*pengali['E.3.2'][2]))*bobot_u['E.3'],
        (bobot_u['E.4.1']*pengali['E.4.1'][2]) * bobot_u['E.4'],
        ((bobot_u['F.1.1']*pengali['F.1.1'][2]))*bobot_u['F.1'],
        (bobot_u['F.2.1']*pengali['F.2.1'][2])*bobot_u['F.2'],
        ((bobot_u['F.3.1']*pengali['F.3.1'][2]) +
         (bobot_u['F.3.2']*pengali['F.3.2'][2]))*bobot_u['F.3'],
        ((bobot_u['G.1.1']*pengali['G.1.1'][2]) + (bobot_u['G.1.2']*pengali['G.1.2']
                                                   [2]) + (bobot_u['G.1.3'] * pengali['G.1.3'][2])) * bobot_u['G.1'],
        (bobot_u['G.2.1']*pengali['G.2.1'][2])*bobot_u['G.2']
    ]

    aspek_l = [
        ((((bobot_l['A.1.1']*pengali['A.1.1'][0]) + (bobot_l['A.1.2']*pengali['A.1.2'][0]) + (bobot_l['A.1.3']
                                                                                              * pengali['A.1.3'][0]) + (bobot_l['A.1.4']*pengali['A.1.4'][0]))*bobot_l['A.1'])*aspek['A_l']),
        ((((bobot_l['B.1.1']*pengali['B.1.1'][0]) + (bobot_l['B.1.2']*pengali['B.1.2']
                                                     [0]) + (bobot_l['B.1.3']*pengali['B.1.3'][0]))*bobot_l['B.1'])*aspek['B_l']),
        (((((bobot_l['C.1.1']*pengali['C.1.1'][0]) + (bobot_l['C.1.2']*pengali['C.1.2'][0]) + (bobot_l['C.1.3']*pengali['C.1.3'][0]))*bobot_l['C.1']) + ((bobot_l['C.2.1']
                                                                                                                                                          * pengali['C.2.1'][0])*bobot_l['C.2']) + ((bobot_l['C.3.1']*pengali['C.3.1'][0]) + (bobot_l['C.3.2']*pengali['C.3.2'][0]))*bobot_l['C.3'])*aspek['C_l']),
        (((((bobot_l['D.1.1']*pengali['D.1.1'][0]))*bobot_l['D.1']) + (((bobot_l['D.2.1'] *
                                                                         pengali['D.2.1'][0]) + (bobot_l['D.2.2']*pengali['D.2.2'][0]))*bobot_l['D.2']))*aspek['D_l']),
        (((((bobot_l['E.1.1']*pengali['E.1.1'][0]) + (bobot_l['E.1.2']*pengali['E.1.2'][0]))*bobot_l['E.1']) + ((bobot_l['E.2.1']*pengali['E.2.1'][0])*bobot_l['E.2']) + (
            (bobot_l['E.3.1']*pengali['E.3.1'][0]) + (bobot_l['E.3.2']*pengali['E.3.2'][0]))*bobot_l['E.3'] + ((bobot_l['E.4.1']*pengali['E.4.1'][0]) * bobot_l['E.4']))*aspek['E_l']),
        (((((bobot_l['F.1.1']*pengali['F.1.1'][0]))*bobot_l['F.1']) + ((bobot_l['F.2.1']*pengali['F.2.1'][0])*bobot_l['F.2']
                                                                       ) + ((bobot_l['F.3.1']*pengali['F.3.1'][0]) + (bobot_l['F.3.2']*pengali['F.3.2'][0]))*bobot_l['F.3'])*aspek['F_l']),
        (((((bobot_l['G.1.1']*pengali['G.1.1'][0]) + (bobot_l['G.1.2']*pengali['G.1.2'][0]) + (bobot_l['G.1.3'] *
                                                                                               pengali['G.1.3'][0])) * bobot_l['G.1']) + ((bobot_l['G.2.1']*pengali['G.2.1'][0])*bobot_l['G.2']))*aspek['G_l'])]

    aspek_m = [
        ((((bobot_m['A.1.1']*pengali['A.1.1'][1]) + (bobot_m['A.1.2']*pengali['A.1.2'][1]) + (bobot_m['A.1.3']
                                                                                              * pengali['A.1.3'][1]) + (bobot_m['A.1.4']*pengali['A.1.4'][1]))*bobot_m['A.1'])*aspek['A_m']),
        ((((bobot_m['B.1.1']*pengali['B.1.1'][1]) + (bobot_m['B.1.2']*pengali['B.1.2']
                                                     [1]) + (bobot_m['B.1.3']*pengali['B.1.3'][1]))*bobot_m['B.1'])*aspek['B_m']),
        (((((bobot_m['C.1.1']*pengali['C.1.1'][1]) + (bobot_m['C.1.2']*pengali['C.1.2'][1]) + (bobot_m['C.1.3']*pengali['C.1.3'][1]))*bobot_m['C.1']) + ((bobot_m['C.2.1']
                                                                                                                                                          * pengali['C.2.1'][1])*bobot_m['C.2']) + ((bobot_m['C.3.1']*pengali['C.3.1'][1]) + (bobot_m['C.3.2']*pengali['C.3.2'][1]))*bobot_m['C.3'])*aspek['C_m']),
        (((((bobot_m['D.1.1']*pengali['D.1.1'][1]))*bobot_m['D.1']) + (((bobot_m['D.2.1'] *
                                                                         pengali['D.2.1'][1]) + (bobot_m['D.2.2']*pengali['D.2.2'][1]))*bobot_m['D.2']))*aspek['D_m']),
        (((((bobot_m['E.1.1']*pengali['E.1.1'][1]) + (bobot_m['E.1.2']*pengali['E.1.2'][1]))*bobot_m['E.1']) + ((bobot_m['E.2.1']*pengali['E.2.1'][1])*bobot_m['E.2']) +
          ((bobot_m['E.3.1']*pengali['E.3.1'][1]) + (bobot_m['E.3.2']*pengali['E.3.2'][1]))*bobot_m['E.3'] + ((bobot_m['E.4.1']*pengali['E.4.1'][1]) * bobot_m['E.4']))*aspek['E_m']),
        (((((bobot_m['F.1.1']*pengali['F.1.1'][1]))*bobot_m['F.1']) + ((bobot_m['F.2.1']*pengali['F.2.1'][1])*bobot_m['F.2']
                                                                       ) + ((bobot_m['F.3.1']*pengali['F.3.1'][1]) + (bobot_m['F.3.2']*pengali['F.3.2'][1]))*bobot_m['F.3'])*aspek['F_m']),
        (((((bobot_m['G.1.1']*pengali['G.1.1'][1]) + (bobot_m['G.1.2']*pengali['G.1.2'][1]) + (bobot_m['G.1.3'] *
                                                                                               pengali['G.1.3'][1])) * bobot_m['G.1']) + ((bobot_m['G.2.1']*pengali['G.2.1'][1])*bobot_m['G.2']))*aspek['G_m'])
    ]

    aspek_u = [
        ((((bobot_u['A.1.1']*pengali['A.1.1'][2]) + (bobot_u['A.1.2']*pengali['A.1.2'][2]) + (bobot_u['A.1.3']
                                                                                              * pengali['A.1.3'][2]) + (bobot_u['A.1.4']*pengali['A.1.4'][2]))*bobot_u['A.1'])*aspek['A_u']),
        ((((bobot_u['B.1.1']*pengali['B.1.1'][2]) + (bobot_u['B.1.2']*pengali['B.1.2']
                                                     [2]) + (bobot_u['B.1.3']*pengali['B.1.3'][2]))*bobot_u['B.1'])*aspek['B_u']),
        (((((bobot_u['C.1.1']*pengali['C.1.1'][2]) + (bobot_u['C.1.2']*pengali['C.1.2'][2]) + (bobot_u['C.1.3']*pengali['C.1.3'][2]))*bobot_u['C.1']) + ((bobot_u['C.2.1']
                                                                                                                                                          * pengali['C.2.1'][2])*bobot_u['C.2']) + ((bobot_u['C.3.1']*pengali['C.3.1'][2]) + (bobot_u['C.3.2']*pengali['C.3.2'][2]))*bobot_u['C.3'])*aspek['C_u']),
        (((((bobot_u['D.1.1']*pengali['D.1.1'][2]))*bobot_u['D.1']) + (((bobot_u['D.2.1'] *
                                                                         pengali['D.2.1'][2]) + (bobot_u['D.2.2']*pengali['D.2.2'][2]))*bobot_u['D.2']))*aspek['D_u']),
        (((((bobot_u['E.1.1']*pengali['E.1.1'][2]) + (bobot_u['E.1.2']*pengali['E.1.2'][2]))*bobot_u['E.1']) + ((bobot_u['E.2.1']*pengali['E.2.1'][2])*bobot_u['E.2']) + (
            (bobot_u['E.3.1']*pengali['E.3.1'][2]) + (bobot_u['E.3.2']*pengali['E.3.2'][2]))*bobot_u['E.3'] + ((bobot_u['E.4.1']*pengali['E.4.1'][2]) * bobot_u['E.4']))*aspek['E_u']),
        (((((bobot_u['F.1.1']*pengali['F.1.1'][2]))*bobot_u['F.1']) + ((bobot_u['F.2.1']*pengali['F.2.1'][2])*bobot_u['F.2']
                                                                       ) + ((bobot_u['F.3.1']*pengali['F.3.1'][2]) + (bobot_u['F.3.2']*pengali['F.3.2'][2]))*bobot_u['F.3'])*aspek['F_u']),
        (((((bobot_u['G.1.1']*pengali['G.1.1'][2]) + (bobot_u['G.1.2']*pengali['G.1.2'][2]) + (bobot_u['G.1.3'] *
                                                                                               pengali['G.1.3'][2])) * bobot_u['G.1']) + ((bobot_u['G.2.1']*pengali['G.2.1'][2])*bobot_u['G.2']))*aspek['G_u'])]
    output_indikator = [
        100*((indikator_l[0] + indikator_m[0] +
              indikator_u[0])/3)/max_indikator[0],
        100*((indikator_l[1] + indikator_m[1] +
              indikator_u[1])/3)/max_indikator[1],
        100*((indikator_l[2] + indikator_m[2] +
              indikator_u[2])/3)/max_indikator[2],
        100*((indikator_l[3] + indikator_m[3] +
              indikator_u[3])/3)/max_indikator[3],
        100*((indikator_l[4] + indikator_m[4] +
              indikator_u[4])/3)/max_indikator[4],
        100*((indikator_l[5] + indikator_m[5] +
              indikator_u[5])/3)/max_indikator[5],
        100*((indikator_l[6] + indikator_m[6] +
              indikator_u[6])/3)/max_indikator[6],
        100*((indikator_l[7] + indikator_m[7] +
              indikator_u[7])/3)/max_indikator[7],
        100*((indikator_l[8] + indikator_m[8] +
              indikator_u[8])/3)/max_indikator[8],
        100*((indikator_l[9] + indikator_m[9] +
              indikator_u[9])/3)/max_indikator[9],
        100*((indikator_l[10] + indikator_m[10] +
              indikator_u[10])/3)/max_indikator[10],
        100*((indikator_l[11] + indikator_m[11] +
              indikator_u[11])/3)/max_indikator[11],
        100*((indikator_l[12] + indikator_m[12] +
              indikator_u[12])/3)/max_indikator[12],
        100*((indikator_l[13] + indikator_m[13] +
              indikator_u[13])/3)/max_indikator[13],
        100*((indikator_l[14] + indikator_m[14] +
              indikator_u[14])/3)/max_indikator[14],
        100*((indikator_l[15] + indikator_m[15] +
              indikator_u[15])/3)/max_indikator[15],
        100*((indikator_l[16] + indikator_m[16] +
              indikator_u[16])/3)/max_indikator[16],
        100*((indikator_l[17] + indikator_m[17] +
              indikator_u[17])/3)/max_indikator[17],
        100*((indikator_l[18] + indikator_m[18] +
              indikator_u[18])/3)/max_indikator[18],
        100*((indikator_l[19] + indikator_m[19] +
              indikator_u[19])/3)/max_indikator[19],
        100*((indikator_l[20] + indikator_m[20] +
              indikator_u[20])/3)/max_indikator[20],
        100*((indikator_l[21] + indikator_m[21] +
              indikator_u[21])/3)/max_indikator[21],
        100*((indikator_l[22] + indikator_m[22] +
              indikator_u[22])/3)/max_indikator[22],
        100*((indikator_l[23] + indikator_m[23] +
              indikator_u[23])/3)/max_indikator[23],
        100*((indikator_l[24] + indikator_m[24] +
              indikator_u[24])/3)/max_indikator[24],
        100*((indikator_l[25] + indikator_m[25] +
              indikator_u[25])/3)/max_indikator[25],
        100*((indikator_l[26] + indikator_m[26] +
              indikator_u[26])/3)/max_indikator[26],
        100*((indikator_l[27] + indikator_m[27] +
              indikator_u[27])/3)/max_indikator[27],
        100*((indikator_l[28] + indikator_m[28] +
              indikator_u[28])/3)/max_indikator[28],
        100*((indikator_l[29] + indikator_m[29] +
              indikator_u[29])/3)/max_indikator[29],
    ]

    output_faktor = [
        100*((faktor_l[0] + faktor_m[0] + faktor_u[0])/3)/max_faktor[0],
        100*((faktor_l[1] + faktor_m[1] + faktor_u[1])/3)/max_faktor[1],
        100*((faktor_l[2] + faktor_m[2] + faktor_u[2])/3)/max_faktor[2],
        100*((faktor_l[3] + faktor_m[3] + faktor_u[3])/3)/max_faktor[3],
        100*((faktor_l[4] + faktor_m[4] + faktor_u[4])/3)/max_faktor[4],
        100*((faktor_l[5] + faktor_m[5] + faktor_u[5])/3)/max_faktor[5],
        100*((faktor_l[6] + faktor_m[6] + faktor_u[6])/3)/max_faktor[6],
        100*((faktor_l[7] + faktor_m[7] + faktor_u[7])/3)/max_faktor[7],
        100*((faktor_l[8] + faktor_m[8] + faktor_u[8])/3)/max_faktor[8],
        100*((faktor_l[9] + faktor_m[9] + faktor_u[9])/3)/max_faktor[9],
        100*((faktor_l[10] + faktor_m[10] + faktor_u[10])/3)/max_faktor[10],
        100*((faktor_l[11] + faktor_m[11] + faktor_u[11])/3)/max_faktor[11],
        100*((faktor_l[12] + faktor_m[12] + faktor_u[12])/3)/max_faktor[12],
        100*((faktor_l[13] + faktor_m[13] + faktor_u[13])/3)/max_faktor[13],
        100*((faktor_l[14] + faktor_m[14] + faktor_u[14])/3)/max_faktor[14],
        100*((faktor_l[15] + faktor_m[15] + faktor_u[15])/3)/max_faktor[15]
    ]
    output_aspek = [
        round(100*((aspek_l[0] + aspek_m[0] + aspek_u[0])/3)/max_aspek[0], 2),
        round(100*((aspek_l[1] + aspek_m[1] + aspek_u[1])/3)/max_aspek[1], 2),
        round(100*((aspek_l[2] + aspek_m[2] + aspek_u[2])/3)/max_aspek[2], 2),
        round(100*((aspek_l[3] + aspek_m[3] + aspek_u[3])/3)/max_aspek[3], 2),
        round(100*((aspek_l[4] + aspek_m[4] + aspek_u[4])/3)/max_aspek[4], 2),
        round(100*((aspek_l[5] + aspek_m[5] + aspek_u[5])/3)/max_aspek[5], 2),
        round(100*((aspek_l[6] + aspek_m[6] + aspek_u[6])/3)/max_aspek[6], 2)
    ]

    # Nilai GC
    print('jumlah_l : {}'.format(jumlah_l))
    print('jumlah_m : {}'.format(jumlah_m))
    print('jumlah_u : {}'.format(jumlah_u))
    nilai_gc = (jumlah_l + jumlah_m + jumlah_u)/3
    # ngc_max = 116.9679854
    nilai_gc = round(100*nilai_gc/ngc_max)
    print('nilai GC : {}'.format(nilai_gc))

    # for output in output_aspek:
    #     nilai_gc += output

    category = nilai_gc_category(nilai_gc)
    proyek = Proyek.objects.filter(
        pk=request.session['proyek_id']).update(nilai_gc=nilai_gc)

    # return render(request, 'sistem_penilaian/hasil.html', {'output_indikator': output_indikator, 'output_faktor': output_faktor, 'output_aspek': output_aspek, 'max_value': max_value})
    return render(request, 'sistem_penilaian/hasil_default.html', {'output_aspek': output_aspek, 'output_faktor': output_faktor, 'output_indikator': output_indikator, 'nilai_gc': nilai_gc, 'nilai_gc_text': category["text"],
                                                                   'nilai_gc_percent': category["percent"]
                                                                   })


def hasil_user(request):
    aspek_by_user = []
    faktor_by_user = []
    indikator_by_user = []
    himpunan = Himpunan.objects.last()
    response_kategori = [
        {
            'sbr_lower': himpunan.sbr_lower,
            'sbr_middle': himpunan.sbr_middle,
            'sbr_upper': himpunan.sbr_upper,
            'br_lower': himpunan.br_lower,
            'br_middle': himpunan.br_middle,
            'br_upper': himpunan.br_upper,
            'c_lower': himpunan.c_lower,
            'c_middle': himpunan.c_middle,
            'c_upper': himpunan.c_upper,
            'b_lower': himpunan.b_lower,
            'b_middle': himpunan.b_middle,
            'b_upper': himpunan.b_upper,
            'sb_lower': himpunan.sb_lower,
            'sb_middle': himpunan.sb_middle,
            'sb_upper': himpunan.sb_upper,
        }
    ]

    # nilai aspek
    aspek = Aspek_user.objects.last()
    aspek_by_user.append(aspek.A.split(','))
    aspek_by_user.append(aspek.B.split(','))
    aspek_by_user.append(aspek.C.split(','))
    aspek_by_user.append(aspek.D.split(','))
    aspek_by_user.append(aspek.E.split(','))
    aspek_by_user.append(aspek.F.split(','))
    aspek_by_user.append(aspek.G.split(','))

    # nilai faktor
    faktor = Faktor_user.objects.last()
    faktor_by_user.append(faktor.f1.split(','))
    faktor_by_user.append(faktor.f2.split(','))
    faktor_by_user.append(faktor.f3.split(','))
    faktor_by_user.append(faktor.f4.split(','))
    faktor_by_user.append(faktor.f5.split(','))
    faktor_by_user.append(faktor.f6.split(','))
    faktor_by_user.append(faktor.f7.split(','))
    faktor_by_user.append(faktor.f8.split(','))
    faktor_by_user.append(faktor.f9.split(','))
    faktor_by_user.append(faktor.f10.split(','))
    faktor_by_user.append(faktor.f11.split(','))
    faktor_by_user.append(faktor.f12.split(','))
    faktor_by_user.append(faktor.f13.split(','))
    faktor_by_user.append(faktor.f14.split(','))
    faktor_by_user.append(faktor.f15.split(','))
    faktor_by_user.append(faktor.f16.split(','))

    # nilai indikator
    indikator = Indikator_user.objects.last()
    indikator_by_user.append(indikator.i1.split(','))
    indikator_by_user.append(indikator.i2.split(','))
    indikator_by_user.append(indikator.i3.split(','))
    indikator_by_user.append(indikator.i4.split(','))
    indikator_by_user.append(indikator.i5.split(','))
    indikator_by_user.append(indikator.i6.split(','))
    indikator_by_user.append(indikator.i7.split(','))
    indikator_by_user.append(indikator.i8.split(','))
    indikator_by_user.append(indikator.i9.split(','))
    indikator_by_user.append(indikator.i10.split(','))
    indikator_by_user.append(indikator.i11.split(','))
    indikator_by_user.append(indikator.i12.split(','))
    indikator_by_user.append(indikator.i13.split(','))
    indikator_by_user.append(indikator.i14.split(','))
    indikator_by_user.append(indikator.i15.split(','))
    indikator_by_user.append(indikator.i16.split(','))
    indikator_by_user.append(indikator.i17.split(','))
    indikator_by_user.append(indikator.i18.split(','))
    indikator_by_user.append(indikator.i19.split(','))
    indikator_by_user.append(indikator.i20.split(','))
    indikator_by_user.append(indikator.i21.split(','))
    indikator_by_user.append(indikator.i22.split(','))
    indikator_by_user.append(indikator.i23.split(','))
    indikator_by_user.append(indikator.i24.split(','))
    indikator_by_user.append(indikator.i25.split(','))
    indikator_by_user.append(indikator.i26.split(','))
    indikator_by_user.append(indikator.i27.split(','))
    indikator_by_user.append(indikator.i28.split(','))
    indikator_by_user.append(indikator.i29.split(','))
    indikator_by_user.append(indikator.i30.split(','))

    bobot_l = {
        'A.1': float(faktor_by_user[0][0]), 'A.1.1': float(indikator_by_user[0][0]), 'A.1.2': float(indikator_by_user[1][0]), 'A.1.3': float(indikator_by_user[2][0]), 'A.1.4': float(indikator_by_user[3][0]),
        'B.1': float(faktor_by_user[1][0]), 'B.1.1': float(indikator_by_user[4][0]), 'B.1.2': float(indikator_by_user[5][0]),
        'B.1.3': float(indikator_by_user[6][0]),
        'C.1': float(faktor_by_user[2][0]), 'C.1.1': float(indikator_by_user[7][0]), 'C.1.2': float(indikator_by_user[8][0]), 'C.1.3': float(indikator_by_user[9][0]), 'C.2': float(faktor_by_user[3][0]), 'C.2.1': float(indikator_by_user[10][0]), 'C.3': float(faktor_by_user[4][0]), 'C.3.1': float(indikator_by_user[11][0]), 'C.3.2': float(indikator_by_user[12][0]),
        'D.1': float(faktor_by_user[4][0]), 'D.1.1': float(indikator_by_user[13][0]), 'D.2': float(faktor_by_user[5][0]), 'D.2.1': float(indikator_by_user[14][0]), 'D.2.2': float(indikator_by_user[15][0]),
        'E.1': float(faktor_by_user[6][0]), 'E.1.1': float(indikator_by_user[16][0]), 'E.1.2': float(indikator_by_user[17][0]), 'E.2': float(faktor_by_user[7][0]), 'E.2.1': float(indikator_by_user[18][0]), 'E.3': float(faktor_by_user[8][0]), 'E.3.1': float(indikator_by_user[19][0]), 'E.3.2': float(indikator_by_user[20][0]), 'E.4': float(faktor_by_user[9][0]), 'E.4.1': float(indikator_by_user[21][0]),
        'F.1': float(faktor_by_user[10][0]), 'F.1.1': float(indikator_by_user[22][0]), 'F.2': float(faktor_by_user[11][0]), 'F.2.1': float(indikator_by_user[23][0]), 'F.3': float(faktor_by_user[12][0]), 'F.3.1': float(indikator_by_user[24][0]), 'F.3.2': float(indikator_by_user[25][0]),
        'G.1': float(faktor_by_user[14][0]), 'G.1.1': float(indikator_by_user[26][0]), 'G.1.2': float(indikator_by_user[27][0]), 'G.1.3': float(indikator_by_user[28][0]), 'G.2': float(faktor_by_user[15][0]), 'G.2.1': float(indikator_by_user[29][0])
    }

    bobot_m = {
        'A.1': float(faktor_by_user[0][1]), 'A.1.1': float(indikator_by_user[0][1]), 'A.1.2': float(indikator_by_user[1][1]), 'A.1.3': float(indikator_by_user[2][1]), 'A.1.4': float(indikator_by_user[3][1]),
        'B.1': float(faktor_by_user[1][1]), 'B.1.1': float(indikator_by_user[4][1]), 'B.1.2': float(indikator_by_user[5][1]),
        'B.1.3': float(indikator_by_user[6][1]),
        'C.1': float(faktor_by_user[2][1]), 'C.1.1': float(indikator_by_user[7][1]), 'C.1.2': float(indikator_by_user[8][1]), 'C.1.3': float(indikator_by_user[9][1]), 'C.2': float(faktor_by_user[3][1]), 'C.2.1': float(indikator_by_user[10][1]), 'C.3': float(faktor_by_user[4][1]), 'C.3.1': float(indikator_by_user[11][1]), 'C.3.2': float(indikator_by_user[12][1]),
        'D.1': float(faktor_by_user[4][1]), 'D.1.1': float(indikator_by_user[13][1]), 'D.2': float(faktor_by_user[5][1]), 'D.2.1': float(indikator_by_user[14][1]), 'D.2.2': float(indikator_by_user[15][1]),
        'E.1': float(faktor_by_user[6][1]), 'E.1.1': float(indikator_by_user[16][1]), 'E.1.2': float(indikator_by_user[17][1]), 'E.2': float(faktor_by_user[7][1]), 'E.2.1': float(indikator_by_user[18][1]), 'E.3': float(faktor_by_user[8][1]), 'E.3.1': float(indikator_by_user[19][1]), 'E.3.2': float(indikator_by_user[20][1]), 'E.4': float(faktor_by_user[9][1]), 'E.4.1': float(indikator_by_user[21][1]),
        'F.1': float(faktor_by_user[10][1]), 'F.1.1': float(indikator_by_user[22][1]), 'F.2': float(faktor_by_user[11][1]), 'F.2.1': float(indikator_by_user[23][1]), 'F.3': float(faktor_by_user[12][1]), 'F.3.1': float(indikator_by_user[24][1]), 'F.3.2': float(indikator_by_user[25][1]),
        'G.1': float(faktor_by_user[14][1]), 'G.1.1': float(indikator_by_user[26][1]), 'G.1.2': float(indikator_by_user[27][1]), 'G.1.3': float(indikator_by_user[28][1]), 'G.2': float(faktor_by_user[15][1]), 'G.2.1': float(indikator_by_user[29][1])
    }

    bobot_u = {
        'A.1': float(faktor_by_user[0][2]), 'A.1.1': float(indikator_by_user[0][2]), 'A.1.2': float(indikator_by_user[1][2]), 'A.1.3': float(indikator_by_user[2][2]), 'A.1.4': float(indikator_by_user[3][2]),
        'B.1': float(faktor_by_user[1][2]), 'B.1.1': float(indikator_by_user[4][2]), 'B.1.2': float(indikator_by_user[5][2]),
        'B.1.3': float(indikator_by_user[6][2]),
        'C.1': float(faktor_by_user[2][2]), 'C.1.1': float(indikator_by_user[7][2]), 'C.1.2': float(indikator_by_user[8][2]), 'C.1.3': float(indikator_by_user[9][2]), 'C.2': float(faktor_by_user[3][2]), 'C.2.1': float(indikator_by_user[10][2]), 'C.3': float(faktor_by_user[4][2]), 'C.3.1': float(indikator_by_user[11][2]), 'C.3.2': float(indikator_by_user[12][2]),
        'D.1': float(faktor_by_user[4][2]), 'D.1.1': float(indikator_by_user[13][2]), 'D.2': float(faktor_by_user[5][2]), 'D.2.1': float(indikator_by_user[14][2]), 'D.2.2': float(indikator_by_user[15][2]),
        'E.1': float(faktor_by_user[6][2]), 'E.1.1': float(indikator_by_user[16][2]), 'E.1.2': float(indikator_by_user[17][2]), 'E.2': float(faktor_by_user[7][2]), 'E.2.1': float(indikator_by_user[18][2]), 'E.3': float(faktor_by_user[8][2]), 'E.3.1': float(indikator_by_user[19][2]), 'E.3.2': float(indikator_by_user[20][2]), 'E.4': float(faktor_by_user[9][2]), 'E.4.1': float(indikator_by_user[21][2]),
        'F.1': float(faktor_by_user[10][2]), 'F.1.1': float(indikator_by_user[22][2]), 'F.2': float(faktor_by_user[11][2]), 'F.2.1': float(indikator_by_user[23][2]), 'F.3': float(faktor_by_user[12][2]), 'F.3.1': float(indikator_by_user[24][2]), 'F.3.2': float(indikator_by_user[25][2]),
        'G.1': float(faktor_by_user[14][2]), 'G.1.1': float(indikator_by_user[26][2]), 'G.1.2': float(indikator_by_user[27][2]), 'G.1.3': float(indikator_by_user[28][2]), 'G.2': float(faktor_by_user[15][2]), 'G.2.1': float(indikator_by_user[29][2])
    }

    aspek = {
        'A_l': float(aspek_by_user[0][0]), 'A_m': float(aspek_by_user[0][1]), 'A_u': float(aspek_by_user[0][2]),
        'B_l': float(aspek_by_user[1][0]), 'B_m': float(aspek_by_user[1][1]), 'B_u': float(aspek_by_user[1][2]),
        'C_l': float(aspek_by_user[2][0]), 'C_m': float(aspek_by_user[2][1]), 'C_u': float(aspek_by_user[2][2]),
        'D_l': float(aspek_by_user[3][0]), 'D_m': float(aspek_by_user[3][1]), 'D_u': float(aspek_by_user[3][2]),
        'E_l': float(aspek_by_user[4][0]), 'E_m': float(aspek_by_user[4][1]), 'E_u': float(aspek_by_user[4][2]),
        'F_l': float(aspek_by_user[5][0]), 'F_m': float(aspek_by_user[5][1]), 'F_u': float(aspek_by_user[5][2]),
        'G_l': float(aspek_by_user[6][0]), 'G_m': float(aspek_by_user[6][1]), 'G_u': float(aspek_by_user[6][2])
    }
    pengali = dict()

    for a in request.POST:
        if request.POST[a] == 'Sangat Buruk':
            pengali.update({a: [float(response_kategori[0]['sbr_lower']),
                                float(response_kategori[0]['sbr_middle']), float(response_kategori[0]['sbr_upper'])]})
        elif request.POST[a] == 'Buruk':
            pengali.update({a: [float(response_kategori[0]['br_lower']),
                                float(response_kategori[0]['br_middle']), float(response_kategori[0]['br_upper'])]})
        elif request.POST[a] == 'Cukup':
            pengali.update({a: [float(response_kategori[0]['c_lower']),
                                float(response_kategori[0]['c_middle']), float(response_kategori[0]['c_upper'])]})
        elif request.POST[a] == 'Baik':
            pengali.update({a: [float(response_kategori[0]['b_lower']),
                                float(response_kategori[0]['b_middle']), float(response_kategori[0]['b_upper'])]})
        elif request.POST[a] == 'Sangat Baik':
            pengali.update({a: [float(response_kategori[0]['sb_lower']),
                                float(response_kategori[0]['sb_middle']), float(response_kategori[0]['sb_upper'])]})

    # perhitungan nilai maksimum
    pengali_max = [float(response_kategori[0]['sb_lower']), float(response_kategori[0]
                                                                  ['sb_middle']), float(response_kategori[0]['sb_upper'])]

    jumlah_l = ((((bobot_l['A.1.1']*pengali_max[0]) + (bobot_l['A.1.2']*pengali_max[0]) + (bobot_l['A.1.3']*pengali_max[0]) + (bobot_l['A.1.4']*pengali_max[0]))*bobot_l['A.1'])*aspek['A_l']) + \
        ((((bobot_l['B.1.1']*pengali_max[0]) + (bobot_l['B.1.2']*pengali_max[0]) + (bobot_l['B.1.3']*pengali_max[0]))*bobot_l['B.1'])*aspek['B_l']) + \
        (((((bobot_l['C.1.1']*pengali_max[0]) + (bobot_l['C.1.2']*pengali_max[0]) + (bobot_l['C.1.3']*pengali_max[0]))*bobot_l['C.1']) + ((bobot_l['C.2.1']*pengali_max[0])*bobot_l['C.2']) + ((bobot_l['C.3.1']*pengali_max[0]) + (bobot_l['C.3.2']*pengali_max[0]))*bobot_l['C.3'])*aspek['C_l']) + \
        (((((bobot_l['D.1.1']*pengali_max[0]))*bobot_l['D.1']) + (((bobot_l['D.2.1']*pengali_max[0]) + (bobot_l['D.2.2']*pengali_max[0]))*bobot_l['D.2']))*aspek['D_l']) + \
        (((((bobot_l['E.1.1']*pengali_max[0]) + (bobot_l['E.1.2']*pengali_max[0]))*bobot_l['E.1']) + ((bobot_l['E.2.1']*pengali_max[0])*bobot_l['E.2']) + ((bobot_l['E.3.1']*pengali_max[0]) + (bobot_l['E.3.2']*pengali_max[0]))*bobot_l['E.3'] + ((bobot_l['E.4.1']*pengali_max[0]) * bobot_l['E.4']))*aspek['E_l']) + \
        (((((bobot_l['F.1.1']*pengali_max[0]))*bobot_l['F.1']) + ((bobot_l['F.2.1']*pengali_max[0])*bobot_l['F.2']) + ((bobot_l['F.3.1']*pengali_max[0]) + (bobot_l['F.3.2']*pengali_max[0]))*bobot_l['F.3'])*aspek['F_l']) + \
        (((((bobot_l['G.1.1']*pengali_max[0]) + (bobot_l['G.1.2']*pengali_max[0]) + (bobot_l['G.1.3'] *
                                                                                     pengali_max[0])) * bobot_l['G.1']) + ((bobot_l['G.2.1']*pengali_max[0])*bobot_l['G.2']))*aspek['G_l'])

    jumlah_m = ((((bobot_m['A.1.1']*pengali_max[1]) + (bobot_m['A.1.2']*pengali_max[1]) + (bobot_m['A.1.3']*pengali_max[1]) + (bobot_m['A.1.4']*pengali_max[1]))*bobot_m['A.1'])*aspek['A_m']) + \
        ((((bobot_m['B.1.1']*pengali_max[1]) + (bobot_m['B.1.2']*pengali_max[1]) + (bobot_m['B.1.3']*pengali_max[1]))*bobot_m['B.1'])*aspek['B_m']) + \
        (((((bobot_m['C.1.1']*pengali_max[1]) + (bobot_m['C.1.2']*pengali_max[1]) + (bobot_m['C.1.3']*pengali_max[1]))*bobot_m['C.1']) + ((bobot_m['C.2.1']*pengali_max[1])*bobot_m['C.2']) + ((bobot_m['C.3.1']*pengali_max[1]) + (bobot_m['C.3.2']*pengali_max[1]))*bobot_m['C.3'])*aspek['C_m']) + \
        (((((bobot_m['D.1.1']*pengali_max[1]))*bobot_m['D.1']) + (((bobot_m['D.2.1']*pengali_max[1]) + (bobot_m['D.2.2']*pengali_max[1]))*bobot_m['D.2']))*aspek['D_m']) + \
        (((((bobot_m['E.1.1']*pengali_max[1]) + (bobot_m['E.1.2']*pengali_max[1]))*bobot_m['E.1']) + ((bobot_m['E.2.1']*pengali_max[1])*bobot_m['E.2']) + ((bobot_m['E.3.1']*pengali_max[1]) + (bobot_m['E.3.2']*pengali_max[1]))*bobot_m['E.3'] + ((bobot_m['E.4.1']*pengali_max[1]) * bobot_m['E.4']))*aspek['E_m']) + \
        (((((bobot_m['F.1.1']*pengali_max[1]))*bobot_m['F.1']) + ((bobot_m['F.2.1']*pengali_max[1])*bobot_m['F.2']) + ((bobot_m['F.3.1']*pengali_max[1]) + (bobot_m['F.3.2']*pengali_max[1]))*bobot_m['F.3'])*aspek['F_m']) + \
        (((((bobot_m['G.1.1']*pengali_max[1]) + (bobot_m['G.1.2']*pengali_max[1]) + (bobot_m['G.1.3'] *
                                                                                     pengali_max[1])) * bobot_m['G.1']) + ((bobot_m['G.2.1']*pengali_max[1])*bobot_m['G.2']))*aspek['G_m'])

    jumlah_u = ((((bobot_u['A.1.1']*pengali_max[2]) + (bobot_u['A.1.2']*pengali_max[2]) + (bobot_u['A.1.3']*pengali_max[2]) + (bobot_u['A.1.4']*pengali_max[2]))*bobot_u['A.1'])*aspek['A_u']) + \
        ((((bobot_u['B.1.1']*pengali_max[2]) + (bobot_u['B.1.2']*pengali_max[2]) + (bobot_u['B.1.3']*pengali_max[2]))*bobot_u['B.1'])*aspek['B_u']) + \
        (((((bobot_u['C.1.1']*pengali_max[2]) + (bobot_u['C.1.2']*pengali_max[2]) + (bobot_u['C.1.3']*pengali_max[2]))*bobot_u['C.1']) + ((bobot_u['C.2.1']*pengali_max[2])*bobot_u['C.2']) + ((bobot_u['C.3.1']*pengali_max[2]) + (bobot_u['C.3.2']*pengali_max[2]))*bobot_u['C.3'])*aspek['C_u']) + \
        (((((bobot_u['D.1.1']*pengali_max[2]))*bobot_u['D.1']) + (((bobot_u['D.2.1']*pengali_max[2]) + (bobot_u['D.2.2']*pengali_max[2]))*bobot_u['D.2']))*aspek['D_u']) + \
        (((((bobot_u['E.1.1']*pengali_max[2]) + (bobot_u['E.1.2']*pengali_max[2]))*bobot_u['E.1']) + ((bobot_u['E.2.1']*pengali_max[2])*bobot_u['E.2']) + ((bobot_u['E.3.1']*pengali_max[2]) + (bobot_u['E.3.2']*pengali_max[2]))*bobot_u['E.3'] + ((bobot_u['E.4.1']*pengali_max[2]) * bobot_u['E.4']))*aspek['E_u']) + \
        (((((bobot_u['F.1.1']*pengali_max[2]))*bobot_u['F.1']) + ((bobot_u['F.2.1']*pengali_max[2])*bobot_u['F.2']) + ((bobot_u['F.3.1']*pengali_max[2]) + (bobot_u['F.3.2']*pengali_max[2]))*bobot_u['F.3'])*aspek['F_u']) + \
        (((((bobot_u['G.1.1']*pengali_max[2]) + (bobot_u['G.1.2']*pengali_max[2]) + (bobot_u['G.1.3'] *
                                                                                     pengali_max[2])) * bobot_u['G.1']) + ((bobot_u['G.2.1']*pengali_max[2])*bobot_u['G.2']))*aspek['G_u'])

    ngc_max = (jumlah_l + jumlah_m + jumlah_u) / 3

    indikator_l = [
        bobot_l['A.1.1']*pengali_max[0], bobot_l['A.1.2']*pengali_max[0], bobot_l['A.1.3'] *
        pengali_max[0], bobot_l['A.1.4']*pengali_max[0],
        bobot_l['B.1.1']*pengali_max[0], bobot_l['B.1.2'] *
        pengali_max[0], bobot_l['B.1.3']*pengali_max[0],
        bobot_l['C.1.1']*pengali_max[0], bobot_l['C.1.2']*pengali_max[0], bobot_l['C.1.3']*pengali_max[0], bobot_l['C.2.1'] *
        pengali_max[0], bobot_l['C.3.1'] *
        pengali_max[0], bobot_l['C.3.2']*pengali_max[0],
        bobot_l['D.1.1']*pengali_max[0], bobot_l['D.2.1'] *
        pengali_max[0], bobot_l['D.2.2']*pengali_max[0],
        bobot_l['E.1.1']*pengali_max[0], bobot_l['E.1.2']*pengali_max[0], bobot_l['E.2.1']*pengali_max[0], bobot_l['E.3.1'] *
        pengali_max[0], bobot_l['E.3.2'] *
        pengali_max[0], bobot_l['E.4.1']*pengali_max[0],
        bobot_l['F.1.1']*pengali_max[0], bobot_l['F.2.1']*pengali_max[0], bobot_l['F.3.1'] *
        pengali_max[0], bobot_l['F.3.2']*pengali_max[0],
        bobot_l['G.1.1']*pengali_max[0], bobot_l['G.1.2']*pengali_max[0], bobot_l['G.1.3'] *
        pengali_max[0], bobot_l['G.2.1']*pengali_max[0]
    ]

    indikator_m = [
        bobot_m['A.1.1']*pengali_max[1], bobot_m['A.1.2']*pengali_max[1], bobot_m['A.1.3'] *
        pengali_max[1], bobot_m['A.1.4']*pengali_max[1],
        bobot_m['B.1.1']*pengali_max[1], bobot_m['B.1.2'] *
        pengali_max[1], bobot_m['B.1.3']*pengali_max[1],
        bobot_m['C.1.1']*pengali_max[1], bobot_m['C.1.2']*pengali_max[1], bobot_m['C.1.3']*pengali_max[1], bobot_m['C.2.1'] *
        pengali_max[1], bobot_m['C.3.1'] *
        pengali_max[1], bobot_m['C.3.2']*pengali_max[1],
        bobot_m['D.1.1']*pengali_max[1], bobot_m['D.2.1'] *
        pengali_max[1], bobot_m['D.2.2']*pengali_max[1],
        bobot_m['E.1.1']*pengali_max[1], bobot_m['E.1.2']*pengali_max[1], bobot_m['E.2.1']*pengali_max[1], bobot_m['E.3.1'] *
        pengali_max[1], bobot_m['E.3.2'] *
        pengali_max[1], bobot_m['E.4.1']*pengali_max[1],
        bobot_m['F.1.1']*pengali_max[1], bobot_m['F.2.1']*pengali_max[1], bobot_m['F.3.1'] *
        pengali_max[1], bobot_m['F.3.2']*pengali_max[1],
        bobot_m['G.1.1']*pengali_max[1], bobot_m['G.1.2']*pengali_max[1], bobot_m['G.1.3'] *
        pengali_max[1], bobot_m['G.2.1']*pengali_max[1]
    ]

    indikator_u = [
        bobot_u['A.1.1']*pengali_max[2], bobot_u['A.1.2']*pengali_max[2], bobot_u['A.1.3'] *
        pengali_max[2], bobot_u['A.1.4']*pengali_max[2],
        bobot_u['B.1.1']*pengali_max[2], bobot_u['B.1.2'] *
        pengali_max[2], bobot_u['B.1.3']*pengali_max[2],
        bobot_u['C.1.1']*pengali_max[2], bobot_u['C.1.2']*pengali_max[2], bobot_u['C.1.3']*pengali_max[2], bobot_u['C.2.1'] *
        pengali_max[2], bobot_u['C.3.1'] *
        pengali_max[2], bobot_u['C.3.2']*pengali_max[2],
        bobot_u['D.1.1']*pengali_max[2], bobot_u['D.2.1'] *
        pengali_max[2], bobot_u['D.2.2']*pengali_max[2],
        bobot_u['E.1.1']*pengali_max[2], bobot_u['E.1.2']*pengali_max[2], bobot_u['E.2.1']*pengali_max[2], bobot_u['E.3.1'] *
        pengali_max[2], bobot_u['E.3.2'] *
        pengali_max[2], bobot_u['E.4.1']*pengali_max[2],
        bobot_u['F.1.1']*pengali_max[2], bobot_u['F.2.1']*pengali_max[2], bobot_u['F.3.1'] *
        pengali_max[2], bobot_u['F.3.2']*pengali_max[2],
        bobot_u['G.1.1']*pengali_max[2], bobot_u['G.1.2']*pengali_max[2], bobot_u['G.1.3'] *
        pengali_max[2], bobot_u['G.2.1']*pengali_max[2]
    ]

    faktor_l = [
        ((bobot_l['A.1.1']*pengali_max[0]) + (bobot_l['A.1.2']*pengali_max[0]) +
         (bobot_l['A.1.3']*pengali_max[0]) + (bobot_l['A.1.4']*pengali_max[0]))*bobot_l['A.1'],
        ((bobot_l['B.1.1']*pengali_max[0]) + (bobot_l['B.1.2']*pengali_max
                                              [0]) + (bobot_l['B.1.3']*pengali_max[0]))*bobot_l['B.1'],
        ((bobot_l['C.1.1']*pengali_max[0]) + (bobot_l['C.1.2']*pengali_max
                                              [0]) + (bobot_l['C.1.3']*pengali_max[0]))*bobot_l['C.1'],
        (bobot_l['C.2.1']*pengali_max[0])*bobot_l['C.2'],
        ((bobot_l['C.3.1']*pengali_max[0]) +
         (bobot_l['C.3.2']*pengali_max[0]))*bobot_l['C.3'],
        ((bobot_l['D.1.1']*pengali_max[0]))*bobot_l['D.1'],
        ((bobot_l['D.2.1']*pengali_max[0]) +
         (bobot_l['D.2.2']*pengali_max[0]))*bobot_l['D.2'],
        ((bobot_l['E.1.1']*pengali_max[0]) +
         (bobot_l['E.1.2']*pengali_max[0]))*bobot_l['E.1'],
        (bobot_l['E.2.1']*pengali_max[0])*bobot_l['E.2'],
        ((bobot_l['E.3.1']*pengali_max[0]) +
         (bobot_l['E.3.2']*pengali_max[0]))*bobot_l['E.3'],
        (bobot_l['E.4.1']*pengali_max[0]) * bobot_l['E.4'],
        ((bobot_l['F.1.1']*pengali_max[0]))*bobot_l['F.1'],
        (bobot_l['F.2.1']*pengali_max[0])*bobot_l['F.2'],
        ((bobot_l['F.3.1']*pengali_max[0]) +
         (bobot_l['F.3.2']*pengali_max[0]))*bobot_l['F.3'],
        ((bobot_l['G.1.1']*pengali_max[0]) + (bobot_l['G.1.2']*pengali_max
                                              [0]) + (bobot_l['G.1.3'] * pengali_max[0])) * bobot_l['G.1'],
        (bobot_l['G.2.1']*pengali_max[0])*bobot_l['G.2']
    ]

    faktor_m = [
        ((bobot_m['A.1.1']*pengali_max[1]) + (bobot_m['A.1.2']*pengali_max[1]) +
         (bobot_m['A.1.3']*pengali_max[1]) + (bobot_m['A.1.4']*pengali_max[1]))*bobot_m['A.1'],
        ((bobot_m['B.1.1']*pengali_max[1]) + (bobot_m['B.1.2']*pengali_max
                                              [1]) + (bobot_m['B.1.3']*pengali_max[1]))*bobot_m['B.1'],
        ((bobot_m['C.1.1']*pengali_max[1]) + (bobot_m['C.1.2']*pengali_max
                                              [1]) + (bobot_m['C.1.3']*pengali_max[1]))*bobot_m['C.1'],
        (bobot_m['C.2.1']*pengali_max[1])*bobot_m['C.2'],
        ((bobot_m['C.3.1']*pengali_max[1]) +
         (bobot_m['C.3.2']*pengali_max[1]))*bobot_m['C.3'],
        ((bobot_m['D.1.1']*pengali_max[1]))*bobot_m['D.1'],
        ((bobot_m['D.2.1']*pengali_max[1]) +
         (bobot_m['D.2.2']*pengali_max[1]))*bobot_m['D.2'],
        ((bobot_m['E.1.1']*pengali_max[1]) +
         (bobot_m['E.1.2']*pengali_max[1]))*bobot_m['E.1'],
        (bobot_m['E.2.1']*pengali_max[1])*bobot_m['E.2'],
        ((bobot_m['E.3.1']*pengali_max[1]) +
         (bobot_m['E.3.2']*pengali_max[1]))*bobot_m['E.3'],
        (bobot_m['E.4.1']*pengali_max[1]) * bobot_m['E.4'],
        ((bobot_m['F.1.1']*pengali_max[1]))*bobot_m['F.1'],
        (bobot_m['F.2.1']*pengali_max[1])*bobot_m['F.2'],
        ((bobot_m['F.3.1']*pengali_max[1]) +
         (bobot_m['F.3.2']*pengali_max[1]))*bobot_m['F.3'],
        ((bobot_m['G.1.1']*pengali_max[1]) + (bobot_m['G.1.2']*pengali_max
                                              [1]) + (bobot_m['G.1.3'] * pengali_max[1])) * bobot_m['G.1'],
        (bobot_m['G.2.1']*pengali_max[1])*bobot_m['G.2']
    ]

    faktor_u = [
        ((bobot_u['A.1.1']*pengali_max[2]) + (bobot_u['A.1.2']*pengali_max[2]) +
         (bobot_u['A.1.3']*pengali_max[2]) + (bobot_u['A.1.4']*pengali_max[2]))*bobot_u['A.1'],
        ((bobot_u['B.1.1']*pengali_max[2]) + (bobot_u['B.1.2']*pengali_max
                                              [2]) + (bobot_u['B.1.3']*pengali_max[2]))*bobot_u['B.1'],
        ((bobot_u['C.1.1']*pengali_max[2]) + (bobot_u['C.1.2']*pengali_max
                                              [2]) + (bobot_u['C.1.3']*pengali_max[2]))*bobot_u['C.1'],
        (bobot_u['C.2.1']*pengali_max[2])*bobot_u['C.2'],
        ((bobot_u['C.3.1']*pengali_max[2]) +
         (bobot_u['C.3.2']*pengali_max[2]))*bobot_u['C.3'],
        ((bobot_u['D.1.1']*pengali_max[2]))*bobot_u['D.1'],
        ((bobot_u['D.2.1']*pengali_max[2]) +
         (bobot_u['D.2.2']*pengali_max[2]))*bobot_u['D.2'],
        ((bobot_u['E.1.1']*pengali_max[2]) +
         (bobot_u['E.1.2']*pengali_max[2]))*bobot_u['E.1'],
        (bobot_u['E.2.1']*pengali_max[2])*bobot_u['E.2'],
        ((bobot_u['E.3.1']*pengali_max[2]) +
         (bobot_u['E.3.2']*pengali_max[2]))*bobot_u['E.3'],
        (bobot_u['E.4.1']*pengali_max[2]) * bobot_u['E.4'],
        ((bobot_u['F.1.1']*pengali_max[2]))*bobot_u['F.1'],
        (bobot_u['F.2.1']*pengali_max[2])*bobot_u['F.2'],
        ((bobot_u['F.3.1']*pengali_max[2]) +
         (bobot_u['F.3.2']*pengali_max[2]))*bobot_u['F.3'],
        ((bobot_u['G.1.1']*pengali_max[2]) + (bobot_u['G.1.2']*pengali_max
                                              [2]) + (bobot_u['G.1.3'] * pengali_max[2])) * bobot_u['G.1'],
        (bobot_u['G.2.1']*pengali_max[2])*bobot_u['G.2']
    ]

    aspek_l = [
        ((((bobot_l['A.1.1']*pengali_max[0]) + (bobot_l['A.1.2']*pengali_max[0]) + (bobot_l['A.1.3']
                                                                                    * pengali_max[0]) + (bobot_l['A.1.4']*pengali_max[0]))*bobot_l['A.1'])*aspek['A_l']),
        ((((bobot_l['B.1.1']*pengali_max[0]) + (bobot_l['B.1.2']*pengali_max
                                                [0]) + (bobot_l['B.1.3']*pengali_max[0]))*bobot_l['B.1'])*aspek['B_l']),
        (((((bobot_l['C.1.1']*pengali_max[0]) + (bobot_l['C.1.2']*pengali_max[0]) + (bobot_l['C.1.3']*pengali_max[0]))*bobot_l['C.1']) + ((bobot_l['C.2.1']
                                                                                                                                           * pengali_max[0])*bobot_l['C.2']) + ((bobot_l['C.3.1']*pengali_max[0]) + (bobot_l['C.3.2']*pengali_max[0]))*bobot_l['C.3'])*aspek['C_l']),
        (((((bobot_l['D.1.1']*pengali_max[0]))*bobot_l['D.1']) + (((bobot_l['D.2.1'] *
                                                                    pengali_max[0]) + (bobot_l['D.2.2']*pengali_max[0]))*bobot_l['D.2']))*aspek['D_l']),
        (((((bobot_l['E.1.1']*pengali_max[0]) + (bobot_l['E.1.2']*pengali_max[0]))*bobot_l['E.1']) + ((bobot_l['E.2.1']*pengali_max[0])*bobot_l['E.2']) + (
            (bobot_l['E.3.1']*pengali_max[0]) + (bobot_l['E.3.2']*pengali_max[0]))*bobot_l['E.3'] + ((bobot_l['E.4.1']*pengali_max[0]) * bobot_l['E.4']))*aspek['E_l']),
        (((((bobot_l['F.1.1']*pengali_max[0]))*bobot_l['F.1']) + ((bobot_l['F.2.1']*pengali_max[0])*bobot_l['F.2']
                                                                  ) + ((bobot_l['F.3.1']*pengali_max[0]) + (bobot_l['F.3.2']*pengali_max[0]))*bobot_l['F.3'])*aspek['F_l']),
        (((((bobot_l['G.1.1']*pengali_max[0]) + (bobot_l['G.1.2']*pengali_max[0]) + (bobot_l['G.1.3'] *
                                                                                     pengali_max[0])) * bobot_l['G.1']) + ((bobot_l['G.2.1']*pengali_max[0])*bobot_l['G.2']))*aspek['G_l'])]

    aspek_m = [
        ((((bobot_m['A.1.1']*pengali_max[1]) + (bobot_m['A.1.2']*pengali_max[1]) + (bobot_m['A.1.3']
                                                                                    * pengali_max[1]) + (bobot_m['A.1.4']*pengali_max[1]))*bobot_m['A.1'])*aspek['A_m']),
        ((((bobot_m['B.1.1']*pengali_max[1]) + (bobot_m['B.1.2']*pengali_max
                                                [1]) + (bobot_m['B.1.3']*pengali_max[1]))*bobot_m['B.1'])*aspek['B_m']),
        (((((bobot_m['C.1.1']*pengali_max[1]) + (bobot_m['C.1.2']*pengali_max[1]) + (bobot_m['C.1.3']*pengali_max[1]))*bobot_m['C.1']) + ((bobot_m['C.2.1']
                                                                                                                                           * pengali_max[1])*bobot_m['C.2']) + ((bobot_m['C.3.1']*pengali_max[1]) + (bobot_m['C.3.2']*pengali_max[1]))*bobot_m['C.3'])*aspek['C_m']),
        (((((bobot_m['D.1.1']*pengali_max[1]))*bobot_m['D.1']) + (((bobot_m['D.2.1'] *
                                                                    pengali_max[1]) + (bobot_m['D.2.2']*pengali_max[1]))*bobot_m['D.2']))*aspek['D_m']),
        (((((bobot_m['E.1.1']*pengali_max[1]) + (bobot_m['E.1.2']*pengali_max[1]))*bobot_m['E.1']) + ((bobot_m['E.2.1']*pengali_max[1])*bobot_m['E.2']) +
          ((bobot_m['E.3.1']*pengali_max[1]) + (bobot_m['E.3.2']*pengali_max[1]))*bobot_m['E.3'] + ((bobot_m['E.4.1']*pengali_max[1]) * bobot_m['E.4']))*aspek['E_m']),
        (((((bobot_m['F.1.1']*pengali_max[1]))*bobot_m['F.1']) + ((bobot_m['F.2.1']*pengali_max[1])*bobot_m['F.2']
                                                                  ) + ((bobot_m['F.3.1']*pengali_max[1]) + (bobot_m['F.3.2']*pengali_max[1]))*bobot_m['F.3'])*aspek['F_m']),
        (((((bobot_m['G.1.1']*pengali_max[1]) + (bobot_m['G.1.2']*pengali_max[1]) + (bobot_m['G.1.3'] *
                                                                                     pengali_max[1])) * bobot_m['G.1']) + ((bobot_m['G.2.1']*pengali_max[1])*bobot_m['G.2']))*aspek['G_m'])
    ]

    aspek_u = [
        ((((bobot_u['A.1.1']*pengali_max[2]) + (bobot_u['A.1.2']*pengali_max[2]) + (bobot_u['A.1.3']
                                                                                    * pengali_max[2]) + (bobot_u['A.1.4']*pengali_max[2]))*bobot_u['A.1'])*aspek['A_u']),
        ((((bobot_u['B.1.1']*pengali_max[2]) + (bobot_u['B.1.2']*pengali_max
                                                [2]) + (bobot_u['B.1.3']*pengali_max[2]))*bobot_u['B.1'])*aspek['B_u']),
        (((((bobot_u['C.1.1']*pengali_max[2]) + (bobot_u['C.1.2']*pengali_max[2]) + (bobot_u['C.1.3']*pengali_max[2]))*bobot_u['C.1']) + ((bobot_u['C.2.1']
                                                                                                                                           * pengali_max[2])*bobot_u['C.2']) + ((bobot_u['C.3.1']*pengali_max[2]) + (bobot_u['C.3.2']*pengali_max[2]))*bobot_u['C.3'])*aspek['C_u']),
        (((((bobot_u['D.1.1']*pengali_max[2]))*bobot_u['D.1']) + (((bobot_u['D.2.1'] *
                                                                    pengali_max[2]) + (bobot_u['D.2.2']*pengali_max[2]))*bobot_u['D.2']))*aspek['D_u']),
        (((((bobot_u['E.1.1']*pengali_max[2]) + (bobot_u['E.1.2']*pengali_max[2]))*bobot_u['E.1']) + ((bobot_u['E.2.1']*pengali_max[2])*bobot_u['E.2']) + (
            (bobot_u['E.3.1']*pengali_max[2]) + (bobot_u['E.3.2']*pengali_max[2]))*bobot_u['E.3'] + ((bobot_u['E.4.1']*pengali_max[2]) * bobot_u['E.4']))*aspek['E_u']),
        (((((bobot_u['F.1.1']*pengali_max[2]))*bobot_u['F.1']) + ((bobot_u['F.2.1']*pengali_max[2])*bobot_u['F.2']
                                                                  ) + ((bobot_u['F.3.1']*pengali_max[2]) + (bobot_u['F.3.2']*pengali_max[2]))*bobot_u['F.3'])*aspek['F_u']),
        (((((bobot_u['G.1.1']*pengali_max[2]) + (bobot_u['G.1.2']*pengali_max[2]) + (bobot_u['G.1.3'] *
                                                                                     pengali_max[2])) * bobot_u['G.1']) + ((bobot_u['G.2.1']*pengali_max[2])*bobot_u['G.2']))*aspek['G_u'])]
    max_indikator = [
        (indikator_l[0] + indikator_m[0] +
         indikator_u[0])/3,
        (indikator_l[1] + indikator_m[1] +
         indikator_u[1])/3,
        (indikator_l[2] + indikator_m[2] +
         indikator_u[2])/3,
        (indikator_l[3] + indikator_m[3] +
         indikator_u[3])/3,
        (indikator_l[4] + indikator_m[4] +
         indikator_u[4])/3,
        (indikator_l[5] + indikator_m[5] +
         indikator_u[5])/3,
        (indikator_l[6] + indikator_m[6] +
         indikator_u[6])/3,
        (indikator_l[7] + indikator_m[7] +
         indikator_u[7])/3,
        (indikator_l[8] + indikator_m[8] +
         indikator_u[8])/3,
        (indikator_l[9] + indikator_m[9] +
         indikator_u[9])/3,
        (indikator_l[10] + indikator_m[10] +
         indikator_u[10])/3,
        (indikator_l[11] + indikator_m[11] +
         indikator_u[11])/3,
        (indikator_l[12] + indikator_m[12] +
         indikator_u[12])/3,
        (indikator_l[13] + indikator_m[13] +
         indikator_u[13])/3,
        (indikator_l[14] + indikator_m[14] +
         indikator_u[14])/3,
        (indikator_l[15] + indikator_m[15] +
         indikator_u[15])/3,
        (indikator_l[16] + indikator_m[16] +
         indikator_u[16])/3,
        (indikator_l[17] + indikator_m[17] +
         indikator_u[17])/3,
        (indikator_l[18] + indikator_m[18] +
         indikator_u[18])/3,
        (indikator_l[19] + indikator_m[19] +
         indikator_u[19])/3,
        (indikator_l[20] + indikator_m[20] +
         indikator_u[20])/3,
        (indikator_l[21] + indikator_m[21] +
         indikator_u[21])/3,
        (indikator_l[22] + indikator_m[22] +
         indikator_u[22])/3,
        (indikator_l[23] + indikator_m[23] +
         indikator_u[23])/3,
        (indikator_l[24] + indikator_m[24] +
         indikator_u[24])/3,
        (indikator_l[25] + indikator_m[25] +
         indikator_u[25])/3,
        (indikator_l[26] + indikator_m[26] +
         indikator_u[26])/3,
        (indikator_l[27] + indikator_m[27] +
         indikator_u[27])/3,
        (indikator_l[28] + indikator_m[28] +
         indikator_u[28])/3,
        (indikator_l[29] + indikator_m[29] +
         indikator_u[29])/3,
    ]

    max_faktor = [
        (faktor_l[0] + faktor_m[0] + faktor_u[0])/3,
        (faktor_l[1] + faktor_m[1] + faktor_u[1])/3,
        (faktor_l[2] + faktor_m[2] + faktor_u[2])/3,
        (faktor_l[3] + faktor_m[3] + faktor_u[3])/3,
        (faktor_l[4] + faktor_m[4] + faktor_u[4])/3,
        (faktor_l[5] + faktor_m[5] + faktor_u[5])/3,
        (faktor_l[6] + faktor_m[6] + faktor_u[6])/3,
        (faktor_l[7] + faktor_m[7] + faktor_u[7])/3,
        (faktor_l[8] + faktor_m[8] + faktor_u[8])/3,
        (faktor_l[9] + faktor_m[9] + faktor_u[9])/3,
        (faktor_l[10] + faktor_m[10] + faktor_u[10])/3,
        (faktor_l[11] + faktor_m[11] + faktor_u[11])/3,
        (faktor_l[12] + faktor_m[12] + faktor_u[12])/3,
        (faktor_l[13] + faktor_m[13] + faktor_u[13])/3,
        (faktor_l[14] + faktor_m[14] + faktor_u[14])/3,
        (faktor_l[15] + faktor_m[15] + faktor_u[15])/3
    ]
    max_aspek = [
        (aspek_l[0] + aspek_m[0] + aspek_u[0])/3,
        (aspek_l[1] + aspek_m[1] + aspek_u[1])/3,
        (aspek_l[2] + aspek_m[2] + aspek_u[2])/3,
        (aspek_l[3] + aspek_m[3] + aspek_u[3])/3,
        (aspek_l[4] + aspek_m[4] + aspek_u[4])/3,
        (aspek_l[5] + aspek_m[5] + aspek_u[5])/3,
        (aspek_l[6] + aspek_m[6] + aspek_u[6])/3
    ]
    # perhitungan input
    jumlah_l = ((((bobot_l['A.1.1']*pengali['A.1.1'][0]) + (bobot_l['A.1.2']*pengali['A.1.2'][0]) + (bobot_l['A.1.3']*pengali['A.1.3'][0]) + (bobot_l['A.1.4']*pengali['A.1.4'][0]))*bobot_l['A.1'])*aspek['A_l']) + \
        ((((bobot_l['B.1.1']*pengali['B.1.1'][0]) + (bobot_l['B.1.2']*pengali['B.1.2'][0]) + (bobot_l['B.1.3']*pengali['B.1.3'][0]))*bobot_l['B.1'])*aspek['B_l']) + \
        (((((bobot_l['C.1.1']*pengali['C.1.1'][0]) + (bobot_l['C.1.2']*pengali['C.1.2'][0]) + (bobot_l['C.1.3']*pengali['C.1.3'][0]))*bobot_l['C.1']) + ((bobot_l['C.2.1']*pengali['C.2.1'][0])*bobot_l['C.2']) + ((bobot_l['C.3.1']*pengali['C.3.1'][0]) + (bobot_l['C.3.2']*pengali['C.3.2'][0]))*bobot_l['C.3'])*aspek['C_l']) + \
        (((((bobot_l['D.1.1']*pengali['D.1.1'][0]))*bobot_l['D.1']) + (((bobot_l['D.2.1']*pengali['D.2.1'][0]) + (bobot_l['D.2.2']*pengali['D.2.2'][0]))*bobot_l['D.2']))*aspek['D_l']) + \
        (((((bobot_l['E.1.1']*pengali['E.1.1'][0]) + (bobot_l['E.1.2']*pengali['E.1.2'][0]))*bobot_l['E.1']) + ((bobot_l['E.2.1']*pengali['E.2.1'][0])*bobot_l['E.2']) + ((bobot_l['E.3.1']*pengali['E.3.1'][0]) + (bobot_l['E.3.2']*pengali['E.3.2'][0]))*bobot_l['E.3'] + ((bobot_l['E.4.1']*pengali['E.4.1'][0]) * bobot_l['E.4']))*aspek['E_l']) + \
        (((((bobot_l['F.1.1']*pengali['F.1.1'][0]))*bobot_l['F.1']) + ((bobot_l['F.2.1']*pengali['F.2.1'][0])*bobot_l['F.2']) + ((bobot_l['F.3.1']*pengali['F.3.1'][0]) + (bobot_l['F.3.2']*pengali['F.3.2'][0]))*bobot_l['F.3'])*aspek['F_l']) + \
        (((((bobot_l['G.1.1']*pengali['G.1.1'][0]) + (bobot_l['G.1.2']*pengali['G.1.2'][0]) + (bobot_l['G.1.3'] *
                                                                                               pengali['G.1.3'][0])) * bobot_l['G.1']) + ((bobot_l['G.2.1']*pengali['G.2.1'][0])*bobot_l['G.2']))*aspek['G_l'])

    jumlah_m = ((((bobot_m['A.1.1']*pengali['A.1.1'][1]) + (bobot_m['A.1.2']*pengali['A.1.2'][1]) + (bobot_m['A.1.3']*pengali['A.1.3'][1]) + (bobot_m['A.1.4']*pengali['A.1.4'][1]))*bobot_m['A.1'])*aspek['A_m']) + \
        ((((bobot_m['B.1.1']*pengali['B.1.1'][1]) + (bobot_m['B.1.2']*pengali['B.1.2'][1]) + (bobot_m['B.1.3']*pengali['B.1.3'][1]))*bobot_m['B.1'])*aspek['B_m']) + \
        (((((bobot_m['C.1.1']*pengali['C.1.1'][1]) + (bobot_m['C.1.2']*pengali['C.1.2'][1]) + (bobot_m['C.1.3']*pengali['C.1.3'][1]))*bobot_m['C.1']) + ((bobot_m['C.2.1']*pengali['C.2.1'][1])*bobot_m['C.2']) + ((bobot_m['C.3.1']*pengali['C.3.1'][1]) + (bobot_m['C.3.2']*pengali['C.3.2'][1]))*bobot_m['C.3'])*aspek['C_m']) + \
        (((((bobot_m['D.1.1']*pengali['D.1.1'][1]))*bobot_m['D.1']) + (((bobot_m['D.2.1']*pengali['D.2.1'][1]) + (bobot_m['D.2.2']*pengali['D.2.2'][1]))*bobot_m['D.2']))*aspek['D_m']) + \
        (((((bobot_m['E.1.1']*pengali['E.1.1'][1]) + (bobot_m['E.1.2']*pengali['E.1.2'][1]))*bobot_m['E.1']) + ((bobot_m['E.2.1']*pengali['E.2.1'][1])*bobot_m['E.2']) + ((bobot_m['E.3.1']*pengali['E.3.1'][1]) + (bobot_m['E.3.2']*pengali['E.3.2'][1]))*bobot_m['E.3'] + ((bobot_m['E.4.1']*pengali['E.4.1'][1]) * bobot_m['E.4']))*aspek['E_m']) + \
        (((((bobot_m['F.1.1']*pengali['F.1.1'][1]))*bobot_m['F.1']) + ((bobot_m['F.2.1']*pengali['F.2.1'][1])*bobot_m['F.2']) + ((bobot_m['F.3.1']*pengali['F.3.1'][1]) + (bobot_m['F.3.2']*pengali['F.3.2'][1]))*bobot_m['F.3'])*aspek['F_m']) + \
        (((((bobot_m['G.1.1']*pengali['G.1.1'][1]) + (bobot_m['G.1.2']*pengali['G.1.2'][1]) + (bobot_m['G.1.3'] *
                                                                                               pengali['G.1.3'][1])) * bobot_m['G.1']) + ((bobot_m['G.2.1']*pengali['G.2.1'][1])*bobot_m['G.2']))*aspek['G_m'])

    jumlah_u = ((((bobot_u['A.1.1']*pengali['A.1.1'][2]) + (bobot_u['A.1.2']*pengali['A.1.2'][2]) + (bobot_u['A.1.3']*pengali['A.1.3'][2]) + (bobot_u['A.1.4']*pengali['A.1.4'][2]))*bobot_u['A.1'])*aspek['A_u']) + \
        ((((bobot_u['B.1.1']*pengali['B.1.1'][2]) + (bobot_u['B.1.2']*pengali['B.1.2'][2]) + (bobot_u['B.1.3']*pengali['B.1.3'][2]))*bobot_u['B.1'])*aspek['B_u']) + \
        (((((bobot_u['C.1.1']*pengali['C.1.1'][2]) + (bobot_u['C.1.2']*pengali['C.1.2'][2]) + (bobot_u['C.1.3']*pengali['C.1.3'][2]))*bobot_u['C.1']) + ((bobot_u['C.2.1']*pengali['C.2.1'][2])*bobot_u['C.2']) + ((bobot_u['C.3.1']*pengali['C.3.1'][2]) + (bobot_u['C.3.2']*pengali['C.3.2'][2]))*bobot_u['C.3'])*aspek['C_u']) + \
        (((((bobot_u['D.1.1']*pengali['D.1.1'][2]))*bobot_u['D.1']) + (((bobot_u['D.2.1']*pengali['D.2.1'][2]) + (bobot_u['D.2.2']*pengali['D.2.2'][2]))*bobot_u['D.2']))*aspek['D_u']) + \
        (((((bobot_u['E.1.1']*pengali['E.1.1'][2]) + (bobot_u['E.1.2']*pengali['E.1.2'][2]))*bobot_u['E.1']) + ((bobot_u['E.2.1']*pengali['E.2.1'][2])*bobot_u['E.2']) + ((bobot_u['E.3.1']*pengali['E.3.1'][2]) + (bobot_u['E.3.2']*pengali['E.3.2'][2]))*bobot_u['E.3'] + ((bobot_u['E.4.1']*pengali['E.4.1'][2]) * bobot_u['E.4']))*aspek['E_u']) + \
        (((((bobot_u['F.1.1']*pengali['F.1.1'][2]))*bobot_u['F.1']) + ((bobot_u['F.2.1']*pengali['F.2.1'][2])*bobot_u['F.2']) + ((bobot_u['F.3.1']*pengali['F.3.1'][2]) + (bobot_u['F.3.2']*pengali['F.3.2'][2]))*bobot_u['F.3'])*aspek['F_u']) + \
        (((((bobot_u['G.1.1']*pengali['G.1.1'][2]) + (bobot_u['G.1.2']*pengali['G.1.2'][2]) + (bobot_u['G.1.3'] *
                                                                                               pengali['G.1.3'][2])) * bobot_u['G.1']) + ((bobot_u['G.2.1']*pengali['G.2.1'][2])*bobot_u['G.2']))*aspek['G_u'])

    indikator_l = [
        bobot_l['A.1.1']*pengali['A.1.1'][0], bobot_l['A.1.2']*pengali['A.1.2'][0], bobot_l['A.1.3'] *
        pengali['A.1.3'][0], bobot_l['A.1.4']*pengali['A.1.4'][0],
        bobot_l['B.1.1']*pengali['B.1.1'][0], bobot_l['B.1.2'] *
        pengali['B.1.2'][0], bobot_l['B.1.3']*pengali['B.1.3'][0],
        bobot_l['C.1.1']*pengali['C.1.1'][0], bobot_l['C.1.2']*pengali['C.1.2'][0], bobot_l['C.1.3']*pengali['C.1.3'][0], bobot_l['C.2.1'] *
        pengali['C.2.1'][0], bobot_l['C.3.1'] *
        pengali['C.3.1'][0], bobot_l['C.3.2']*pengali['C.3.2'][0],
        bobot_l['D.1.1']*pengali['D.1.1'][0], bobot_l['D.2.1'] *
        pengali['D.2.1'][0], bobot_l['D.2.2']*pengali['D.2.2'][0],
        bobot_l['E.1.1']*pengali['E.1.1'][0], bobot_l['E.1.2']*pengali['E.1.2'][0], bobot_l['E.2.1']*pengali['E.2.1'][0], bobot_l['E.3.1'] *
        pengali['E.3.1'][0], bobot_l['E.3.2'] *
        pengali['E.3.2'][0], bobot_l['E.4.1']*pengali['E.4.1'][0],
        bobot_l['F.1.1']*pengali['F.1.1'][0], bobot_l['F.2.1']*pengali['F.2.1'][0], bobot_l['F.3.1'] *
        pengali['F.3.1'][0], bobot_l['F.3.2']*pengali['F.3.2'][0],
        bobot_l['G.1.1']*pengali['G.1.1'][0], bobot_l['G.1.2']*pengali['G.1.2'][0], bobot_l['G.1.3'] *
        pengali['G.1.3'][0], bobot_l['G.2.1']*pengali['G.2.1'][0]
    ]

    indikator_m = [
        bobot_m['A.1.1']*pengali['A.1.1'][1], bobot_m['A.1.2']*pengali['A.1.2'][1], bobot_m['A.1.3'] *
        pengali['A.1.3'][1], bobot_m['A.1.4']*pengali['A.1.4'][1],
        bobot_m['B.1.1']*pengali['B.1.1'][1], bobot_m['B.1.2'] *
        pengali['B.1.2'][1], bobot_m['B.1.3']*pengali['B.1.3'][1],
        bobot_m['C.1.1']*pengali['C.1.1'][1], bobot_m['C.1.2']*pengali['C.1.2'][1], bobot_m['C.1.3']*pengali['C.1.3'][1], bobot_m['C.2.1'] *
        pengali['C.2.1'][1], bobot_m['C.3.1'] *
        pengali['C.3.1'][1], bobot_m['C.3.2']*pengali['C.3.2'][1],
        bobot_m['D.1.1']*pengali['D.1.1'][1], bobot_m['D.2.1'] *
        pengali['D.2.1'][1], bobot_m['D.2.2']*pengali['D.2.2'][1],
        bobot_m['E.1.1']*pengali['E.1.1'][1], bobot_m['E.1.2']*pengali['E.1.2'][1], bobot_m['E.2.1']*pengali['E.2.1'][1], bobot_m['E.3.1'] *
        pengali['E.3.1'][1], bobot_m['E.3.2'] *
        pengali['E.3.2'][1], bobot_m['E.4.1']*pengali['E.4.1'][1],
        bobot_m['F.1.1']*pengali['F.1.1'][1], bobot_m['F.2.1']*pengali['F.2.1'][1], bobot_m['F.3.1'] *
        pengali['F.3.1'][1], bobot_m['F.3.2']*pengali['F.3.2'][1],
        bobot_m['G.1.1']*pengali['G.1.1'][1], bobot_m['G.1.2']*pengali['G.1.2'][1], bobot_m['G.1.3'] *
        pengali['G.1.3'][1], bobot_m['G.2.1']*pengali['G.2.1'][1]
    ]

    indikator_u = [
        bobot_u['A.1.1']*pengali['A.1.1'][2], bobot_u['A.1.2']*pengali['A.1.2'][2], bobot_u['A.1.3'] *
        pengali['A.1.3'][2], bobot_u['A.1.4']*pengali['A.1.4'][2],
        bobot_u['B.1.1']*pengali['B.1.1'][2], bobot_u['B.1.2'] *
        pengali['B.1.2'][2], bobot_u['B.1.3']*pengali['B.1.3'][2],
        bobot_u['C.1.1']*pengali['C.1.1'][2], bobot_u['C.1.2']*pengali['C.1.2'][2], bobot_u['C.1.3']*pengali['C.1.3'][2], bobot_u['C.2.1'] *
        pengali['C.2.1'][2], bobot_u['C.3.1'] *
        pengali['C.3.1'][2], bobot_u['C.3.2']*pengali['C.3.2'][2],
        bobot_u['D.1.1']*pengali['D.1.1'][2], bobot_u['D.2.1'] *
        pengali['D.2.1'][2], bobot_u['D.2.2']*pengali['D.2.2'][2],
        bobot_u['E.1.1']*pengali['E.1.1'][2], bobot_u['E.1.2']*pengali['E.1.2'][2], bobot_u['E.2.1']*pengali['E.2.1'][2], bobot_u['E.3.1'] *
        pengali['E.3.1'][2], bobot_u['E.3.2'] *
        pengali['E.3.2'][2], bobot_u['E.4.1']*pengali['E.4.1'][2],
        bobot_u['F.1.1']*pengali['F.1.1'][2], bobot_u['F.2.1']*pengali['F.2.1'][2], bobot_u['F.3.1'] *
        pengali['F.3.1'][2], bobot_u['F.3.2']*pengali['F.3.2'][2],
        bobot_u['G.1.1']*pengali['G.1.1'][2], bobot_u['G.1.2']*pengali['G.1.2'][2], bobot_u['G.1.3'] *
        pengali['G.1.3'][2], bobot_u['G.2.1']*pengali['G.2.1'][2]
    ]

    faktor_l = [
        ((bobot_l['A.1.1']*pengali['A.1.1'][0]) + (bobot_l['A.1.2']*pengali['A.1.2'][0]) +
         (bobot_l['A.1.3']*pengali['A.1.3'][0]) + (bobot_l['A.1.4']*pengali['A.1.4'][0]))*bobot_l['A.1'],
        ((bobot_l['B.1.1']*pengali['B.1.1'][0]) + (bobot_l['B.1.2']*pengali['B.1.2']
                                                   [0]) + (bobot_l['B.1.3']*pengali['B.1.3'][0]))*bobot_l['B.1'],
        ((bobot_l['C.1.1']*pengali['C.1.1'][0]) + (bobot_l['C.1.2']*pengali['C.1.2']
                                                   [0]) + (bobot_l['C.1.3']*pengali['C.1.3'][0]))*bobot_l['C.1'],
        (bobot_l['C.2.1']*pengali['C.2.1'][0])*bobot_l['C.2'],
        ((bobot_l['C.3.1']*pengali['C.3.1'][0]) +
         (bobot_l['C.3.2']*pengali['C.3.2'][0]))*bobot_l['C.3'],
        ((bobot_l['D.1.1']*pengali['D.1.1'][0]))*bobot_l['D.1'],
        ((bobot_l['D.2.1']*pengali['D.2.1'][0]) +
         (bobot_l['D.2.2']*pengali['D.2.2'][0]))*bobot_l['D.2'],
        ((bobot_l['E.1.1']*pengali['E.1.1'][0]) +
         (bobot_l['E.1.2']*pengali['E.1.2'][0]))*bobot_l['E.1'],
        (bobot_l['E.2.1']*pengali['E.2.1'][0])*bobot_l['E.2'],
        ((bobot_l['E.3.1']*pengali['E.3.1'][0]) +
         (bobot_l['E.3.2']*pengali['E.3.2'][0]))*bobot_l['E.3'],
        (bobot_l['E.4.1']*pengali['E.4.1'][0]) * bobot_l['E.4'],
        ((bobot_l['F.1.1']*pengali['F.1.1'][0]))*bobot_l['F.1'],
        (bobot_l['F.2.1']*pengali['F.2.1'][0])*bobot_l['F.2'],
        ((bobot_l['F.3.1']*pengali['F.3.1'][0]) +
         (bobot_l['F.3.2']*pengali['F.3.2'][0]))*bobot_l['F.3'],
        ((bobot_l['G.1.1']*pengali['G.1.1'][0]) + (bobot_l['G.1.2']*pengali['G.1.2']
                                                   [0]) + (bobot_l['G.1.3'] * pengali['G.1.3'][0])) * bobot_l['G.1'],
        (bobot_l['G.2.1']*pengali['G.2.1'][0])*bobot_l['G.2']
    ]

    faktor_m = [
        ((bobot_m['A.1.1']*pengali['A.1.1'][1]) + (bobot_m['A.1.2']*pengali['A.1.2'][1]) +
         (bobot_m['A.1.3']*pengali['A.1.3'][1]) + (bobot_m['A.1.4']*pengali['A.1.4'][1]))*bobot_m['A.1'],
        ((bobot_m['B.1.1']*pengali['B.1.1'][1]) + (bobot_m['B.1.2']*pengali['B.1.2']
                                                   [1]) + (bobot_m['B.1.3']*pengali['B.1.3'][1]))*bobot_m['B.1'],
        ((bobot_m['C.1.1']*pengali['C.1.1'][1]) + (bobot_m['C.1.2']*pengali['C.1.2']
                                                   [1]) + (bobot_m['C.1.3']*pengali['C.1.3'][1]))*bobot_m['C.1'],
        (bobot_m['C.2.1']*pengali['C.2.1'][1])*bobot_m['C.2'],
        ((bobot_m['C.3.1']*pengali['C.3.1'][1]) +
         (bobot_m['C.3.2']*pengali['C.3.2'][1]))*bobot_m['C.3'],
        ((bobot_m['D.1.1']*pengali['D.1.1'][1]))*bobot_m['D.1'],
        ((bobot_m['D.2.1']*pengali['D.2.1'][1]) +
         (bobot_m['D.2.2']*pengali['D.2.2'][1]))*bobot_m['D.2'],
        ((bobot_m['E.1.1']*pengali['E.1.1'][1]) +
         (bobot_m['E.1.2']*pengali['E.1.2'][1]))*bobot_m['E.1'],
        (bobot_m['E.2.1']*pengali['E.2.1'][1])*bobot_m['E.2'],
        ((bobot_m['E.3.1']*pengali['E.3.1'][1]) +
         (bobot_m['E.3.2']*pengali['E.3.2'][1]))*bobot_m['E.3'],
        (bobot_m['E.4.1']*pengali['E.4.1'][1]) * bobot_m['E.4'],
        ((bobot_m['F.1.1']*pengali['F.1.1'][1]))*bobot_m['F.1'],
        (bobot_m['F.2.1']*pengali['F.2.1'][1])*bobot_m['F.2'],
        ((bobot_m['F.3.1']*pengali['F.3.1'][1]) +
         (bobot_m['F.3.2']*pengali['F.3.2'][1]))*bobot_m['F.3'],
        ((bobot_m['G.1.1']*pengali['G.1.1'][1]) + (bobot_m['G.1.2']*pengali['G.1.2']
                                                   [1]) + (bobot_m['G.1.3'] * pengali['G.1.3'][1])) * bobot_m['G.1'],
        (bobot_m['G.2.1']*pengali['G.2.1'][1])*bobot_m['G.2']
    ]

    faktor_u = [
        ((bobot_u['A.1.1']*pengali['A.1.1'][2]) + (bobot_u['A.1.2']*pengali['A.1.2'][2]) +
         (bobot_u['A.1.3']*pengali['A.1.3'][2]) + (bobot_u['A.1.4']*pengali['A.1.4'][2]))*bobot_u['A.1'],
        ((bobot_u['B.1.1']*pengali['B.1.1'][2]) + (bobot_u['B.1.2']*pengali['B.1.2']
                                                   [2]) + (bobot_u['B.1.3']*pengali['B.1.3'][2]))*bobot_u['B.1'],
        ((bobot_u['C.1.1']*pengali['C.1.1'][2]) + (bobot_u['C.1.2']*pengali['C.1.2']
                                                   [2]) + (bobot_u['C.1.3']*pengali['C.1.3'][2]))*bobot_u['C.1'],
        (bobot_u['C.2.1']*pengali['C.2.1'][2])*bobot_u['C.2'],
        ((bobot_u['C.3.1']*pengali['C.3.1'][2]) +
         (bobot_u['C.3.2']*pengali['C.3.2'][2]))*bobot_u['C.3'],
        ((bobot_u['D.1.1']*pengali['D.1.1'][2]))*bobot_u['D.1'],
        ((bobot_u['D.2.1']*pengali['D.2.1'][2]) +
         (bobot_u['D.2.2']*pengali['D.2.2'][2]))*bobot_u['D.2'],
        ((bobot_u['E.1.1']*pengali['E.1.1'][2]) +
         (bobot_u['E.1.2']*pengali['E.1.2'][2]))*bobot_u['E.1'],
        (bobot_u['E.2.1']*pengali['E.2.1'][2])*bobot_u['E.2'],
        ((bobot_u['E.3.1']*pengali['E.3.1'][2]) +
         (bobot_u['E.3.2']*pengali['E.3.2'][2]))*bobot_u['E.3'],
        (bobot_u['E.4.1']*pengali['E.4.1'][2]) * bobot_u['E.4'],
        ((bobot_u['F.1.1']*pengali['F.1.1'][2]))*bobot_u['F.1'],
        (bobot_u['F.2.1']*pengali['F.2.1'][2])*bobot_u['F.2'],
        ((bobot_u['F.3.1']*pengali['F.3.1'][2]) +
         (bobot_u['F.3.2']*pengali['F.3.2'][2]))*bobot_u['F.3'],
        ((bobot_u['G.1.1']*pengali['G.1.1'][2]) + (bobot_u['G.1.2']*pengali['G.1.2']
                                                   [2]) + (bobot_u['G.1.3'] * pengali['G.1.3'][2])) * bobot_u['G.1'],
        (bobot_u['G.2.1']*pengali['G.2.1'][2])*bobot_u['G.2']
    ]

    aspek_l = [
        ((((bobot_l['A.1.1']*pengali['A.1.1'][0]) + (bobot_l['A.1.2']*pengali['A.1.2'][0]) + (bobot_l['A.1.3']
                                                                                              * pengali['A.1.3'][0]) + (bobot_l['A.1.4']*pengali['A.1.4'][0]))*bobot_l['A.1'])*aspek['A_l']),
        ((((bobot_l['B.1.1']*pengali['B.1.1'][0]) + (bobot_l['B.1.2']*pengali['B.1.2']
                                                     [0]) + (bobot_l['B.1.3']*pengali['B.1.3'][0]))*bobot_l['B.1'])*aspek['B_l']),
        (((((bobot_l['C.1.1']*pengali['C.1.1'][0]) + (bobot_l['C.1.2']*pengali['C.1.2'][0]) + (bobot_l['C.1.3']*pengali['C.1.3'][0]))*bobot_l['C.1']) + ((bobot_l['C.2.1']
                                                                                                                                                          * pengali['C.2.1'][0])*bobot_l['C.2']) + ((bobot_l['C.3.1']*pengali['C.3.1'][0]) + (bobot_l['C.3.2']*pengali['C.3.2'][0]))*bobot_l['C.3'])*aspek['C_l']),
        (((((bobot_l['D.1.1']*pengali['D.1.1'][0]))*bobot_l['D.1']) + (((bobot_l['D.2.1'] *
                                                                         pengali['D.2.1'][0]) + (bobot_l['D.2.2']*pengali['D.2.2'][0]))*bobot_l['D.2']))*aspek['D_l']),
        (((((bobot_l['E.1.1']*pengali['E.1.1'][0]) + (bobot_l['E.1.2']*pengali['E.1.2'][0]))*bobot_l['E.1']) + ((bobot_l['E.2.1']*pengali['E.2.1'][0])*bobot_l['E.2']) + (
            (bobot_l['E.3.1']*pengali['E.3.1'][0]) + (bobot_l['E.3.2']*pengali['E.3.2'][0]))*bobot_l['E.3'] + ((bobot_l['E.4.1']*pengali['E.4.1'][0]) * bobot_l['E.4']))*aspek['E_l']),
        (((((bobot_l['F.1.1']*pengali['F.1.1'][0]))*bobot_l['F.1']) + ((bobot_l['F.2.1']*pengali['F.2.1'][0])*bobot_l['F.2']
                                                                       ) + ((bobot_l['F.3.1']*pengali['F.3.1'][0]) + (bobot_l['F.3.2']*pengali['F.3.2'][0]))*bobot_l['F.3'])*aspek['F_l']),
        (((((bobot_l['G.1.1']*pengali['G.1.1'][0]) + (bobot_l['G.1.2']*pengali['G.1.2'][0]) + (bobot_l['G.1.3'] *
                                                                                               pengali['G.1.3'][0])) * bobot_l['G.1']) + ((bobot_l['G.2.1']*pengali['G.2.1'][0])*bobot_l['G.2']))*aspek['G_l'])]

    aspek_m = [
        ((((bobot_m['A.1.1']*pengali['A.1.1'][1]) + (bobot_m['A.1.2']*pengali['A.1.2'][1]) + (bobot_m['A.1.3']
                                                                                              * pengali['A.1.3'][1]) + (bobot_m['A.1.4']*pengali['A.1.4'][1]))*bobot_m['A.1'])*aspek['A_m']),
        ((((bobot_m['B.1.1']*pengali['B.1.1'][1]) + (bobot_m['B.1.2']*pengali['B.1.2']
                                                     [1]) + (bobot_m['B.1.3']*pengali['B.1.3'][1]))*bobot_m['B.1'])*aspek['B_m']),
        (((((bobot_m['C.1.1']*pengali['C.1.1'][1]) + (bobot_m['C.1.2']*pengali['C.1.2'][1]) + (bobot_m['C.1.3']*pengali['C.1.3'][1]))*bobot_m['C.1']) + ((bobot_m['C.2.1']
                                                                                                                                                          * pengali['C.2.1'][1])*bobot_m['C.2']) + ((bobot_m['C.3.1']*pengali['C.3.1'][1]) + (bobot_m['C.3.2']*pengali['C.3.2'][1]))*bobot_m['C.3'])*aspek['C_m']),
        (((((bobot_m['D.1.1']*pengali['D.1.1'][1]))*bobot_m['D.1']) + (((bobot_m['D.2.1'] *
                                                                         pengali['D.2.1'][1]) + (bobot_m['D.2.2']*pengali['D.2.2'][1]))*bobot_m['D.2']))*aspek['D_m']),
        (((((bobot_m['E.1.1']*pengali['E.1.1'][1]) + (bobot_m['E.1.2']*pengali['E.1.2'][1]))*bobot_m['E.1']) + ((bobot_m['E.2.1']*pengali['E.2.1'][1])*bobot_m['E.2']) +
          ((bobot_m['E.3.1']*pengali['E.3.1'][1]) + (bobot_m['E.3.2']*pengali['E.3.2'][1]))*bobot_m['E.3'] + ((bobot_m['E.4.1']*pengali['E.4.1'][1]) * bobot_m['E.4']))*aspek['E_m']),
        (((((bobot_m['F.1.1']*pengali['F.1.1'][1]))*bobot_m['F.1']) + ((bobot_m['F.2.1']*pengali['F.2.1'][1])*bobot_m['F.2']
                                                                       ) + ((bobot_m['F.3.1']*pengali['F.3.1'][1]) + (bobot_m['F.3.2']*pengali['F.3.2'][1]))*bobot_m['F.3'])*aspek['F_m']),
        (((((bobot_m['G.1.1']*pengali['G.1.1'][1]) + (bobot_m['G.1.2']*pengali['G.1.2'][1]) + (bobot_m['G.1.3'] *
                                                                                               pengali['G.1.3'][1])) * bobot_m['G.1']) + ((bobot_m['G.2.1']*pengali['G.2.1'][1])*bobot_m['G.2']))*aspek['G_m'])
    ]

    aspek_u = [
        ((((bobot_u['A.1.1']*pengali['A.1.1'][2]) + (bobot_u['A.1.2']*pengali['A.1.2'][2]) + (bobot_u['A.1.3']
                                                                                              * pengali['A.1.3'][2]) + (bobot_u['A.1.4']*pengali['A.1.4'][2]))*bobot_u['A.1'])*aspek['A_u']),
        ((((bobot_u['B.1.1']*pengali['B.1.1'][2]) + (bobot_u['B.1.2']*pengali['B.1.2']
                                                     [2]) + (bobot_u['B.1.3']*pengali['B.1.3'][2]))*bobot_u['B.1'])*aspek['B_u']),
        (((((bobot_u['C.1.1']*pengali['C.1.1'][2]) + (bobot_u['C.1.2']*pengali['C.1.2'][2]) + (bobot_u['C.1.3']*pengali['C.1.3'][2]))*bobot_u['C.1']) + ((bobot_u['C.2.1']
                                                                                                                                                          * pengali['C.2.1'][2])*bobot_u['C.2']) + ((bobot_u['C.3.1']*pengali['C.3.1'][2]) + (bobot_u['C.3.2']*pengali['C.3.2'][2]))*bobot_u['C.3'])*aspek['C_u']),
        (((((bobot_u['D.1.1']*pengali['D.1.1'][2]))*bobot_u['D.1']) + (((bobot_u['D.2.1'] *
                                                                         pengali['D.2.1'][2]) + (bobot_u['D.2.2']*pengali['D.2.2'][2]))*bobot_u['D.2']))*aspek['D_u']),
        (((((bobot_u['E.1.1']*pengali['E.1.1'][2]) + (bobot_u['E.1.2']*pengali['E.1.2'][2]))*bobot_u['E.1']) + ((bobot_u['E.2.1']*pengali['E.2.1'][2])*bobot_u['E.2']) + (
            (bobot_u['E.3.1']*pengali['E.3.1'][2]) + (bobot_u['E.3.2']*pengali['E.3.2'][2]))*bobot_u['E.3'] + ((bobot_u['E.4.1']*pengali['E.4.1'][2]) * bobot_u['E.4']))*aspek['E_u']),
        (((((bobot_u['F.1.1']*pengali['F.1.1'][2]))*bobot_u['F.1']) + ((bobot_u['F.2.1']*pengali['F.2.1'][2])*bobot_u['F.2']
                                                                       ) + ((bobot_u['F.3.1']*pengali['F.3.1'][2]) + (bobot_u['F.3.2']*pengali['F.3.2'][2]))*bobot_u['F.3'])*aspek['F_u']),
        (((((bobot_u['G.1.1']*pengali['G.1.1'][2]) + (bobot_u['G.1.2']*pengali['G.1.2'][2]) + (bobot_u['G.1.3'] *
                                                                                               pengali['G.1.3'][2])) * bobot_u['G.1']) + ((bobot_u['G.2.1']*pengali['G.2.1'][2])*bobot_u['G.2']))*aspek['G_u'])]

    output_indikator = [
        100*((indikator_l[0] + indikator_m[0] +
              indikator_u[0])/3)/max_indikator[0],
        100*((indikator_l[1] + indikator_m[1] +
              indikator_u[1])/3)/max_indikator[1],
        100*((indikator_l[2] + indikator_m[2] +
              indikator_u[2])/3)/max_indikator[2],
        100*((indikator_l[3] + indikator_m[3] +
              indikator_u[3])/3)/max_indikator[3],
        100*((indikator_l[4] + indikator_m[4] +
              indikator_u[4])/3)/max_indikator[4],
        100*((indikator_l[5] + indikator_m[5] +
              indikator_u[5])/3)/max_indikator[5],
        100*((indikator_l[6] + indikator_m[6] +
              indikator_u[6])/3)/max_indikator[6],
        100*((indikator_l[7] + indikator_m[7] +
              indikator_u[7])/3)/max_indikator[7],
        100*((indikator_l[8] + indikator_m[8] +
              indikator_u[8])/3)/max_indikator[8],
        100*((indikator_l[9] + indikator_m[9] +
              indikator_u[9])/3)/max_indikator[9],
        100*((indikator_l[10] + indikator_m[10] +
              indikator_u[10])/3)/max_indikator[10],
        100*((indikator_l[11] + indikator_m[11] +
              indikator_u[11])/3)/max_indikator[11],
        100*((indikator_l[12] + indikator_m[12] +
              indikator_u[12])/3)/max_indikator[12],
        100*((indikator_l[13] + indikator_m[13] +
              indikator_u[13])/3)/max_indikator[13],
        100*((indikator_l[14] + indikator_m[14] +
              indikator_u[14])/3)/max_indikator[14],
        100*((indikator_l[15] + indikator_m[15] +
              indikator_u[15])/3)/max_indikator[15],
        100*((indikator_l[16] + indikator_m[16] +
              indikator_u[16])/3)/max_indikator[16],
        100*((indikator_l[17] + indikator_m[17] +
              indikator_u[17])/3)/max_indikator[17],
        100*((indikator_l[18] + indikator_m[18] +
              indikator_u[18])/3)/max_indikator[18],
        100*((indikator_l[19] + indikator_m[19] +
              indikator_u[19])/3)/max_indikator[19],
        100*((indikator_l[20] + indikator_m[20] +
              indikator_u[20])/3)/max_indikator[20],
        100*((indikator_l[21] + indikator_m[21] +
              indikator_u[21])/3)/max_indikator[21],
        100*((indikator_l[22] + indikator_m[22] +
              indikator_u[22])/3)/max_indikator[22],
        100*((indikator_l[23] + indikator_m[23] +
              indikator_u[23])/3)/max_indikator[23],
        100*((indikator_l[24] + indikator_m[24] +
              indikator_u[24])/3)/max_indikator[24],
        100*((indikator_l[25] + indikator_m[25] +
              indikator_u[25])/3)/max_indikator[25],
        100*((indikator_l[26] + indikator_m[26] +
              indikator_u[26])/3)/max_indikator[26],
        100*((indikator_l[27] + indikator_m[27] +
              indikator_u[27])/3)/max_indikator[27],
        100*((indikator_l[28] + indikator_m[28] +
              indikator_u[28])/3)/max_indikator[28],
        100*((indikator_l[29] + indikator_m[29] +
              indikator_u[29])/3)/max_indikator[29],
    ]

    output_faktor = [
        100*((faktor_l[0] + faktor_m[0] + faktor_u[0])/3)/max_faktor[0],
        100*((faktor_l[1] + faktor_m[1] + faktor_u[1])/3)/max_faktor[1],
        100*((faktor_l[2] + faktor_m[2] + faktor_u[2])/3)/max_faktor[2],
        100*((faktor_l[3] + faktor_m[3] + faktor_u[3])/3)/max_faktor[3],
        100*((faktor_l[4] + faktor_m[4] + faktor_u[4])/3)/max_faktor[4],
        100*((faktor_l[5] + faktor_m[5] + faktor_u[5])/3)/max_faktor[5],
        100*((faktor_l[6] + faktor_m[6] + faktor_u[6])/3)/max_faktor[6],
        100*((faktor_l[7] + faktor_m[7] + faktor_u[7])/3)/max_faktor[7],
        100*((faktor_l[8] + faktor_m[8] + faktor_u[8])/3)/max_faktor[8],
        100*((faktor_l[9] + faktor_m[9] + faktor_u[9])/3)/max_faktor[9],
        100*((faktor_l[10] + faktor_m[10] + faktor_u[10])/3)/max_faktor[10],
        100*((faktor_l[11] + faktor_m[11] + faktor_u[11])/3)/max_faktor[11],
        100*((faktor_l[12] + faktor_m[12] + faktor_u[12])/3)/max_faktor[12],
        100*((faktor_l[13] + faktor_m[13] + faktor_u[13])/3)/max_faktor[13],
        100*((faktor_l[14] + faktor_m[14] + faktor_u[14])/3)/max_faktor[14],
        100*((faktor_l[15] + faktor_m[15] + faktor_u[15])/3)/max_faktor[15]
    ]
    output_aspek = [
        round(100*((aspek_l[0] + aspek_m[0] + aspek_u[0])/3)/max_aspek[0], 2),
        round(100*((aspek_l[1] + aspek_m[1] + aspek_u[1])/3)/max_aspek[1], 2),
        round(100*((aspek_l[2] + aspek_m[2] + aspek_u[2])/3)/max_aspek[2], 2),
        round(100*((aspek_l[3] + aspek_m[3] + aspek_u[3])/3)/max_aspek[3], 2),
        round(100*((aspek_l[4] + aspek_m[4] + aspek_u[4])/3)/max_aspek[4], 2),
        round(100*((aspek_l[5] + aspek_m[5] + aspek_u[5])/3)/max_aspek[5], 2),
        round(100*((aspek_l[6] + aspek_m[6] + aspek_u[6])/3)/max_aspek[6], 2)
    ]

    # Nilai GC
    print('jumlah_l : {}'.format(jumlah_l))
    print('jumlah_m : {}'.format(jumlah_m))
    print('jumlah_u : {}'.format(jumlah_u))
    nilai_gc = (jumlah_l + jumlah_m + jumlah_u)/3
    nilai_gc = round(100*nilai_gc/ngc_max)
    print('nilai GC : {}'.format(nilai_gc))
    category = nilai_gc_category(nilai_gc)

    return render(request, 'sistem_penilaian/hasil.html', {'output_indikator': output_indikator, 'output_faktor': output_faktor, 'output_aspek': output_aspek, 'nilai_gc': nilai_gc, 'nilai_gc_text': category["text"], 'nilai_gc_percent': category["percent"]})


# max_aspek = [16.8921494, 19.15577911, 17.43906024,
    #              11.7706905, 15.28102276, 20.95579956, 15.47348377]

    # max_faktor = [
    #     96.4, 94.0, 27.5, 36.2, 40.1, 51.3, 43.1, 39.2, 14.2, 31.9, 17.6, 25.3, 28.6, 44.7, 52.2, 42.5
    # ]

    # max_indikator = [
    #     15.29160689, 26.60001004, 33.01304073, 21.44726746, 26.51107986, 34.6096549, 32.8482192, 32.77039391, 32.85629796, 28.80761033, 90.33333333, 54.06329572, 39.42188173, 90.33333333, 34.77346278, 57.13686177, 41.53103605, 52.26084497, 90.33333333, 37.34789195, 55.89414149, 90.33333333, 90.33333333, 90.33333333, 55.348929, 37.8953462, 22.45433538, 51.27204151, 21.8576013, 90.33333333
    # ]

# aspek_by_user = [[[0.143, 0.143, 0.143], [0.143, 0.143, 0.143], [0.143, 0.143, 0.143], [
    #     0.143, 0.143, 0.143], [0.143, 0.143, 0.143], [0.143, 0.143, 0.143], [0.143, 0.143, 0.143]]]
    # faktor_by_user = [[1, 1, 1], [1, 1, 1], [0.333, 0.333, 0.333], [0.333, 0.333, 0.333], [0.333, 0.333, 0.333], [0.5, 0.5, 0.5], [0.5, 0.5, 0.5], [0.25, 0.25, 0.25], [
    #     0.25, 0.25, 0.25], [0.25, 0.25, 0.25], [0.25, 0.25, 0.25], [0.333, 0.333, 0.333], [0.333, 0.333, 0.333], [0.333, 0.333, 0.333], [0.5, 0.5, 0.5], [0.5, 0.5, 0.5]]
    # indikator_by_user = [[0.25, 0.25, 0.25], [0.25, 0.25, 0.25], [0.25, 0.25, 0.25], [0.25, 0.25, 0.25], [0.333, 0.333, 0.333], [0.333, 0.333, 0.333], [0.333, 0.333, 0.333], [0.333, 0.333, 0.333], [0.333, 0.333, 0.333], [0.333, 0.333, 0.333], [1, 1, 1], [0.5, 0.5, 0.5], [0.5, 0.5, 0.5], [
    #     1, 1, 1], [0.5, 0.5, 0.5], [0.5, 0.5, 0.5], [0.5, 0.5, 0.5], [0.5, 0.5, 0.5], [1, 1, 1], [0.5, 0.5, 0.5], [0.5, 0.5, 0.5], [1, 1, 1], [1, 1, 1], [1, 1, 1], [0.5, 0.5, 0.5], [0.5, 0.5, 0.5], [0.333, 0.333, 0.333], [0.333, 0.333, 0.333], [0.333, 0.333, 0.333], [1, 1, 1]]
