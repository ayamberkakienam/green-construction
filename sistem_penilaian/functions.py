import numpy as np


def calculate_cr(data):
    # Konstanta RI
    RI = [0, 0, 0.58, 0.9, 1.12, 1.24, 1.32, 1.41, 1.45, 1.49]

    n = len(data)
    selected_ri = RI[len(data) - 1]

    jumlah = np.zeros((n, 1))
    for row in data:
        jumlah += row

    priority_vector = np.zeros((n, 1))
    idx = 0
    for row in data:
        idx_col = 0
        for col in row:
            priority_vector[idx][0] += col/jumlah[idx_col][0]
            idx_col += 1
        priority_vector[idx][0] *= 1/n
        idx += 1

    eigen = 0
    for i in range(0, n):
        eigen += priority_vector[i][0] * jumlah[i][0]

    ci = (eigen - n)/(n-1)
    if (selected_ri != 0):
        cr = ci/selected_ri
    else:
        cr = 0

    return cr


def create_fuzzy_matrix(data):
    fuzzy_matrix = np.zeros((len(data), len(data), 3))

    for idx_row in range(len(data)):
        for idx_col in range(len(data)):
            if (data[idx_row][idx_col] == 1):  # Equal
                fuzzy_matrix[idx_row][idx_col] = [1, 1, 1]
            elif (data[idx_row][idx_col] == 9):  # Extreme Strong
                fuzzy_matrix[idx_row][idx_col] = [9, 9, 9]
            elif (data[idx_row][idx_col] > 1 and data[idx_row][idx_col] < 9):
                fuzzy_matrix[idx_row][idx_col] = [
                    data[idx_row][idx_col] - 1, data[idx_row][idx_col], data[idx_row][idx_col] + 1]
            elif (data[idx_row][idx_col] > 0 and data[idx_row][idx_col] < 1):  # Pecahan
                num = 1/data[idx_row][idx_col]
                fuzzy_matrix[idx_row][idx_col] = [
                    1/(num + 1), 1/num, 1/(num - 1)]

    return fuzzy_matrix


def calculate_weight(fuzzy_matrix):
    weight = np.zeros(len(fuzzy_matrix))
    # Calculate fuzzy geometric mean
    mean = np.ones((len(fuzzy_matrix), 3))
    for row in range(0, len(fuzzy_matrix)):
        for data in fuzzy_matrix[row][:, 0]:
            mean[row][0] *= data
        mean[row][0] = pow(mean[row][0], 1/len(fuzzy_matrix))

        for data in fuzzy_matrix[row][:, 1]:
            mean[row][1] *= data
        mean[row][1] = pow(mean[row][1], 1/len(fuzzy_matrix))

        for data in fuzzy_matrix[row][:, 2]:
            mean[row][2] *= data
        mean[row][2] = pow(mean[row][2], 1/len(fuzzy_matrix))

    # Calculate all total mean
    total = np.zeros(3)
    total = [np.sum(mean[:, 0]), np.sum(mean[:, 1]), np.sum(mean[:, 2])]
    # Calculate weight
    multplier = np.array([1/total[2], 1/total[1], 1/total[0]])
    total_weight = 0

    for idx_weight in range(0, len(weight)):
        fuzzy_weight = np.multiply(mean[idx_weight], multplier)
        weight[idx_weight] = round(np.sum(fuzzy_weight) / 3, 3)
        total_weight += weight[idx_weight]

    # Normalized weight
    if (total_weight != 1):
        for i in range(0, len(weight)):
            weight[i] = weight[i]/total_weight
            weight[i] = round(weight[i], 3)
    return weight


def get_rule_result(bobots, values):
    sigma = 0
    for i in range(len(bobots)):
        sigma += bobots[i] * values[i]

    if sigma >= 0 and sigma < 1:
        return 'buruk'
    elif sigma >= 1 and sigma < 2:
        return 'cukup'
    elif sigma >= 2 and sigma < 3:
        return 'baik'
    else:
        return 'unknown'


def fuzzy_logic(bobots, values, fuzzy, buruk_params, cukup_params, baik_params):
    result = get_rule_result(bobots, values)
    minimum_fuzzy = 9999999
    for fuz in fuzzy:
        if fuz < minimum_fuzzy:
            minimum_fuzzy = fuz

    if result == 'buruk':
        total = 0
        divider = 0
        for x in range(round(buruk_params[0]), round(buruk_params[2])):
            y = -1 / (buruk_params[2] - buruk_params[1]
                      ) * (x - buruk_params[1]) + 1
            if y >= minimum_fuzzy:
                total += minimum_fuzzy * x
                divider += minimum_fuzzy
            else:
                total += y * x
                divider += y
        return total/divider

    elif result == 'cukup':
        total = 0
        divider = 0
        for x in range(round(cukup_params[0]), round(cukup_params[2])):
            if x < cukup_params[1]:
                y = 1 / (cukup_params[1] - cukup_params[0]
                         ) * (x - cukup_params[0])
                if y >= minimum_fuzzy:
                    total += minimum_fuzzy * x
                    divider += minimum_fuzzy
                else:
                    total += y * x
                    divider += y
            else:
                y = -1 / (cukup_params[2] - cukup_params[1]
                          ) * (x - cukup_params[1]) + 1
                if y >= minimum_fuzzy:
                    total += minimum_fuzzy * x
                    divider += minimum_fuzzy
                else:
                    total += y * x
                    divider += y
        return total/divider

    elif result == 'baik':
        total = 0
        divider = 0
        for x in range(round(baik_params[0]), round(baik_params[2])):
            y = 1 / (baik_params[1] - baik_params[0]) * (x - baik_params[0])
            if y >= minimum_fuzzy:
                total += minimum_fuzzy * x
                divider += minimum_fuzzy
            else:
                total += y * x
                divider += y
        return total/divider

    else:
        return 0


def nilai_gc_category(nilai_gc):
    category_list = {
        "Sangat Tidak Hijau": [0, 21.43, 32.86],
        "Tidak Hijau": [24.29, 41.07, 55.43],
        "Cukup Hijau": [45.00, 59.29, 70.14],
        "Hijau": [65.00, 75.71, 85.14],
        "Sangat Hijau": [80.43, 89.86, 100.0],
    }

    best_percent = 0
    best_category = ""
    for key, values in category_list.items():
        percent = 0

        if key not in ["Sangat Tidak Hijau", "Sangat Hijau"]:
            # Check if nilai_gc is on left triangle
            if values[0] <= nilai_gc and nilai_gc <= values[1]:
                percent = 1 / (values[1] - values[0]) * (nilai_gc - values[0])
            # Check if nilai_gc is on right triangle
            elif values[1] < nilai_gc and nilai_gc <= values[2]:
                percent = -1 / (values[2] - values[1]) * \
                    (nilai_gc - values[1]) + 1

        # Check if nilai_gc is on bahu trapesium kiri
        elif key == "Sangat Tidak Hijau":
            # Check if nilai_gc is on bahu
            if values[0] <= nilai_gc and nilai_gc <= values[1]:
                percent = 1
            # Check if nilai_gc is on garis miring
            elif values[1] < nilai_gc and nilai_gc <= values[2]:
                percent = -1 / (values[2] - values[1]) * \
                    (nilai_gc - values[1]) + 1

        # Check if nilai_gc is on bahu trapesium kanan
        elif key == "Sangat Hijau":
            # Check if nilai_gc is on left triangle
            if values[0] <= nilai_gc and nilai_gc <= values[1]:
                percent = 1 / (values[1] - values[0]) * (nilai_gc - values[0])
            # Check if nilai_gc is on right triangle
            elif values[1] < nilai_gc and nilai_gc <= values[2]:
                percent = 1

        # Check whether current percent is the best or not
        if percent > best_percent:
            best_percent = percent
            best_category = key

    nilai_gc_text = best_category
    nilai_gc_percent = str(round(best_percent * 100)) + "%"

    return {"text": nilai_gc_text, "percent": nilai_gc_percent}


def calculate_weight_aspek(fuzzy_matrix):
    weight = np.zeros(len(fuzzy_matrix))
    # Calculate fuzzy geometric mean
    mean = np.ones((len(fuzzy_matrix), 3))
    for row in range(0, len(fuzzy_matrix)):
        for data in fuzzy_matrix[row][:, 0]:
            mean[row][0] *= data
        mean[row][0] = pow(mean[row][0], 1/len(fuzzy_matrix))

        for data in fuzzy_matrix[row][:, 1]:
            mean[row][1] *= data
        mean[row][1] = pow(mean[row][1], 1/len(fuzzy_matrix))

        for data in fuzzy_matrix[row][:, 2]:
            mean[row][2] *= data
        mean[row][2] = pow(mean[row][2], 1/len(fuzzy_matrix))

    # Calculate all total mean
    total = np.zeros(3)
    total = [np.sum(mean[:, 0]), np.sum(mean[:, 1]), np.sum(mean[:, 2])]
    # Calculate weight
    multplier = np.array([1/total[2], 1/total[1], 1/total[0]])
    multplier = multplier.tolist()
    mean = mean.tolist()
    for i in range(len(mean)):
        for j in range(len(mean[i])):
            mean[i][j] = round(mean[i][j] * multplier[j], 3)

    total_weight = 0
    return mean
