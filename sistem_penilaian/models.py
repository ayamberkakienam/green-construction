from django.db import models


class User(models.Model):
    nama = models.CharField(max_length=100)
    instansi = models.CharField(max_length=100)
    pendidikan = models.CharField(max_length=100)
    jabatan = models.CharField(max_length=100)
    lama_bekerja = models.CharField(max_length=100)
    kontak = models.CharField(max_length=100)

    class Meta:
        db_table = "user"


class Proyek(models.Model):
    nama = models.CharField(max_length=100)
    jenis_bangunan = models.CharField(max_length=100)
    luas_bangunan = models.CharField(max_length=100)
    lokasi = models.CharField(max_length=100)
    nilai_kontrak = models.CharField(max_length=100)
    green_building = models.CharField(max_length=100)
    kontraktor_pelaksana = models.CharField(max_length=100)
    user_id = models.IntegerField()
    nilai_gc = models.IntegerField()

    class Meta:
        db_table = "proyek"


class Penilaian(models.Model):
    proyek_id = models.IntegerField()
    metode = models.IntegerField()
    nilai = models.IntegerField()
    bobot_aspek_id = models.IntegerField()
    bobot_faktor_id = models.IntegerField()
    bobot_indikator_id = models.IntegerField()

    class Meta:
        db_table = "penilaian"


class Bobot_Aspek(models.Model):
    aspek = models.CharField(max_length=200)

    def split_bobot(self):
        return self.aspek.split(',')

    def labels(self):
        return [
            'Konservasi Energi', 'Konservasi Air', 'Tepat Guna Lahan', 'Sumber dan Siklus Material',
            'Manajemen Lingkungan Bangunan', 'Kualitas Udara', 'Kesehatan dan Kenyamanan dalam Proyek'
        ]

    class Meta:
        db_table = "bobot_aspek"


class Bobot_Faktor(models.Model):
    faktor = models.CharField(max_length=200)

    def split_bobot(self):
        return self.faktor.split(',')

    def labels(self):
        return [
            'Efisiensi Energi', 'Efisiensi Air', 'Rencana Perlindungan Lokasi Pekerjaan',
            'Pengurangan Jejak Ekologis Tahap Konstruksi', 'Pengelolaan Lahan', 'Perencanaan dan Penjadwalan Material',
            'Efisiensi Material', 'Manajemen Limbah Konstruksi', 'Pelatihan bagi Subkontraktor', 'Manajemen Lingkungan Proyek',
            'Dokumentasi', 'Penyimpanan dan Perlindungan Material', 'Pemilihan dan Operasional Peralatan Konstruksi',
            'Kualitas Udara dalam Proyek', 'Kesehatan Lingkungan Kerja Tahap Konstruksi', 'Program Kesehatan dan Keselamatan Kerja'
        ]

    class Meta:
        db_table = "bobot_faktor"


class Bobot_Indikator(models.Model):
    indikator = models.CharField(max_length=200)

    def split_bobot(self):
        return self.indikator.split(',')

    def labels(self):
        return [
            'Kendali Penggunaan Energi Khusus Sistem Pencahayaan', 'Kendali Penggunaan Energi Khusus Sistem Udara',
            'Penghematan Energi melalui Sistem Manajemen Energi', 'Penghematan Energi pada Sistem Transportasi Proyek dan lainnya',
            'Pemanfaatan Sumber Air Alternatif', 'Sistem Manajemen Air Bersih', 'Penghematan Air', 'Perencanaan untuk Konservasi Lingkungan',
            'Rencana Pengaturan Air Limbah dan Limpasan', 'Rencana Penggunaan Air Konstruksi',
            'Upaya untuk Meminimalkan Gangguan Lokasi Kerja (site disturbance)', 'Upaya untuk Mempertahankan Tanaman dan Menanam Kembali',
            'Pengelolaan Air Limpasan', 'Perencanaan Pengadaaan Material dan Peralatan', 'Penggunaan Material Ramah Lingkungan',
            'Metode Konstruksi Ramah Lingkungan', 'Mendaur Ulang Limbah Konstruksi', 'Tindakan untuk Mengurangi Limbah Konstruksi',
            'Ada kebijakan dan pelatihan terkait penanganan limbah bagi Subkontraktor', 'Upaya mengurangi sampah akibat kemasan makanan dan minuman',
            'Pemantauan dan Pengelolaan Sampah Konstruksi dan Domestik', 'Dokumentasi Manajemen Lingkungan Proyek Konstruksi', 'Penyimpanan dan Perlindungan Material',
            'Kebijakan Pengelolaan/Operasional Peralatan Kontruksi', 'Sistem Manajemen Kualitas Udara', 'Kententuan mengenai Pengelolaan Kualitas Udara',
            'Kendali Asap Rokok', 'Ada Kebijakan dan peralatan K3', 'Pengadaan Lokasi Khusus untuk Membersihkan Peralatan', 'Pemantauan Pelaksanaan Program K3 secara rutin'
        ]

    class Meta:
        db_table = "bobot_indikator"


class Himpunan(models.Model):
    sbr_lower = models.FloatField()
    sbr_middle = models.FloatField()
    sbr_upper = models.FloatField()
    br_lower = models.FloatField()
    br_middle = models.FloatField()
    br_upper = models.FloatField()
    c_lower = models.FloatField()
    c_middle = models.FloatField()
    c_upper = models.FloatField()
    b_lower = models.FloatField()
    b_middle = models.FloatField()
    b_upper = models.FloatField()
    sb_lower = models.FloatField()
    sb_middle = models.FloatField()
    sb_upper = models.FloatField()

    class Meta:
        db_table = "himpunan"


class Aspek_user(models.Model):
    A = models.CharField(max_length=200)
    B = models.CharField(max_length=200)
    C = models.CharField(max_length=200)
    D = models.CharField(max_length=200)
    E = models.CharField(max_length=200)
    F = models.CharField(max_length=200)
    G = models.CharField(max_length=200)

    class Meta:
        db_table = "aspek"


class Faktor_user(models.Model):
    f1 = models.CharField(max_length=200)
    f2 = models.CharField(max_length=200)
    f3 = models.CharField(max_length=200)
    f4 = models.CharField(max_length=200)
    f5 = models.CharField(max_length=200)
    f6 = models.CharField(max_length=200)
    f7 = models.CharField(max_length=200)
    f8 = models.CharField(max_length=200)
    f9 = models.CharField(max_length=200)
    f10 = models.CharField(max_length=200)
    f11 = models.CharField(max_length=200)
    f12 = models.CharField(max_length=200)
    f13 = models.CharField(max_length=200)
    f14 = models.CharField(max_length=200)
    f15 = models.CharField(max_length=200)
    f16 = models.CharField(max_length=200)

    class Meta:
        db_table = "faktors"


class Indikator_user(models.Model):
    i1 = models.CharField(max_length=200)
    i2 = models.CharField(max_length=200)
    i3 = models.CharField(max_length=200)
    i4 = models.CharField(max_length=200)
    i5 = models.CharField(max_length=200)
    i6 = models.CharField(max_length=200)
    i7 = models.CharField(max_length=200)
    i8 = models.CharField(max_length=200)
    i9 = models.CharField(max_length=200)
    i10 = models.CharField(max_length=200)
    i11 = models.CharField(max_length=200)
    i12 = models.CharField(max_length=200)
    i13 = models.CharField(max_length=200)
    i14 = models.CharField(max_length=200)
    i15 = models.CharField(max_length=200)
    i16 = models.CharField(max_length=200)
    i17 = models.CharField(max_length=200)
    i18 = models.CharField(max_length=200)
    i19 = models.CharField(max_length=200)
    i20 = models.CharField(max_length=200)
    i21 = models.CharField(max_length=200)
    i22 = models.CharField(max_length=200)
    i23 = models.CharField(max_length=200)
    i24 = models.CharField(max_length=200)
    i25 = models.CharField(max_length=200)
    i26 = models.CharField(max_length=200)
    i27 = models.CharField(max_length=200)
    i28 = models.CharField(max_length=200)
    i29 = models.CharField(max_length=200)
    i30 = models.CharField(max_length=200)

    class Meta:
        db_table = "indikator"


class Cr(models.Model):
    cr_value = models.FloatField()

    class Meta:
        db_table = "cr_nilai"
