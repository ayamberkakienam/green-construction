# Green Construction
Green Construction Assessment System web application running on Django.

## Running the Program
1. Install the requirements.
   ```
   pip install -r requirements.txt
   ```
2. Run wsgi server using `gunicorn`.
   ```
   gunicorn greenconstruction.wsgi
   ```

## Developing the Application
1. It is recommended to use [pypoetry][1] to manage project dependency in development
   environment, as it helps keep track of the explicit deps.

2. Install the requirement.
   ```
   poetry install
   ```

3. Activate development environment.
   ```
   poetry shell
   ```

[1]: https://python-poetry.org/docs/
